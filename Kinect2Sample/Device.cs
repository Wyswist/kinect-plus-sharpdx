﻿
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
using WindowsPreview.Kinect;
using SharpDX;

namespace Kinect2Sample
{
    public class Device
    {
        private byte[] backBuffer;
        private WriteableBitmap bmp;
        public Dictionary<Tuple<JointType, JointType>, Line> BonelLines = new BodyInfo(Colors.Aqua, 2).BoneLines;

        public static string which { get; set; }
        public string Draw_dots { get; set; }
        public static string Draw_resized_Teacher { get; set; }
        public static string Draw_full_resized_Teacher { get; set; }
        public static string Draw_Student_record { get; set; }
        public static string Draw_Teacher { get; set; }

        public static string Draw_Hybrid { get; set; }
        public static string Draw_BigHybrid { get; set; }
        public static int mirror = (-1);

        
        
        public float Distance;
       
        public List<JointType> Torso = new List<JointType>
        {
            JointType.Head,
            JointType.Neck,
            JointType.SpineShoulder,
            JointType.SpineMid,
            JointType.SpineBase,
            JointType.ShoulderRight,
            JointType.ShoulderLeft,
            JointType.HipRight,
            JointType.HipLeft
        };
        public List<JointType>RightArm = new List<JointType>
        {
            JointType.ElbowRight ,
            JointType.WristRight ,
            JointType.HandRight ,
            JointType.HandTipRight ,
            JointType.ThumbRight 
        };
        public List<JointType> LeftArm = new List<JointType>
        {
            // Left Arm
            JointType.ElbowLeft ,
            JointType.WristLeft ,
            JointType.HandLeft ,
            JointType.HandTipLeft ,
            JointType.ThumbLeft 
        };

        public Device(WriteableBitmap bmp)
        {
            this.bmp = bmp;
            // the back buffer size is equal to the number of pixels to draw
            // on screen (width*height) * 4 (R,G,B & Alpha values). 
            backBuffer = new byte[bmp.PixelWidth * bmp.PixelHeight * 4];
            which = Draw_dots=Draw_resized_Teacher=Draw_full_resized_Teacher=Draw_Student_record=Draw_Hybrid=Draw_Teacher=Draw_BigHybrid="";
        }

        // This method is called to clear the back buffer with a specific color
        public void Clear(byte r, byte g, byte b, byte a)
        {
            for (var index = 0; index < backBuffer.Length; index += 4)
            {
                // BGRA is used by Windows instead by RGBA in HTML5
                backBuffer[index] = b;
                backBuffer[index + 1] = g;
                backBuffer[index + 2] = r;
                backBuffer[index + 3] = a;
            }
        }

        // Once everything is ready, we can flush the back buffer
        // into the front buffer. 
        public void Present()
        {
            using (var stream = bmp.PixelBuffer.AsStream())
            {
                // writing our byte[] back buffer into our WriteableBitmap stream
                stream.Write(backBuffer, 0, backBuffer.Length);
            }
            // request a redraw of the entire bitmap
            bmp.Invalidate();
        }

        // Called to put a pixel on screen at a specific X,Y coordinates
        public void PutPixel(int x, int y, Color4 color)
        {
            // As we have a 1-D Array for our back buffer
            // we need to know the equivalent cell in 1-D based
            // on the 2D coordinates on screen
            var index = (x + y * bmp.PixelWidth) * 4;

            backBuffer[index] = (byte)(color.Blue * 255);
            backBuffer[index + 1] = (byte)(color.Green * 255);
            backBuffer[index + 2] = (byte)(color.Red * 255);
            backBuffer[index + 3] = (byte)(color.Alpha * 255);
        }

        // Project takes some 3D coordinates and transform them
        // in 2D coordinates using the transformation matrix
        public Vector2 Project(Vector3 coord, SharpDX.Matrix transMat)
        {
            // transforming the coordinates
            var point = Vector3.TransformCoordinate(coord, transMat);
            // The transformed coordinates will be based on coordinate system
            // starting on the center of the screen. But drawing on screen normally starts
            // from top left. We then need to transform them again to have x:0, y:0 on top left.
            var x = ((mirror)*point.X * bmp.PixelWidth + bmp.PixelWidth / 2.0f);
            var y = (-point.Y * bmp.PixelHeight + bmp.PixelHeight / 2.0f);
            return (new Vector2(x, y));
        }

        // DrawPoint calls PutPixel but does the clipping operation before
        public void DrawPoint(Vector2 point, Color4 color)
        {
            // Clipping what's visible on screen
            if (point.X >= 0 && point.Y >= 0 && point.X < bmp.PixelWidth && point.Y < bmp.PixelHeight)
            {
                // Drawing a yellow point
                PutPixel((int)point.X, (int)point.Y, color);
            }
        }

        public void DrawLine(Vector2 point0, Vector2 point1)
        {
            var dist = (point1 - point0).Length();

            // If the distance between the 2 points is less than 2 pixels
            // We're exiting
            if (dist < 2)
                return;

            // Find the middle point between first & second point
            Vector2 middlePoint = point0 + (point1 - point0) / 2;
            // We draw this point on screen
            DrawPoint(middlePoint, new Color4(1.0f, 1.0f, 0.0f, 1.0f));
            // Recursive algorithm launched between first & middle point
            // and between middle s& second point
            DrawLine(point0, middlePoint);
            DrawLine(middlePoint, point1);
        }

        public void DrawBline(Vector2 point0, Vector2 point1,Color4 color)
        {
            int x0 = (int)point0.X;
            int y0 = (int)point0.Y;
            int x1 = (int)point1.X;
            int y1 = (int)point1.Y;
            
            var dx = Math.Abs(x1 - x0);
            var dy = Math.Abs(y1 - y0);
            var sx = (x0 < x1) ? 1 : -1;
            var sy = (y0 < y1) ? 1 : -1;
            var err = dx - dy;

            while (true) {
                DrawPoint(new Vector2(x0, y0),color);

                if ((x0 == x1) && (y0 == y1)) break;
                var e2 = 2 * err;
                if (e2 > -dy) { err -= dy; x0 += sx; }
                if (e2 < dx) { err += dx; y0 += sy; }
            }
        }

        // The main method of the engine that re-compute each vertex projection
        // during each frame
        public void Render( Camera camera, params Mesh[] meshes)
        {
            // To understand this part, please read the prerequisites resources
            var viewMatrix = SharpDX.Matrix.LookAtRH(camera.Position, camera.Target, Vector3.UnitY);
            var projectionMatrix = SharpDX.Matrix.PerspectiveFovRH(0.078f, (float)bmp.PixelWidth / bmp.PixelHeight, 0.001f, 10.0f);

            foreach (Mesh mesh in meshes) 
            {
                // Beware to apply rotation before translation 
                var worldMatrix = SharpDX.Matrix.RotationYawPitchRoll(mesh.Rotation.Y, mesh.Rotation.X, mesh.Rotation.Z) *SharpDX.Matrix.Translation(mesh.Position);

                var transformMatrix = worldMatrix * viewMatrix * projectionMatrix;


                if (Draw_dots=="Can_Draw_Platform")
                {
                    

                    //rysowanie pionowych kresek patrzac w dół osi Y z do góry x w lewo
                    int Istep = 0;
                    for (int i = 0; i<mesh.PlatformDictionary.Count-1; i++)
                    {
                        if (Istep==SecondPage.how_many_dots_in_half_ax*2)
                        {
                            Istep=0;
                            continue;
                        }
                        var vertexA = mesh.PlatformDictionary[i];
                        var vertexB = mesh.PlatformDictionary[i+1];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);

                        DrawBline(pixelA, pixelB, new Color4(0.1f, 0.1f, 0.2f, 0.05f));
                        Istep++;
                    }
                    int IIstep = 0;
                    for (int i = 0; i<mesh.PlatformDictionary.Count-1-SecondPage.how_many_dots_in_half_ax*2; i++)
                    {

                        var vertexA = mesh.PlatformDictionary[i];
                        var vertexB = mesh.PlatformDictionary[i+SecondPage.how_many_dots_in_half_ax*2+1];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);

                        DrawBline(pixelA, pixelB, new Color4(0.1f, 0.1f, 0.2f, 0.05f));
                        if (IIstep==SecondPage.how_many_dots_in_half_ax*2)
                        {
                            IIstep=0;
                            continue;
                        }
                        IIstep++;
                    }

                    // na dolepo to by mozna było kropki widziec

                    Color4 platform_color = new Color4(0.0f, 1f, 0.0f, 1f);
                    foreach (var ikey in mesh.PlatformDictionary)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, platform_color);
                    }
                }
                if (mesh.Teacher != null && Draw_Teacher=="draw_Teacher")
                {
                    Draw_resized_Teacher="";
                    foreach (var ikey in mesh.Teacher)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(0.3f, 0.7f, 0.0f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {
                        Color4 color = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
                        if (mesh.Teacher!=null)
                        {
                            var vertexA = mesh.Teacher[boneline.Item1.ToString()];
                            var vertexB = mesh.Teacher[boneline.Item2.ToString()];
                            var pixelA = Project(vertexA, transformMatrix);
                            var pixelB = Project(vertexB, transformMatrix);
                           
                            DrawBline(pixelA, pixelB, color);
                        }
                    }
                    //if(mesh.TeacherCenterOfGravity!=null)
                    //{
                    //    var verr = Project(mesh.TeacherCenterOfGravity, transformMatrix);
                    //    DrawPoint(verr, new Color4(1f, 1f, 1f, 1.0f));
                    //}

                }
                if (SecondPage.Draw_axis_Click_bool==true)
                {
                    Color4 color_ax =new Color4(0.3f, 0.3f, 0.3f,1f);
                    string ax_name = "";
                    foreach (var ax in mesh.AxisDictionary)
                    {
                        if (ax.Key.Contains("X"))
                        {
                            ax_name = "X";
                            color_ax=new Color4(1f, 0.3f, 0.3f, 0.8f);
                        }
                        if (ax.Key.Contains("Y"))
                        {
                            ax_name="Y";
                            color_ax=new Color4(0.3f, 1f, 0.3f, 0.8f); 
                        }
                        if (ax.Key.Contains("Z"))
                        {
                            ax_name = "Z";
                            color_ax=new Color4(0.3f, 0.3f, 1f, 0.8f);
                        }
                        var vertexXcur = mesh.AxisDictionary[(ax_name+ax.Value.Item1)].Item3;
                        var vertexXnext = mesh.AxisDictionary[(ax_name+ax.Value.Item2)].Item3;
                        var pixelcur = Project(vertexXcur, transformMatrix);
                        var pixelnext = Project(vertexXnext, transformMatrix);
                        DrawBline(pixelcur, pixelnext, color_ax);
                    }
                }
                if (mesh.RefMan != null)
                {
                    Color4 color = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
                    foreach (var ikey in mesh.RefMan)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(0.3f, 0.7f, 0.0f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {
                        var vertexA = mesh.RefMan[boneline.Item1.ToString()];
                        var vertexB = mesh.RefMan[boneline.Item2.ToString()];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);
                        if (vertexB.X > 0)
                            color = new Color4(1.0f, 0.0f, 0.0f, 1.0f);
                        if (vertexB.Y > 0)
                            color = new Color4(0.0f, 1.0f, 0.0f, 1.0f);
                        if (vertexB.Z > 0)
                            color = new Color4(0.0f, 0.0f, 1.0f, 1.0f);
                        DrawBline(pixelA, pixelB, color);
                    }
                }
                if (Draw_resized_Teacher == "draw_resized_Teacher")
                {
                    Color4 resized_teacher_color = new Color4(0.7f, 0.7f, 0.8f, 1.0f);
                    foreach (var ikey in mesh.ResizedTeacher)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(1f, 0.7f, 0.8f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {
                        var vertexA = mesh.ResizedTeacher[boneline.Item1.ToString()];
                        var vertexB = mesh.ResizedTeacher[boneline.Item2.ToString()];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);
                        DrawBline(pixelA, pixelB, resized_teacher_color);
                    }
                    if (mesh.ResizedTeacherCenterOfGravity!=null)
                    {
                        var verr = Project(mesh.ResizedTeacherCenterOfGravity, transformMatrix);
                        DrawPoint(verr, new Color4(1f, 1f, 1f, 1.0f));
                    }
                }
                if (Draw_full_resized_Teacher=="draw_full_resized_Teacher")
                {
                    Color4 full_resized_teacher_color = new Color4(1f, 0.7f, 0f, 1.0f);
                    foreach (var ikey in mesh.FullResizedTeacher)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(0f, 1f, 0f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {
                        var vertexA = mesh.FullResizedTeacher[boneline.Item1.ToString()];
                        var vertexB = mesh.FullResizedTeacher[boneline.Item2.ToString()];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);
                        DrawBline(pixelA, pixelB, full_resized_teacher_color);
                    }
                

                    //Color4 full_resized_teacher_color = new Color4(1f, 0.7f, 0f, 1.0f);
                    //foreach (var ikey in mesh.FullResizedTeacher)
                    //{
                    //    if (ikey.Key==JointType.Head.ToString()||ikey.Key==JointType.Neck.ToString()||ikey.Key==JointType.SpineShoulder.ToString()||ikey.Key==JointType.SpineMid.ToString()||ikey.Key==JointType.SpineBase.ToString()||ikey.Key==JointType.ShoulderRight.ToString())
                    //    {
                    //        var verr = Project(ikey.Value, transformMatrix);
                    //        DrawPoint(verr, new Color4(0f, 1f, 0f, 1.0f));
                    //    }

                    //}
                    //foreach (var boneline in BonelLines.Keys)
                    //{
                    //    if (boneline.Item1==JointType.Head&&boneline.Item2==JointType.Neck)
                    //    {
                    //        var vertexA = mesh.FullResizedTeacher[boneline.Item1.ToString()];
                    //        var vertexB = mesh.FullResizedTeacher[boneline.Item2.ToString()];
                    //        var pixelA = Project(vertexA, transformMatrix);
                    //        var pixelB = Project(vertexB, transformMatrix);
                    //        DrawBline(pixelA, pixelB, full_resized_teacher_color); 
                    //    }
                    //    if (boneline.Item1==JointType.Neck&&boneline.Item2==JointType.SpineShoulder)
                    //    {
                    //        var vertexA = mesh.FullResizedTeacher[boneline.Item1.ToString()];
                    //        var vertexB = mesh.FullResizedTeacher[boneline.Item2.ToString()];
                    //        var pixelA = Project(vertexA, transformMatrix);
                    //        var pixelB = Project(vertexB, transformMatrix);
                    //        DrawBline(pixelA, pixelB, full_resized_teacher_color);
                    //    }
                    //    if (boneline.Item1==JointType.SpineShoulder&&boneline.Item2==JointType.SpineMid)
                    //    {
                    //        var vertexA = mesh.FullResizedTeacher[boneline.Item1.ToString()];
                    //        var vertexB = mesh.FullResizedTeacher[boneline.Item2.ToString()];
                    //        var pixelA = Project(vertexA, transformMatrix);
                    //        var pixelB = Project(vertexB, transformMatrix);
                    //        DrawBline(pixelA, pixelB, full_resized_teacher_color);
                    //    }
                    //    if (boneline.Item1==JointType.SpineMid&&boneline.Item2==JointType.SpineBase)
                    //    {
                    //        var vertexA = mesh.FullResizedTeacher[boneline.Item1.ToString()];
                    //        var vertexB = mesh.FullResizedTeacher[boneline.Item2.ToString()];
                    //        var pixelA = Project(vertexA, transformMatrix);
                    //        var pixelB = Project(vertexB, transformMatrix);
                    //        DrawBline(pixelA, pixelB, full_resized_teacher_color);
                    //    }
                    //    if (boneline.Item1==JointType.SpineShoulder&&boneline.Item2==JointType.ShoulderRight)
                    //    {
                    //        var vertexA = mesh.FullResizedTeacher[boneline.Item1.ToString()];
                    //        var vertexB = mesh.FullResizedTeacher[boneline.Item2.ToString()];
                    //        var pixelA = Project(vertexA, transformMatrix);
                    //        var pixelB = Project(vertexB, transformMatrix);
                    //        DrawBline(pixelA, pixelB, full_resized_teacher_color);
                    //    }
                    //}




                    //Color4 full_resized_teacher_color = new Color4(1f, 0.7f, 0f, 1.0f);
                    //foreach (var ikey in mesh.TMPFullResizedTeacher)
                    //{
                    //    var verr = Project(ikey.Value, transformMatrix);
                    //    DrawPoint(verr, new Color4(0f, 1f, 0f, 1.0f));
                    //}
                    //foreach (var boneline in BonelLines.Keys)
                    //{
                    //    var vertexA = mesh.TMPFullResizedTeacher[boneline.Item2.ToString()];
                    //    var vertexB = mesh.TMPFullResizedTeacher[boneline.Item2.ToString()+"prim"];
                    //    var pixelA = Project(vertexA, transformMatrix);
                    //    var pixelB = Project(vertexB, transformMatrix);
                    //    DrawBline(pixelA, pixelB, full_resized_teacher_color);
                    //}

                }
                if (which=="Rysuj_aktualnego_studenta")
                {
                    Color4 student_color = new Color4(0.5f, 0.7f, 0.8f, 1.0f);
                    foreach (var ikey in mesh.Student)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(1f, 0.7f, 0.8f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {
                        var vertexA = mesh.Student[boneline.Item1.ToString()];
                        var vertexB = mesh.Student[boneline.Item2.ToString()];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);
                        DrawBline(pixelA, pixelB, student_color);
                    }
                    if(mesh.StudentCenterOfGravity!=null)
                    {
                        var verr = Project(mesh.StudentCenterOfGravity , transformMatrix);
                        DrawPoint(verr, new Color4(1f, 0.7f, 0.8f, 1.0f));
                    }
                }
                if (Draw_Hybrid=="draw_Hybrid")
                {
                    Color4 hybrid_color = new Color4(0f, 0f, 1f, 0f);
                    //var ActualHybrid = mesh.Hybrid[SecondPage.StudentStartPosition];
                    Dictionary<string, Vector3> ActualHybrid;
                    //if (SecondPage.StudentRecComboBoxSelectedIndex<=mesh.Hybrid.Keys.Count-1)
                    //{
                    ActualHybrid=mesh.Hybrid[SecondPage.StudentRecComboBoxSelectedIndex];
                    //}
                    //else
                    //{
                    //    ActualHybrid=mesh.Hybrid[0];
                    //}

                    foreach (var ikey in ActualHybrid)
                    {

                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(1f, 1f, 0f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {

                        if (( boneline.Item1==JointType.SpineBase&&boneline.Item2==JointType.HipRight )||( boneline.Item1==JointType.SpineBase&&boneline.Item2==JointType.HipLeft ))
                            continue;

                        var vertexA = ActualHybrid[boneline.Item1.ToString()];
                        var vertexB = ActualHybrid[boneline.Item2.ToString()];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);
                        DrawBline(pixelA, pixelB, hybrid_color);
                    }
                }
                if (Draw_BigHybrid=="draw_BigHybrid")
                {
                    Color4 Bighybrid_color = new Color4(0f, 0f, 1f, 0f);
                    Dictionary<string, Vector3> ActualBigHybrid;
                    ActualBigHybrid=mesh.HybridAll[SecondPage.BigHybridActualIndex];
                    var StudentRecordFootLeft = new Vector3(mesh.StudentRecord[JointType.FootLeft.ToString()].X, mesh.StudentRecord[JointType.FootLeft.ToString()].Y, mesh.StudentRecord[JointType.FootLeft.ToString()].Z);
                    var StudentRecordFootRight = new Vector3(mesh.StudentRecord[JointType.FootRight.ToString()].X, mesh.StudentRecord[JointType.FootRight.ToString()].Y, mesh.StudentRecord[JointType.FootRight.ToString()].Z);

                    var LeftFootDistanceToMove = Vector3.Subtract(StudentRecordFootLeft, ActualBigHybrid[JointType.FootLeft.ToString()]);
                    var RightFootDistanceToMove = Vector3.Subtract(StudentRecordFootRight, ActualBigHybrid[JointType.FootRight.ToString()]);
                    var ShoulderLeftToMove = Vector3.Subtract(mesh.StudentRecord[JointType.ShoulderLeft.ToString()],ActualBigHybrid[JointType.ShoulderLeft.ToString()]);
                    var ShoulderRightToMove = Vector3.Subtract(mesh.StudentRecord[JointType.ShoulderRight.ToString()], ActualBigHybrid[JointType.ShoulderRight.ToString()]);

                    foreach (var item in Torso)
                    {
                        ActualBigHybrid[item.ToString()]=mesh.StudentRecord[item.ToString()];
                    }
                    foreach (var item in LeftArm)
                    {
                        ActualBigHybrid[item.ToString()]=Vector3.Add(ActualBigHybrid[item.ToString()], ShoulderLeftToMove);
                    }
                    foreach (var item in RightArm)
                    {
                        ActualBigHybrid[item.ToString()]=Vector3.Add(ActualBigHybrid[item.ToString()], ShoulderRightToMove);
                    }
                    ActualBigHybrid[JointType.FootLeft.ToString()]=Vector3.Add(ActualBigHybrid[JointType.FootLeft.ToString()], LeftFootDistanceToMove);
                    ActualBigHybrid[JointType.AnkleLeft.ToString()]=Vector3.Add(ActualBigHybrid[JointType.AnkleLeft.ToString()], LeftFootDistanceToMove);
                    ActualBigHybrid[JointType.KneeLeft.ToString()]=Vector3.Add(ActualBigHybrid[JointType.KneeLeft.ToString()], LeftFootDistanceToMove);
                    ActualBigHybrid[JointType.HipLeft.ToString()]=Vector3.Add(ActualBigHybrid[JointType.HipLeft.ToString()], LeftFootDistanceToMove);

                    ActualBigHybrid[JointType.FootRight.ToString()]=Vector3.Add(ActualBigHybrid[JointType.FootRight.ToString()], RightFootDistanceToMove);
                    ActualBigHybrid[JointType.AnkleRight.ToString()]=Vector3.Add(ActualBigHybrid[JointType.AnkleRight.ToString()], RightFootDistanceToMove);
                    ActualBigHybrid[JointType.KneeRight.ToString()]=Vector3.Add(ActualBigHybrid[JointType.KneeRight.ToString()], RightFootDistanceToMove);
                    ActualBigHybrid[JointType.HipRight.ToString()]=Vector3.Add(ActualBigHybrid[JointType.HipRight.ToString()], RightFootDistanceToMove);


                    foreach (var ikey in ActualBigHybrid)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(1f, 1f, 0f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {

                        if (( boneline.Item1==JointType.SpineBase&&boneline.Item2==JointType.HipRight )||( boneline.Item1==JointType.SpineBase&&boneline.Item2==JointType.HipLeft ))
                            continue;

                        var vertexA = ActualBigHybrid[boneline.Item1.ToString()];
                        var vertexB = ActualBigHybrid[boneline.Item2.ToString()];
                        var pixelA = Project(vertexA, transformMatrix);
                        var pixelB = Project(vertexB, transformMatrix);
                        DrawBline(pixelA, pixelB, Bighybrid_color);
                    }
                    //if (mesh.ResizedTeacherCenterOfGravity!=null)
                    //{
                    //    var verr = Project(mesh.ResizedTeacherCenterOfGravity, transformMatrix);
                    //    DrawPoint(verr, new Color4(1f, 1f, 1f, 1.0f));
                    //}
                }
                if (Draw_Student_record=="draw_Student_record")
                {
                    Color4 Student_Record_color=new Color4(1f, 1f, 0f, 0f); 
                    foreach (var ikey in mesh.StudentRecord)
                    {
                        var verr = Project(ikey.Value, transformMatrix);
                        DrawPoint(verr, new Color4(0f, 0f, 1f, 1.0f));
                    }
                    foreach (var boneline in BonelLines.Keys)
                    {
                        
                        var StudentRecVertexA = mesh.StudentRecord[boneline.Item1.ToString()];
                        var StudentRecVertexB = mesh.StudentRecord[boneline.Item2.ToString()];
                       
                        if (mesh.Hybrid!=null)
                        {
                            var ActualHybrid = mesh.Hybrid[SecondPage.StudentRecComboBoxSelectedIndex];

                            if (ActualHybrid!=null)
                            {
                                var HybridVertexA = ActualHybrid[boneline.Item1.ToString()];
                                var HybridVertexB = ActualHybrid[boneline.Item2.ToString()];

                                var StudentRecVector = Vector3.Add(StudentRecVertexB, StudentRecVertexA);
                                var HybridVector = Vector3.Add(HybridVertexB, HybridVertexA);
                                Distance = Vector3.Distance(StudentRecVector, HybridVector);
                                Distance=Math.Abs(Distance);
                                if(Distance<0.1)
                                {
                                    Student_Record_color=new Color4(0f, 1f, 0f, 0f);
                                }
                                else
                                {
                                    Student_Record_color=new Color4(1f, 0f, 0f, 0f);
                                }
                            }
                        }
                        var pixelA = Project(StudentRecVertexA, transformMatrix);
                        var pixelB = Project(StudentRecVertexB, transformMatrix);
                        DrawBline(pixelA, pixelB, Student_Record_color);
                    }
                }
          
            }
        }
    }
}
