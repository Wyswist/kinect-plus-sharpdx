﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using WindowsPreview.Kinect;
using SharpDX;
using Windows.UI.Xaml.Shapes;
using Windows.UI;
using Windows.System.Threading;
using System.Threading;
using Windows.UI.Xaml.Automation.Peers;
using Windows.UI.Xaml.Automation.Provider;

namespace Kinect2Sample
{
    class Measurement
    {
        Timers Timer = new Timers();
        public int Index { get; set; }
        public int CommandsToJointsIndex { get; set; }
        public int CheckingChecking { get; set; }
        private string[] joint_names;
        public DispatcherTimer TimerForString;
        public DispatcherTimer TimerFor_jestgit;
        public bool cangetthru;
        public int przeszło = 0;
        string command;
        public Dictionary<Tuple<JointType, JointType>, Line> BonelLines = new BodyInfo(Colors.Aqua, 2).BoneLines;
        //float  1-Xdistance, 2-Ydistance, 3-Zdistance, 4-|distance|
        public Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> TeacherBoneLength { get; set; }
        public Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> StudentBoneLength { get; set; }
        public Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> DifferenceBetweenTeacherBoneLengthStudentBoneLength { get; set; }

        /// <summary>
        ///pierwszy double to alpha ,drugi beta ,trzeci gamma,na wektor składają sie zmienne  długosc po X_odejmowanego kawałka 'X_lenght'
        ///Y_lenght Z_lenght
        ///alpha to kat pomiedzy X a lenght
        ///beta to kat pomiedzy Y a lenght
        ///gamma to kat pomiedzy Z a lenght
        ///czwarta wartosc to róznica pomiedzy długością kości nauczyciela a studenta
        /// </summary>
        public Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>> TeacherAnglesAndStudentNewBones { get; set; }
        public Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>> StudentBoneAnglesAndVectors { get; set; }

        public Dictionary<Tuple<string, string, string>, double> TeacherAnglesBetweenBones { get; set; }
        public Dictionary<Tuple<string, string, string>, double> StudentAnglesBetweenBones { get; set; }
        public Dictionary<Tuple<string, string, string>, bool> StudentAnglesBetweenBonesBools { get; set; }
        public Dictionary<string, Tuple<Dictionary<string, bool>, bool>> PositioningBool { get; set; }
        public bool once_gone { get; set; }
        bool once_gone_thru_for_COG { get; set; }
        double AngleBetweenBones_to_once_gone_thru = 0;
        string WhatToDo;
        bool razchce;
        //1bool-czy po x jest ustawione 
        //3 bool czy po z jest ustawione
        private Dictionary<string, Tuple<bool, bool, bool>> XYZCorrectnessDictionary { get; set; }
        public bool ShowMovementOnTeacher_BOOL_ONCE;
        public bool SetStartingPosition_BOOL_ONCE;
        public static bool wait_bool { get; set; }
        public static int milliseconds { get; set; }
        public bool ShowTime_bool_once { get; set; }
        public SecondPage SecondPageObj;
        public Measurement(SecondPage SecondPageAsParam)
        {
            razchce=true;
            CommandsToJointsIndex=0;
            Index=15;
            CheckingChecking=0;
            joint_names=Enum.GetNames(typeof(JointType));
            cangetthru=false;
            command=String.Empty;
            PositioningBool=new Dictionary<string, Tuple<Dictionary<string, bool>, bool>>();
            once_gone=true;
            once_gone_thru_for_COG=false;
            WhatToDo="Wyprowadzenie_ciosu";
            ShowMovementOnTeacher_BOOL_ONCE=true;
            SetStartingPosition_BOOL_ONCE=true;
            wait_bool=false;
            SecondPageObj=SecondPageAsParam;
        }
        public Vector3 measure(Vector3 pointA, Vector3 pointB, int round, bool Abs)
        {
            if (Abs==true)
            {
                float distanceAtX = (float)Math.Round(Math.Abs(pointA.X-pointB.X), round);
                float distanceAtY = (float)Math.Round(Math.Abs(pointA.Y-pointB.Y), round);
                float distanceAtZ = (float)Math.Round(Math.Abs(pointA.Z-pointB.Z), round);
                return new Vector3(distanceAtX, distanceAtY, distanceAtZ);
            }
            else
            {
                float distanceAtX = (float)Math.Round(( pointA.X-pointB.X ), round);
                float distanceAtY = (float)Math.Round(( pointA.Y-pointB.Y ), round);
                float distanceAtZ = (float)Math.Round(( pointA.Z-pointB.Z ), round);
                return new Vector3(distanceAtX, distanceAtY, distanceAtZ);
            }
        }

        public string CommandsToJoints(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            string command = "nic";
            if (Teacher!=null&&Student!=null)
            {
                if (CommandsToJointsIndex==4)
                {
                    command="jestes ustawiony";
                    TimerForString=new DispatcherTimer { Interval=new TimeSpan(00, 0, 0, 3, 0) };
                    TimerForString.Tick+=TimerForString_Tick;
                    SecondPage.move_to_next_position=true;
                    TimerForString.Start();
                }
                for (; CommandsToJointsIndex<Enum.GetValues(typeof(JointType)).Length-21;)
                {

                    if (cangetthru)
                    {
                        command=joint_names[CommandsToJointsIndex]+" jest git";
                        break;
                    }
                    Vector3 distance = measure(Teacher[joint_names[CommandsToJointsIndex]], Student[joint_names[CommandsToJointsIndex]], 2, false);
                    if (Math.Abs(distance.Z)<accuracy)
                    {
                        command=joint_names[CommandsToJointsIndex]+" jest git";
                        cangetthru=true;
                        break;
                    }
                    if (distance.Z>0)
                    {
                        command=" Przesuń "+joint_names[CommandsToJointsIndex]+" o "+Math.Abs(distance.Z*100)+" cm do tyłu";
                        break;
                    }
                    if (distance.Z<0)
                    {
                        command=" Przesuń "+joint_names[CommandsToJointsIndex]+" o "+Math.Abs(distance.Z*100)+" cm do przodu";
                        //command=" Przesuń";
                        break;
                    }
                }
                if (cangetthru&&przeszło==0)
                {
                    StartTimer();
                    przeszło++;
                }
            }
            return command;
        }

        public string SettingFootPosition(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            switch (Index)
            {
                case (int)JointType.FootLeft:
                    {
                        if (Teacher!=null&&Student!=null)
                        {
                            Vector3 distance = measure(Teacher[JointType.FootLeft.ToString()], Student[JointType.FootLeft.ToString()], 2, false);
                            if (XYZCorrectnessDictionary[JointType.FootLeft.ToString()].Item3==false)
                            {
                                if (Math.Abs(distance.Z)<accuracy)
                                {
                                    command=JointType.FootLeft.ToString()+" jest git na zetach";
                                    XYZCorrectnessDictionary[JointType.FootLeft.ToString()]=new Tuple<bool, bool, bool>(false, false, true);
                                }
                                if (distance.Z>0)
                                {
                                    command=" Przesuń "+JointType.FootLeft.ToString()+" o "+Math.Abs(distance.Z*100)+" cm do tyłu";
                                }
                                if (distance.Z<0)
                                {
                                    command=" Przesuń "+JointType.FootLeft.ToString()+" o "+Math.Abs(distance.Z*100)+" cm do przodu";
                                }
                            }
                            if (XYZCorrectnessDictionary[JointType.FootLeft.ToString()].Item1==false&&XYZCorrectnessDictionary[JointType.FootLeft.ToString()].Item3==true)
                            {
                                if (Math.Abs(distance.X)<accuracy)
                                {
                                    command=JointType.FootLeft.ToString()+" jest git na iksach";
                                    XYZCorrectnessDictionary[JointType.FootLeft.ToString()]=new Tuple<bool, bool, bool>(true, false, true);
                                }
                                if (distance.X>0)
                                {
                                    command=" Przesuń "+JointType.FootLeft.ToString()+" o "+Math.Abs(distance.X*100)+" cm w prawo";

                                }
                                if (distance.X<0)
                                {
                                    command=" Przesuń "+JointType.FootLeft.ToString()+" o "+Math.Abs(distance.X*100)+" cm w lewo";

                                }
                            }
                            if (XYZCorrectnessDictionary[JointType.FootLeft.ToString()].Item1==true&&XYZCorrectnessDictionary[JointType.FootLeft.ToString()].Item2==false&&XYZCorrectnessDictionary[JointType.FootLeft.ToString()].Item3==true)
                            {
                                if (CheckingChecking==5)
                                {
                                    Index=19;
                                    CheckingChecking=0;
                                }
                                XYZCorrectnessDictionary[JointType.FootLeft.ToString()]=new Tuple<bool, bool, bool>(false, false, false);
                                CheckingChecking++;
                            }
                        }
                    }
                    break;
                case (int)JointType.FootRight:
                    {
                        if (Teacher!=null&&Student!=null)
                        {
                            Vector3 distance = measure(Teacher[JointType.FootRight.ToString()], Student[JointType.FootRight.ToString()], 2, false);

                            if (XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item3==false)
                            {
                                if (Math.Abs(distance.Z)<accuracy)
                                {
                                    command=JointType.FootRight.ToString()+" jest git";
                                    XYZCorrectnessDictionary[JointType.FootRight.ToString()]=new Tuple<bool, bool, bool>(false, false, true);
                                }
                                else if (distance.Z>0)
                                {
                                    command=" Przesuń "+JointType.FootRight.ToString()+" o "+Math.Abs(distance.Z*100)+" cm do tyłu";
                                }
                                else if (distance.Z<0)
                                {
                                    command=" Przesuń "+JointType.FootRight.ToString()+" o "+Math.Abs(distance.Z*100)+" cm do przodu";
                                }
                            }
                            if (XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item1==false&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item3==true)
                            {
                                if (Math.Abs(distance.X)<accuracy)
                                {
                                    command=JointType.FootRight.ToString()+" jest git";
                                    XYZCorrectnessDictionary[JointType.FootRight.ToString()]=new Tuple<bool, bool, bool>(true, false, true);
                                }
                                else if (distance.X>0)
                                {
                                    command=" Przesuń "+JointType.FootRight.ToString()+" o "+Math.Abs(distance.X*100)+" cm w prawo";
                                }
                                else if (distance.X<0)
                                {
                                    command=" Przesuń "+JointType.FootRight.ToString()+" o "+Math.Abs(distance.X*100)+" cm w lewo";
                                }
                            }
                            if (XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item1==true&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item2==false&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item3==true)
                            {
                                if (CheckingChecking>=10)
                                {
                                    if (razchce)
                                    {
                                        command="Poszło do BonesMeasurment";
                                    }
                                    break;
                                }
                                XYZCorrectnessDictionary[JointType.FootRight.ToString()]=new Tuple<bool, bool, bool>(false, false, false);
                                CheckingChecking++;
                            }
                        }
                    }
                    break;
                default:
                    command="nic";
                    break;
            }
            if (XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item1==true&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item2==false&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item3==true&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item1==true&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item2==false&&XYZCorrectnessDictionary[JointType.FootRight.ToString()].Item3==true)
            {
                BonesMeasurment(Teacher, Student);
                if (razchce)
                {
                    //CheckingChecking=100;
                    command="poszłooooo";
                    YEAH_Resizig();
                    razchce=false;
                    //AnglesDetermination();
                }
            }
            return command;
        }
        public void Setting_XYZCorrectnessDictionary()
        {
            CheckingChecking=0;
            Index=15;
            razchce=true;
            if (XYZCorrectnessDictionary!=null)
            {
                XYZCorrectnessDictionary.Clear();
                foreach (var jointType in Enum.GetValues(typeof(JointType)))
                {
                    XYZCorrectnessDictionary.Add(jointType.ToString(), new Tuple<bool, bool, bool>(false, false, false));
                }
            }
            else
            {
                XYZCorrectnessDictionary=new Dictionary<string, Tuple<bool, bool, bool>>();
                foreach (var jointType in Enum.GetValues(typeof(JointType)))
                {
                    XYZCorrectnessDictionary.Add(jointType.ToString(), new Tuple<bool, bool, bool>(false, false, false));
                }
            }
        }

        public void BonesMeasurment(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student)
        {
            if (StudentBoneLength==null)
                StudentBoneLength=new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();
            else
                StudentBoneLength.Clear();

            if (TeacherBoneLength==null)
                TeacherBoneLength=new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();
            else
                TeacherBoneLength.Clear();

            foreach (var boneline in BonelLines.Keys)
            {
                Vector3 XYZ_lenght = Vector3.Subtract(Teacher[boneline.Item1.ToString()], Teacher[boneline.Item2.ToString()]);
                //Debug.WriteLine(boneline.Item1.ToString()+"-"+boneline.Item2.ToString()+"   vector  "+XYZ_lenght.ToString());
                float lenght = (float)Math.Sqrt(Math.Pow(XYZ_lenght.X, 2)+Math.Pow(XYZ_lenght.Y, 2)+Math.Pow(XYZ_lenght.Z, 2));
                TeacherBoneLength.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<Vector3, float>(XYZ_lenght, lenght));

                XYZ_lenght=Vector3.Subtract(Student[boneline.Item1.ToString()], Student[boneline.Item2.ToString()]);
                lenght=(float)Math.Sqrt(Math.Pow(XYZ_lenght.X, 2)+Math.Pow(XYZ_lenght.Y, 2)+Math.Pow(XYZ_lenght.Z, 2));
                StudentBoneLength.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<Vector3, float>(XYZ_lenght, lenght));
            }
        }

        public void YEAH_Resizig()
        {
            if (StudentBoneLength!=null&&TeacherBoneLength!=null)
            {
                if (DifferenceBetweenTeacherBoneLengthStudentBoneLength==null)
                    DifferenceBetweenTeacherBoneLengthStudentBoneLength=new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();
                else
                    DifferenceBetweenTeacherBoneLengthStudentBoneLength.Clear();

                foreach (var boneline in BonelLines.Keys)
                {
                    Vector3 bonelength_difference_vector = Vector3.Subtract(TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1, StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1);

                    float bonelength_difference = Math.Abs(TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2-StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2);

                    //Debug.WriteLine(boneline.Item1.ToString()+"-"+boneline.Item2.ToString()+"\t\t\t\t"+bonelength_difference);

                    DifferenceBetweenTeacherBoneLengthStudentBoneLength.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<Vector3, float>(bonelength_difference_vector, bonelength_difference));
                }
            }
        }
        public void AnglesDeterminationForHybrid()
        {

            if (TeacherBoneLength!=null&&StudentBoneLength!=null)
            {

                if (TeacherAnglesAndStudentNewBones==null)
                    TeacherAnglesAndStudentNewBones=new Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>>();
                else
                    TeacherAnglesAndStudentNewBones.Clear();

                foreach (var boneline in BonelLines.Keys)
                {
                    double beta = Math.Acos(( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Y )/( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 ));

                    //przekatna na płaszzyznie XZ
                    double Diagonal_XZ_Plane = TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);

                    double alpha = Math.Acos(( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.X )/( Diagonal_XZ_Plane ));
                    double gamma = Math.Acos(( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Z )/( Diagonal_XZ_Plane ));

                    double Y_length = Math.Cos(beta)*( StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 );
                    double Diagonal_XZ_PlaneForSubstractingPiece = StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
                    double X_length = Math.Cos(alpha)*( Diagonal_XZ_PlaneForSubstractingPiece );
                    double Z_length = Math.Cos(gamma)*( Diagonal_XZ_PlaneForSubstractingPiece );

                    //tylko do sprawdzenia _________________________________________________________________________________________________________________________________________
                    double checking_beta = Math.Acos(Y_length/StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2);
                    double checking_alpha = Math.Acos(X_length/Diagonal_XZ_PlaneForSubstractingPiece);
                    double checking_gamma = Math.Acos(Z_length/Diagonal_XZ_PlaneForSubstractingPiece);
                    //_____________________________________________________________________________________________________________________________________________________________________

                    float lenght = (float)Math.Sqrt(Math.Pow(X_length, 2)+Math.Pow(Y_length, 2)+Math.Pow(Z_length, 2));

                    TeacherAnglesAndStudentNewBones.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<double, double, double, Vector3>(alpha, beta, gamma, new Vector3((float)X_length, (float)Y_length, (float)Z_length)));

                    //sprawdzam czy katy sa te same  
                    Debug.WriteLine(boneline.Item1+"---"+boneline.Item2+" Kąty_kosci \t\t"+alpha+" ,  "+beta+" ,  "+gamma);
                    Debug.WriteLine(boneline.Item1+"---"+boneline.Item2+" Kąty_nowych kosci studenta "+checking_alpha+" ,  "+checking_beta+" ,  "+checking_gamma);
                    Debug.WriteLine(" length "+X_length+"  ,  "+Y_length+"  ,  "+Z_length+"  długosc  "+lenght);
                }
            }
        }
        public void AnglesDeterminationForStudent()
        {
            if (StudentBoneAnglesAndVectors==null)
                StudentBoneAnglesAndVectors=new Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>>();
            else
                StudentBoneAnglesAndVectors.Clear();

            foreach (var boneline in BonelLines.Keys)
            {
                double beta = Math.Acos(( StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Y )/( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 ));

                //przekatna na płaszzyznie XZ
                double Diagonal_XZ_Plane = StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);

                double alpha = Math.Acos(( StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.X )/( Diagonal_XZ_Plane ));
                double gamma = Math.Acos(( StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Z )/( Diagonal_XZ_Plane ));

                double Y_length = Math.Cos(beta)*( StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 );
                double Diagonal_XZ_PlaneForSubstractingPiece = StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
                double X_length = Math.Cos(alpha)*( Diagonal_XZ_PlaneForSubstractingPiece );
                double Z_length = Math.Cos(gamma)*( Diagonal_XZ_PlaneForSubstractingPiece );

                //tylko do sprawdzenia _________________________________________________________________________________________________________________________________________
                double checking_beta = Math.Acos(Y_length/StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2);
                double checking_alpha = Math.Acos(X_length/Diagonal_XZ_PlaneForSubstractingPiece);
                double checking_gamma = Math.Acos(Z_length/Diagonal_XZ_PlaneForSubstractingPiece);
                //_____________________________________________________________________________________________________________________________________________________________________

                float lenght = (float)Math.Sqrt(Math.Pow(X_length, 2)+Math.Pow(Y_length, 2)+Math.Pow(Z_length, 2));

                StudentBoneAnglesAndVectors.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<double, double, double, Vector3>(alpha, beta, gamma, new Vector3((float)X_length, (float)Y_length, (float)Z_length)));

                //sprawdzam czy katy sa te same  
                Debug.WriteLine(boneline.Item1+"---"+boneline.Item2+" Kąty_kosci \t\t"+alpha+" ,  "+beta+" ,  "+gamma);
                Debug.WriteLine(boneline.Item1+"---"+boneline.Item2+" Kąty_nowych kosci studenta "+checking_alpha+" ,  "+checking_beta+" ,  "+checking_gamma);
                Debug.WriteLine(" length "+X_length+"  ,  "+Y_length+"  ,  "+Z_length+"  długosc  "+lenght);

            }
        }


        public string AnglesBetweenBonesCommands(Dictionary<string, Vector3> Person, string WhichPerson)
        {
            if (StudentAnglesBetweenBones==null&&TeacherAnglesBetweenBones!=null)
                StudentAnglesBetweenBones=new Dictionary<Tuple<string, string, string>, double>(TeacherAnglesBetweenBones);
            else if (TeacherAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones.Clear();
                StudentAnglesBetweenBones=new Dictionary<Tuple<string, string, string>, double>(TeacherAnglesBetweenBones);
            }
            string command = string.Empty;
            if (WhichPerson=="Student")
            {
                command+="KĄTY          DLA        STUDENTA";
                command+=Environment.NewLine;
            }
            if (WhichPerson=="Teacher")
            {
                command+="KĄTY          DLA        NAUCZYCIELA";
                command+=Environment.NewLine;
            }
            //uda
            Vector3 Person_I_Bone = Vector3.Subtract(Person[JointType.HipLeft.ToString()], Person[JointType.KneeLeft.ToString()]);
            Vector3 Person_II_Bone = Vector3.Subtract(Person[JointType.HipRight.ToString()], Person[JointType.KneeRight.ToString()]);
            //Iloczyn skalarny 
            float DotProduct = Vector3.Dot(Person_I_Bone, Person_II_Bone);
            double Person_I_Bone_lenght = Math.Sqrt(Math.Pow(Person_I_Bone.X, 2)+Math.Pow(Person_I_Bone.Y, 2)+Math.Pow(Person_I_Bone.Z, 2));
            double Person_II_Bone_lenght = Math.Sqrt(Math.Pow(Person_II_Bone.X, 2)+Math.Pow(Person_II_Bone.Y, 2)+Math.Pow(Person_II_Bone.Z, 2));
            double AngleBetweenBones = Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            command+="kąt pomiedzy udami = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";
            command+=Environment.NewLine;

            command+="lewa strona-----------------------------------------";
            //-----------------------------------------lewa strona------------------------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a lewy bark
            Vector3 cervical_spine = Vector3.Subtract(Person[JointType.SpineShoulder.ToString()], Person[JointType.SpineMid.ToString()]);
            Vector3 Lshoulder = Vector3.Subtract(Person[JointType.SpineShoulder.ToString()], Person[JointType.ShoulderLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(cervical_spine, Lshoulder);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(cervical_spine.X, 2)+Math.Pow(cervical_spine.Y, 2)+Math.Pow(cervical_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lshoulder.X, 2)+Math.Pow(Lshoulder.Y, 2)+Math.Pow(Lshoulder.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "cervical_spine", "Lshoulder")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy lewym barkiem a górną częścią kręgosłupa  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //bark-ramię
            Vector3 LshoulderNegate = Vector3.Negate(Lshoulder);
            Vector3 Larm = Vector3.Subtract(Person[JointType.ShoulderLeft.ToString()], Person[JointType.ElbowLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LshoulderNegate, Larm);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LshoulderNegate.X, 2)+Math.Pow(LshoulderNegate.Y, 2)+Math.Pow(LshoulderNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Larm.X, 2)+Math.Pow(Larm.Y, 2)+Math.Pow(Larm.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "LshoulderNegate", "Larm")]=AngleBetweenBones;
            }

            command+=Environment.NewLine;
            command+="kąt pomiedzy lewym barkiem a ramieniem  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Lhumerus = Vector3.Subtract(Person[JointType.ElbowLeft.ToString()], Person[JointType.ShoulderLeft.ToString()]);
            Vector3 Lulna = Vector3.Subtract(Person[JointType.ElbowLeft.ToString()], Person[JointType.WristLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Lhumerus, Lulna);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(Lhumerus.X, 2)+Math.Pow(Lhumerus.Y, 2)+Math.Pow(Lhumerus.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lulna.X, 2)+Math.Pow(Lulna.Y, 2)+Math.Pow(Lulna.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "Lhumerus", "Lulna")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy lewym ramieniem a koscia łokciowa  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //lewe biodro a dolna czesc kregosłupa (lumbar spine)
            Vector3 lumbar_spine = Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.SpineMid.ToString()]);
            Vector3 Lhip = Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(lumbar_spine, Lhip);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(lumbar_spine.X, 2)+Math.Pow(lumbar_spine.Y, 2)+Math.Pow(lumbar_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lhip.X, 2)+Math.Pow(Lhip.Y, 2)+Math.Pow(Lhip.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "lumbar_spine", "Lhip")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy dolną częścią kręgosłupa a lewym biodrem = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //lewe biodro a lewe udo 
            Vector3 LhipNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipLeft.ToString()]));
            Vector3 LThigh = Vector3.Subtract(Person[JointType.HipLeft.ToString()], Person[JointType.KneeLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LhipNegate, LThigh);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LhipNegate.X, 2)+Math.Pow(LhipNegate.Y, 2)+Math.Pow(LhipNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(LThigh.X, 2)+Math.Pow(LThigh.Y, 2)+Math.Pow(LThigh.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "LhipNegate", "LThigh")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy lewym biodrem a lewym udem  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //lewe udo a lewy piszczel
            Vector3 LThighNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.HipLeft.ToString()], Person[JointType.KneeLeft.ToString()]));
            Vector3 LTibia = Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LThighNegate, LTibia);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LThighNegate.X, 2)+Math.Pow(LThighNegate.Y, 2)+Math.Pow(LThighNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(LTibia.X, 2)+Math.Pow(LTibia.Y, 2)+Math.Pow(LTibia.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "LThighNegate", "LTibia")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy lewe udo a lewy piszczel = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //lewy piszczel a lewa stopa
            Vector3 LTibiaNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleLeft.ToString()]));
            Vector3 Lfoot = Vector3.Subtract(Person[JointType.AnkleLeft.ToString()], Person[JointType.FootLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LTibiaNegate, Lfoot);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LTibiaNegate.X, 2)+Math.Pow(LTibiaNegate.Y, 2)+Math.Pow(LTibiaNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lfoot.X, 2)+Math.Pow(Lfoot.Y, 2)+Math.Pow(Lfoot.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "LTibiaNegate", "Lfoot")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzylewy piszczel a lewa stopa= "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";




            //---------------------------------------------------prawa-strona ----------------------------------------------------------------------------------------------------------
            command+=Environment.NewLine;
            command+=Environment.NewLine;

            command+="--prawa-strona ---------------------------------------------------";
            //górna cześć kręgosłupa( cervical spine ) a prawy bark
            Vector3 Rshoulder = Vector3.Subtract(Person[JointType.SpineShoulder.ToString()], Person[JointType.ShoulderRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(cervical_spine, Rshoulder);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(cervical_spine.X, 2)+Math.Pow(cervical_spine.Y, 2)+Math.Pow(cervical_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rshoulder.X, 2)+Math.Pow(Rshoulder.Y, 2)+Math.Pow(Rshoulder.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "cervical_spine", "Rshoulder")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy prawym barkiem a górną częścią kręgosłupa  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //bark-ramię
            Vector3 RshoulderNegate = Vector3.Negate(Rshoulder);
            Vector3 Rarm = Vector3.Subtract(Person[JointType.ShoulderRight.ToString()], Person[JointType.ElbowRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RshoulderNegate, Rarm);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RshoulderNegate.X, 2)+Math.Pow(RshoulderNegate.Y, 2)+Math.Pow(RshoulderNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rarm.X, 2)+Math.Pow(Rarm.Y, 2)+Math.Pow(Rarm.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "RshoulderNegate", "Rarm")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy prawym barkiem a ramieniem  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Rhumerus = Vector3.Subtract(Person[JointType.ElbowRight.ToString()], Person[JointType.ShoulderRight.ToString()]);
            Vector3 Rulna = Vector3.Subtract(Person[JointType.ElbowRight.ToString()], Person[JointType.WristRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Rhumerus, Rulna);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(Rhumerus.X, 2)+Math.Pow(Rhumerus.Y, 2)+Math.Pow(Rhumerus.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rulna.X, 2)+Math.Pow(Rulna.Y, 2)+Math.Pow(Rulna.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "Rhumerus", "Rulna")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy prawym ramieniem a koscia łokciowa  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //prawe biodro a dolna czesc kregosłupa (lumbar spine)
            Vector3 Rhip = Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(lumbar_spine, Rhip);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(lumbar_spine.X, 2)+Math.Pow(lumbar_spine.Y, 2)+Math.Pow(lumbar_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rhip.X, 2)+Math.Pow(Rhip.Y, 2)+Math.Pow(Rhip.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "lumbar_spine", "Rhip")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy dolną częścią kręgosłupa a prawym biodrem = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //prawe biodro a lewe udo 
            Vector3 RhipNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipRight.ToString()]));
            Vector3 RThigh = Vector3.Subtract(Person[JointType.HipRight.ToString()], Person[JointType.KneeRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RhipNegate, RThigh);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RhipNegate.X, 2)+Math.Pow(RhipNegate.Y, 2)+Math.Pow(RhipNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(RThigh.X, 2)+Math.Pow(RThigh.Y, 2)+Math.Pow(RThigh.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "RhipNegate", "RThigh")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy prawym biodrem a lewym udem  = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //lewe udo a lewy piszczel
            Vector3 RThighNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.HipRight.ToString()], Person[JointType.KneeRight.ToString()]));
            Vector3 RTibia = Vector3.Subtract(Person[JointType.KneeRight.ToString()], Person[JointType.AnkleRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RThighNegate, RTibia);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RThighNegate.X, 2)+Math.Pow(RThighNegate.Y, 2)+Math.Pow(RThighNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(RTibia.X, 2)+Math.Pow(RTibia.Y, 2)+Math.Pow(RTibia.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "RThighNegate", "RTibia")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy prawym udem a lewym piszczelem = "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            //lewy piszczel a lewa stopa
            Vector3 RTibiaNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleRight.ToString()]));
            Vector3 Rfoot = Vector3.Subtract(Person[JointType.AnkleRight.ToString()], Person[JointType.FootRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RTibiaNegate, Rfoot);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RTibiaNegate.X, 2)+Math.Pow(RTibiaNegate.Y, 2)+Math.Pow(RTibiaNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rfoot.X, 2)+Math.Pow(Rfoot.Y, 2)+Math.Pow(Rfoot.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            if (StudentAnglesBetweenBones!=null)
            {
                StudentAnglesBetweenBones[new Tuple<string, string, string>("left", "RTibiaNegate", "Rfoot")]=AngleBetweenBones;
            }
            command+=Environment.NewLine;
            command+="kąt pomiedzy prawym piszczel a lewa stopa= "+Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0)+" stopni";

            return command;
        }

        public void Make_TeacheAnglesBetweenBonesDictionary(Dictionary<string, Vector3> Person)
        {
            if (TeacherAnglesBetweenBones==null)
                TeacherAnglesBetweenBones=new Dictionary<Tuple<string, string, string>, double>();
            else
                TeacherAnglesBetweenBones.Clear();

            //-----------------------------------------lewa strona------------------------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a lewy bark
            Vector3 cervical_spine = Vector3.Subtract(Person[JointType.SpineShoulder.ToString()], Person[JointType.SpineMid.ToString()]);
            Vector3 Lshoulder = Vector3.Subtract(Person[JointType.SpineShoulder.ToString()], Person[JointType.ShoulderLeft.ToString()]);
            //Iloczyn skalarny 
            float DotProduct = Vector3.Dot(cervical_spine, Lshoulder);
            double Person_I_Bone_lenght = Math.Sqrt(Math.Pow(cervical_spine.X, 2)+Math.Pow(cervical_spine.Y, 2)+Math.Pow(cervical_spine.Z, 2));
            double Person_II_Bone_lenght = Math.Sqrt(Math.Pow(Lshoulder.X, 2)+Math.Pow(Lshoulder.Y, 2)+Math.Pow(Lshoulder.Z, 2));
            double AngleBetweenBones = Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "cervical_spine", "Lshoulder"), AngleBetweenBones);


            //bark-ramię
            Vector3 LshoulderNegate = Vector3.Negate(Lshoulder);
            Vector3 Larm = Vector3.Subtract(Person[JointType.ShoulderLeft.ToString()], Person[JointType.ElbowLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LshoulderNegate, Larm);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LshoulderNegate.X, 2)+Math.Pow(LshoulderNegate.Y, 2)+Math.Pow(LshoulderNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Larm.X, 2)+Math.Pow(Larm.Y, 2)+Math.Pow(Larm.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "LshoulderNegate", "Larm"), AngleBetweenBones);


            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Lhumerus = Vector3.Subtract(Person[JointType.ElbowLeft.ToString()], Person[JointType.ShoulderLeft.ToString()]);
            Vector3 Lulna = Vector3.Subtract(Person[JointType.ElbowLeft.ToString()], Person[JointType.WristLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Lhumerus, Lulna);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(Lhumerus.X, 2)+Math.Pow(Lhumerus.Y, 2)+Math.Pow(Lhumerus.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lulna.X, 2)+Math.Pow(Lulna.Y, 2)+Math.Pow(Lulna.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "Lhumerus", "Lulna"), AngleBetweenBones);


            //lewe biodro a dolna czesc kregosłupa (lumbar spine)
            Vector3 lumbar_spine = Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.SpineMid.ToString()]);
            Vector3 Lhip = Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(lumbar_spine, Lhip);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(lumbar_spine.X, 2)+Math.Pow(lumbar_spine.Y, 2)+Math.Pow(lumbar_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lhip.X, 2)+Math.Pow(Lhip.Y, 2)+Math.Pow(Lhip.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "lumbar_spine", "Lhip"), AngleBetweenBones);


            //lewe biodro a lewe udo 
            Vector3 LhipNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipLeft.ToString()]));
            Vector3 LThigh = Vector3.Subtract(Person[JointType.HipLeft.ToString()], Person[JointType.KneeLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LhipNegate, LThigh);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LhipNegate.X, 2)+Math.Pow(LhipNegate.Y, 2)+Math.Pow(LhipNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(LThigh.X, 2)+Math.Pow(LThigh.Y, 2)+Math.Pow(LThigh.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "LhipNegate", "LThigh"), AngleBetweenBones);



            //lewe udo a lewy piszczel
            Vector3 LThighNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.HipLeft.ToString()], Person[JointType.KneeLeft.ToString()]));
            Vector3 LTibia = Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LThighNegate, LTibia);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LThighNegate.X, 2)+Math.Pow(LThighNegate.Y, 2)+Math.Pow(LThighNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(LTibia.X, 2)+Math.Pow(LTibia.Y, 2)+Math.Pow(LTibia.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "LThighNegate", "LTibia"), AngleBetweenBones);

            //lewy piszczel a lewa stopa
            Vector3 LTibiaNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleLeft.ToString()]));
            Vector3 Lfoot = Vector3.Subtract(Person[JointType.AnkleLeft.ToString()], Person[JointType.FootLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(LTibiaNegate, Lfoot);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(LTibiaNegate.X, 2)+Math.Pow(LTibiaNegate.Y, 2)+Math.Pow(LTibiaNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Lfoot.X, 2)+Math.Pow(Lfoot.Y, 2)+Math.Pow(Lfoot.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("left", "LTibiaNegate", "Lfoot"), AngleBetweenBones);

            //---------------------------------------------------prawa-strona ----------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a prawy bark
            Vector3 Rshoulder = Vector3.Subtract(Person[JointType.SpineShoulder.ToString()], Person[JointType.ShoulderRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(cervical_spine, Rshoulder);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(cervical_spine.X, 2)+Math.Pow(cervical_spine.Y, 2)+Math.Pow(cervical_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rshoulder.X, 2)+Math.Pow(Rshoulder.Y, 2)+Math.Pow(Rshoulder.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "cervical_spine", "Rshoulder"), AngleBetweenBones);

            //bark-ramię
            Vector3 RshoulderNegate = Vector3.Subtract(Person[JointType.ShoulderRight.ToString()], Person[JointType.SpineShoulder.ToString()]);
            Vector3 Rarm = Vector3.Subtract(Person[JointType.ShoulderRight.ToString()], Person[JointType.ElbowRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RshoulderNegate, Rarm);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RshoulderNegate.X, 2)+Math.Pow(RshoulderNegate.Y, 2)+Math.Pow(RshoulderNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rarm.X, 2)+Math.Pow(Rarm.Y, 2)+Math.Pow(Rarm.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "RshoulderNegate", "Rarm"), AngleBetweenBones);

            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Rhumerus = Vector3.Subtract(Person[JointType.ElbowRight.ToString()], Person[JointType.ShoulderRight.ToString()]);
            Vector3 Rulna = Vector3.Subtract(Person[JointType.ElbowRight.ToString()], Person[JointType.WristRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Rhumerus, Rulna);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(Rhumerus.X, 2)+Math.Pow(Rhumerus.Y, 2)+Math.Pow(Rhumerus.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rulna.X, 2)+Math.Pow(Rulna.Y, 2)+Math.Pow(Rulna.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "Rhumerus", "Rulna"), AngleBetweenBones);

            //prawe biodro a dolna czesc kregosłupa (lumbar spine)
            Vector3 Rhip = Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(lumbar_spine, Rhip);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(lumbar_spine.X, 2)+Math.Pow(lumbar_spine.Y, 2)+Math.Pow(lumbar_spine.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rhip.X, 2)+Math.Pow(Rhip.Y, 2)+Math.Pow(Rhip.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "lumbar_spine", "Rhip"), AngleBetweenBones);

            //prawe biodro a lewe udo 
            Vector3 RhipNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.SpineBase.ToString()], Person[JointType.HipRight.ToString()]));
            Vector3 RThigh = Vector3.Subtract(Person[JointType.HipRight.ToString()], Person[JointType.KneeRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RhipNegate, RThigh);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RhipNegate.X, 2)+Math.Pow(RhipNegate.Y, 2)+Math.Pow(RhipNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(RThigh.X, 2)+Math.Pow(RThigh.Y, 2)+Math.Pow(RThigh.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "RhipNegate", "RThigh"), AngleBetweenBones);

            //lewe udo a lewy piszczel
            Vector3 RThighNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.HipRight.ToString()], Person[JointType.KneeRight.ToString()]));
            Vector3 RTibia = Vector3.Subtract(Person[JointType.KneeRight.ToString()], Person[JointType.AnkleRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RThighNegate, RTibia);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RThighNegate.X, 2)+Math.Pow(RThighNegate.Y, 2)+Math.Pow(RThighNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(RTibia.X, 2)+Math.Pow(RTibia.Y, 2)+Math.Pow(RTibia.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "RThighNegate", "RTibia"), AngleBetweenBones);

            //lewy piszczel a lewa stopa
            Vector3 RTibiaNegate = Vector3.Negate(Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleRight.ToString()]));
            Vector3 Rfoot = Vector3.Subtract(Person[JointType.AnkleRight.ToString()], Person[JointType.FootRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(RTibiaNegate, Rfoot);
            Person_I_Bone_lenght=Math.Sqrt(Math.Pow(RTibiaNegate.X, 2)+Math.Pow(RTibiaNegate.Y, 2)+Math.Pow(RTibiaNegate.Z, 2));
            Person_II_Bone_lenght=Math.Sqrt(Math.Pow(Rfoot.X, 2)+Math.Pow(Rfoot.Y, 2)+Math.Pow(Rfoot.Z, 2));
            AngleBetweenBones=Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            TeacherAnglesBetweenBones.Add(new Tuple<string, string, string>("right", "RTibiaNegate", "Rfoot"), AngleBetweenBones);
        }


        public string AnglesComparison(double accuracy)
        {
            string command = string.Empty;
            command+="PORÓWNYWANIE KĄTÓW";
            command+=Environment.NewLine;
            command+=Environment.NewLine;

            if (StudentAnglesBetweenBones==null)
                return command;

            if (StudentAnglesBetweenBonesBools==null)
            {
                StudentAnglesBetweenBonesBools=new Dictionary<Tuple<string, string, string>, bool>();
                foreach (var item in TeacherAnglesBetweenBones)
                {
                    StudentAnglesBetweenBonesBools.Add(new Tuple<string, string, string>(item.Key.Item1, item.Key.Item2, item.Key.Item3), false);
                }
            }
            foreach (var item in TeacherAnglesBetweenBones)
            {
                Debug.WriteLine(Math.Abs(MathUtil.RadiansToDegrees((float)( StudentAnglesBetweenBones[new Tuple<string, string, string>(item.Key.Item1, item.Key.Item2, item.Key.Item3)]-item.Value ))));
                if (Math.Abs(MathUtil.RadiansToDegrees((float)( StudentAnglesBetweenBones[new Tuple<string, string, string>(item.Key.Item1, item.Key.Item2, item.Key.Item3)]-item.Value )))<accuracy)
                {
                    StudentAnglesBetweenBonesBools[new Tuple<string, string, string>(item.Key.Item1, item.Key.Item2, item.Key.Item3)]=true;
                }
            }
            foreach (var item in StudentAnglesBetweenBonesBools)
            {
                if (item.Value==false)
                {
                    command+=item.Key.ToString()+" NIE jest ok";
                    command+=Environment.NewLine;
                }
                if (item.Value==true)
                {
                    command+=item.Key.ToString()+" jest ok";
                    command+=Environment.NewLine;
                }
            }
            foreach (var item in StudentAnglesBetweenBonesBools)
            {
                if (item.Value==false)
                {
                    command+=Environment.NewLine;
                    command+=Environment.NewLine;
                    command+=Environment.NewLine;
                    command+=item.Key.ToString()+" NIE jest ok"+"     Nie jestes ustawiony";
                    return command;
                }
            }
            command+=Environment.NewLine;
            command+=Environment.NewLine;
            command+=Environment.NewLine;
            command+="JESTES USTAWIONY";
            StudentAnglesBetweenBonesBools.Clear();
            foreach (var item in TeacherAnglesBetweenBones)
            {
                StudentAnglesBetweenBonesBools.Add(new Tuple<string, string, string>(item.Key.Item1, item.Key.Item2, item.Key.Item3), false);
            }
            SecondPage.move_to_next_position=true;
            return command;
        }
        public string AnglesComparisonTheSecondV(Dictionary<string, Vector3> Teacher, Vector3 TeacherCenterOfGravity, Dictionary<string, Vector3> Student, Vector3 StudentCenterOfGravity, float accuracy)
        {
            string command = string.Empty;
            switch (WhatToDo)
            {
                case ( "Ustawiaj_Nogi" ):
                    {
                        if (once_gone)
                        {
                            Dictionary<string, bool> PartialDictionaryPositioningBool = new Dictionary<string, bool>();
                            PartialDictionaryPositioningBool.Add("FootUnderShoulders", false);
                            PartialDictionaryPositioningBool.Add("AngleBetweenThigs", false);
                            PositioningBool.Add("Ustawiaj_Nogi", new Tuple<Dictionary<string, bool>, bool>(PartialDictionaryPositioningBool, false));
                            once_gone=false;
                        }
                        command+="Ustawianie Nóg";
                        command+=Environment.NewLine;
                        command+="Przsuń stopy na wysokość barków";
                        command+=Environment.NewLine;

                        //przesuniecie nóg na wysokość barków;
                        command+=FootUnderShoulders(Student, accuracy);

                        //ustawianie odpowiedniego kąta miedzy nogami;
                        command+=AngleBetweenThigs(Student, 10, 30);

                        if (PositioningBool["Ustawiaj_Nogi"].Item1["FootUnderShoulders"]==true&&PositioningBool["Ustawiaj_Nogi"].Item1["AngleBetweenThigs"]==true)
                        {
                            PositioningBool["Ustawiaj_Nogi"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Ustawiaj_Nogi"].Item1, true);
                        }

                        if (PositioningBool["Ustawiaj_Nogi"].Item2==true)
                        {
                            WhatToDo="Prawa_stopa";
                            once_gone=true;
                        }
                    }
                    break;
                case ( "Prawa_stopa" ):
                    {
                        command="";
                        if (once_gone)
                        {
                            Dictionary<string, bool> PartialDictionaryPositioningBool = new Dictionary<string, bool>();
                            PartialDictionaryPositioningBool.Add("RightFoodPositioning", false);
                            PositioningBool.Add("Prawa_stopa", new Tuple<Dictionary<string, bool>, bool>(PartialDictionaryPositioningBool, false));
                            once_gone=false;
                        }
                        command+=RightFoodPositioning(Student, 3);

                        if (PositioningBool["Prawa_stopa"].Item1["RightFoodPositioning"]==true)
                        {
                            PositioningBool["Prawa_stopa"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Prawa_stopa"].Item1, true);
                        }

                        if (PositioningBool["Prawa_stopa"].Item2==true)
                        {
                            WhatToDo="Kat_w_kolanach";
                            once_gone=true;
                        }
                    }
                    break;
                case ( "Kat_w_kolanach" ):
                    {
                        command="";
                        if (once_gone)
                        {
                            Dictionary<string, bool> PartialDictionaryPositioningBool = new Dictionary<string, bool>();
                            PartialDictionaryPositioningBool.Add("KneeAnglesComparison", false);
                            PositioningBool.Add("Kat_w_kolanach", new Tuple<Dictionary<string, bool>, bool>(PartialDictionaryPositioningBool, false));
                            once_gone=false;
                        }
                        command+="ustawianie kata w kolanach ";
                        command+=Environment.NewLine;
                        //ustawianie kata w kolanach
                        command+=KneeAnglesComparison(Student, 10);
                        if (PositioningBool["Kat_w_kolanach"].Item1["KneeAnglesComparison"]==true)
                        {
                            PositioningBool["Kat_w_kolanach"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Kat_w_kolanach"].Item1, true);
                        }
                        if (PositioningBool["Kat_w_kolanach"].Item2==true)
                        {
                            WhatToDo="Srodek_Ciezkosci";
                            once_gone=true;
                        }
                    }
                    break;
                case ( "Srodek_Ciezkosci" ):
                    {
                        command=string.Empty;
                        if (once_gone)
                        {
                            Dictionary<string, bool> PartialDictionaryPositioningBool = new Dictionary<string, bool>();
                            PartialDictionaryPositioningBool.Add("CenterOfGravityCommands", false);
                            PositioningBool.Add("Srodek_Ciezkosci", new Tuple<Dictionary<string, bool>, bool>(PartialDictionaryPositioningBool, false));
                            once_gone=false;
                        }
                        command+=Environment.NewLine;
                        command+=CenterOfGravityCommands(Teacher, TeacherCenterOfGravity, Student, StudentCenterOfGravity, 5);
                        if (PositioningBool["Srodek_Ciezkosci"].Item1["CenterOfGravityCommands"]==true)
                        {
                            PositioningBool["Srodek_Ciezkosci"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Srodek_Ciezkosci"].Item1, true);
                        }
                        if (PositioningBool["Srodek_Ciezkosci"].Item2==true)
                        {
                            WhatToDo="Ustawianie_Rak";
                            once_gone=true;
                        }
                    }
                    break;
                case ( "Ustawianie_Rak" ):
                    {
                        command=string.Empty;
                        command="Ustawianie_rąk";
                        command+=Environment.NewLine;
                        command+="Przycisnij łokcie do boków tak żeby je zakrywały !!!";
                        command+=Environment.NewLine;
                        command+="Wewnętrzne części pięści muszą być 'przyklejone' do policzków!!!";
                        command+=Environment.NewLine;
                        command+="";
                        if (once_gone)
                        {
                            Dictionary<string, bool> PartialDictionaryPositioningBool = new Dictionary<string, bool>();
                            PartialDictionaryPositioningBool.Add("HandsPositioning", false);
                            PositioningBool.Add("Ustawianie_Rak", new Tuple<Dictionary<string, bool>, bool>(PartialDictionaryPositioningBool, false));
                            once_gone=false;
                        }
                        command+=HandsPositioning(Teacher, Student, 15);
                        if (PositioningBool["Ustawianie_Rak"].Item1["HandsPositioning"]==true)
                        {
                            PositioningBool["Ustawianie_Rak"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Ustawianie_Rak"].Item1, true);
                        }
                        if (PositioningBool["Ustawianie_Rak"].Item2==true)
                        {
                            WhatToDo="Wyprowadzenie_ciosu_przejscie";
                            once_gone=true;
                        }
                    }
                    break;
                case ( "Wyprowadzenie_ciosu_przejscie" ):
                    {
                        command=string.Empty;
                        command="Pozycja statyczna została ustawiona poprawnie";
                        command+=Environment.NewLine;
                        command+="Za chwile przejdziesz do instruktora wyprowadzania ciosu->(lewy prosty) :)";
                        command+=Environment.NewLine;
                        command+="POZOSTAN TAK USTAWIONY";
                        if (!Timers.Timer_start)
                        {
                            Timer.Start_timer();
                        }
                        if (Timers.I_can_not_go_further)
                            break;
                        WhatToDo="Wyprowadzenie_ciosu";
                    }
                    break;
                case ( "Wyprowadzenie_ciosu" ):
                    {
                        //if (Measurement.wait_bool)
                        //{
                        //    new System.Threading.ManualResetEvent(false).WaitOne(Measurement.milliseconds);
                        //    Measurement.wait_bool=false;
                        //}
                        command=string.Empty;
                        command="WYPROWADZANIE LEWEGO PROSTEGO ";
                        if (once_gone)
                        {
                            Dictionary<string, bool> PartialDictionaryPositioningBool = new Dictionary<string, bool>();
                            PartialDictionaryPositioningBool.Add("RightKneeDeflection", false);
                            PartialDictionaryPositioningBool.Add("HipsTwist", false);
                            PartialDictionaryPositioningBool.Add("StraightLeftPunchAngles", false);
                            PartialDictionaryPositioningBool.Add("StraightLeftPunchHeight", false);
                            PositioningBool.Add("Wyprowadzenie_ciosu", new Tuple<Dictionary<string, bool>, bool>(PartialDictionaryPositioningBool, false));
                            once_gone=false;
                        }
                        if (ShowMovementOnTeacher_BOOL_ONCE==true)
                        {
                            ShowMovementOnTeacher(112, 120);
                            ShowMovementOnTeacher_BOOL_ONCE=false;
                        }
                        if (SecondPage.TeacherActualPosition==SecondPage.TeacherEndPosition)
                        {
                            if (SetStartingPosition_BOOL_ONCE)
                            {
                                SetStartingPosition(111);
                                SetStartingPosition_BOOL_ONCE=false;
                                StartStudentRecording();

                            }
                        }
                        break;
                        //--------------------------------------------------------------------------------------------------------------------------------------------------------
                        if (PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]==false)
                        {
                            command=string.Empty;
                            command="WYPROWADZANIE LEWEGO PROSTEGO ";

                            command+="1. "+RightKneeDeflection(Teacher, Student, 20);


                        }
                        else if (!( PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]==false ))
                        {
                            command=string.Empty;
                            command="WYPROWADZANIE LEWEGO PROSTEGO ";
                            command+="1. Kąt w kolanie poprawny";

                        }

                        if (PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]==false&&PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]==true)
                        {
                            command=string.Empty;
                            command="WYPROWADZANIE LEWEGO PROSTEGO ";
                            command+=HipsTwist(Teacher, Student, 15);

                        }
                        else if (( PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]==false&&PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]==true ))
                        {
                            command=string.Empty;
                            command="WYPROWADZANIE LEWEGO PROSTEGO ";
                            command+="2. Biodro na dobrej pozycji";
                            // Set is never called, so we wait always until the timeout occurs

                        }

                        if (PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchAngles"]==false&&PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]==true)
                        {
                            command=string.Empty;
                            command="WYPROWADZANIE LEWEGO PROSTEGO ";
                            command+=StraightLeftPunchAngles(Teacher, Student, 20);

                        }
                        else if (( PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchAngles"]==false&&PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]==true ))
                        {
                            command=string.Empty;
                            command="WYPROWADZANIE LEWEGO PROSTEGO ";
                            command+="3. Kąt w LEWYM łokciu poprawny";

                        }
                        //command+=StraightLeftPunchHeight(Teacher, Student, 0.05f);
                        command+=Environment.NewLine;
                        command+=Environment.NewLine;
                        command+=Environment.NewLine;
                        command+=SecondPage.SelectedIndex;
                        if (PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]==true&&PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]==true&&PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchAngles"]==true)
                        {
                            PositioningBool["Wyprowadzenie_ciosu"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Wyprowadzenie_ciosu"].Item1, true);
                        }

                        if (PositioningBool["Wyprowadzenie_ciosu"].Item2==true)
                        {
                            //WhatToDo="Wyprowadzenie_ciosu";
                            command+=Environment.NewLine;
                            command+="PRZECHODZISZ DO NASTĘPNEJ POZYCJI";

                            PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]=PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchAngles"]=false;
                            PositioningBool["Wyprowadzenie_ciosu"]=new Tuple<Dictionary<string, bool>, bool>(PositioningBool["Wyprowadzenie_ciosu"].Item1, false);
                            SecondPage.move_to_next_position=true;
                            WhatToDo="Wyprowadzenie_ciosu";

                        }
                    }
                    break;
                default:
                    break;
            }
            return command;
        }

        public Vector3 DeterminationOfTheCenterOfGravity(Dictionary<string, Vector3> Person)
        {
            Vector3[] Contener = new Vector3[BonelLines.Count-10];
            int i = 0;
            float X_coordinate_of_center = 0;
            float Y_coordinate_of_center = 0;
            float Z_coordinate_of_center = 0;
            foreach (var bonelLines in BonelLines)
            {
                if (bonelLines.Key.Item1==JointType.ShoulderRight&&bonelLines.Key.Item2==JointType.ElbowRight
                         ||bonelLines.Key.Item1==JointType.ElbowRight&&bonelLines.Key.Item2==JointType.WristRight
                         ||bonelLines.Key.Item1==JointType.WristRight&&bonelLines.Key.Item2==JointType.HandRight
                         ||bonelLines.Key.Item1==JointType.HandRight&&bonelLines.Key.Item2==JointType.HandTipRight
                         ||bonelLines.Key.Item1==JointType.WristRight&&bonelLines.Key.Item2==JointType.ThumbRight
                         ||bonelLines.Key.Item1==JointType.ShoulderLeft&&bonelLines.Key.Item2==JointType.ElbowLeft
                         ||bonelLines.Key.Item1==JointType.ElbowLeft&&bonelLines.Key.Item2==JointType.WristLeft
                         ||bonelLines.Key.Item1==JointType.WristLeft&&bonelLines.Key.Item2==JointType.HandLeft
                         ||bonelLines.Key.Item1==JointType.HandLeft&&bonelLines.Key.Item2==JointType.HandTipLeft
                         ||bonelLines.Key.Item1==JointType.WristLeft&&bonelLines.Key.Item2==JointType.ThumbLeft)
                    continue;

                var subtracted = Vector3.Subtract(Person[bonelLines.Key.Item1.ToString()], Person[bonelLines.Key.Item2.ToString()]);
                var divided = Vector3.Divide(subtracted, 2);
                var added = Vector3.Add(Person[bonelLines.Key.Item2.ToString()], divided);
                Contener[i]=added;
                i++;
            }
            for (int j = 0; j<Contener.Length; j++)
            {
                X_coordinate_of_center+=Contener[j].X;
                Y_coordinate_of_center+=Contener[j].Y;
                Z_coordinate_of_center+=Contener[j].Z;
            }
            X_coordinate_of_center=X_coordinate_of_center/Contener.Length;
            Y_coordinate_of_center=Y_coordinate_of_center/Contener.Length;
            Z_coordinate_of_center=Z_coordinate_of_center/Contener.Length;
            return ( new Vector3(X_coordinate_of_center, Y_coordinate_of_center, Z_coordinate_of_center) );
        }



















        //--do ustawiania człowieczka --------------------------------------------------------------------------------------------//
        private string FootUnderShoulders(Dictionary<string, Vector3> Student, double accuracy)
        {
            bool left_side_correct = false;
            bool right_side_correct = false;
            string command = string.Empty;
            var AnkleLeft = Student[JointType.AnkleLeft.ToString()];
            var AnkleRight = Student[JointType.AnkleRight.ToString()];
            var ShoulderLeft = Student[JointType.ShoulderLeft.ToString()];
            var ShoulderRight = Student[JointType.ShoulderRight.ToString()];
            float min = 0;
            if (AnkleLeft.X<0||AnkleRight.X<0||ShoulderLeft.X<0||ShoulderRight.X<0)
            {
                min=Math.Min(Math.Min(AnkleLeft.X, AnkleRight.X), Math.Min(ShoulderLeft.X, ShoulderRight.X));
                //command+=Environment.NewLine;
                //command+="min"+min;
            }
            if (min<0)
            {
                min=Math.Abs(min)+0.5f;
                AnkleLeft.X+=min;
                AnkleRight.X+=min;
                ShoulderLeft.X+=min;
                ShoulderRight.X+=min;
                //command+=Environment.NewLine;
                //command+="AnkleLeft.X"+AnkleLeft.X;
                //command+=Environment.NewLine;
                //command+="AnkleRight.X"+AnkleRight.X;
                //command+=Environment.NewLine;
                //command+="ShoulderLeft.X"+ShoulderLeft.X;
                //command+=Environment.NewLine;
                //command+="ShoulderRight.X"+ShoulderRight.X;
            }
            float LeftSide = Math.Abs(ShoulderLeft.X)-Math.Abs(AnkleLeft.X);
            float RightSide = Math.Abs(ShoulderRight.X)-Math.Abs(AnkleRight.X);

            //lewa strona 
            if (Math.Abs(LeftSide)<accuracy)
            {
                command+=Environment.NewLine;
                command+="Lewa stopa jest dobrze ustawiona";
                left_side_correct=true;
            }
            else if (Math.Abs(LeftSide)>=accuracy)
            {
                command+=Environment.NewLine;
                command+="Przesuń lewą stopę o ";
                if (LeftSide>0)
                    command+=Math.Round(Math.Abs(LeftSide)*100, 0)+"cm  do środka";
                if (LeftSide<0)
                    command+=Math.Round(Math.Abs(LeftSide)*100, 0)+"cm  na zewnątrz";
                left_side_correct=false;
            }

            //prawa strona
            if (Math.Abs(RightSide)<accuracy)
            {
                command+=Environment.NewLine;
                command+="Prawa stopa jest dobrze ustawiona";
                right_side_correct=true;
            }
            else if (Math.Abs(RightSide)>=accuracy)
            {
                command+=Environment.NewLine;
                command+="Przesuń prawą stopę o";
                if (RightSide<0)
                    command+=Math.Round(Math.Abs(RightSide)*100, 0)+"cm  do środka";
                if (RightSide>0)
                    command+=Math.Round(Math.Abs(RightSide)*100, 0)+"cm  na zewnątrz";
                right_side_correct=false;

            }
            if (left_side_correct==true&&right_side_correct==true)
            {
                PositioningBool["Ustawiaj_Nogi"].Item1["FootUnderShoulders"]=true;
            }
            return command;
        }

        private string AngleBetweenThigs(Dictionary<string, Vector3> Person, double LowBorder, double HighBorder)
        {


            string command = string.Empty;
            Vector3 Person_I_Bone = Vector3.Subtract(Person[JointType.HipLeft.ToString()], Person[JointType.KneeLeft.ToString()]);
            Vector3 Person_II_Bone = Vector3.Subtract(Person[JointType.HipRight.ToString()], Person[JointType.KneeRight.ToString()]);
            //Iloczyn skalarny 
            float DotProduct = Vector3.Dot(Person_I_Bone, Person_II_Bone);
            double Person_I_Bone_lenght = Math.Sqrt(Math.Pow(Person_I_Bone.X, 2)+Math.Pow(Person_I_Bone.Y, 2)+Math.Pow(Person_I_Bone.Z, 2));
            double Person_II_Bone_lenght = Math.Sqrt(Math.Pow(Person_II_Bone.X, 2)+Math.Pow(Person_II_Bone.Y, 2)+Math.Pow(Person_II_Bone.Z, 2));
            double AngleBetweenBones = Math.Acos(DotProduct/( Person_I_Bone_lenght*Person_II_Bone_lenght ));
            double AngleBetweenBonesDegree = Math.Round(MathUtil.RadiansToDegrees((float)AngleBetweenBones), 0);
            if (AngleBetweenBonesDegree>=HighBorder)
            {
                command+=Environment.NewLine;
                command+="Przesuń uda do siebie";
                PositioningBool["Ustawiaj_Nogi"].Item1["AngleBetweenThigs"]=false;
            }
            if (AngleBetweenBonesDegree<=LowBorder)
            {
                command+=Environment.NewLine;
                command+=" Rozszerz uda ";
                PositioningBool["Ustawiaj_Nogi"].Item1["AngleBetweenThigs"]=false;
            }
            if (AngleBetweenBonesDegree>LowBorder&&AngleBetweenBonesDegree<HighBorder)
            {
                command+=Environment.NewLine;
                command+=" Kąt miedzy nogami ustawiony prawidłowo ";
                PositioningBool["Ustawiaj_Nogi"].Item1["AngleBetweenThigs"]=true;
            }
            return command;
        }

        private string KneeAnglesComparison(Dictionary<string, Vector3> Person, float accuracy)
        {
            string command = string.Empty;
            //lewe udo a lewy piszczel
            Vector3 LThigh = Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.HipLeft.ToString()]);
            Vector3 LTibia = Vector3.Subtract(Person[JointType.KneeLeft.ToString()], Person[JointType.AnkleLeft.ToString()]);
            //Iloczyn skalarny 
            float LDotProduct = Vector3.Dot(LThigh, LTibia);
            double LThighNegate_lenght = Math.Sqrt(Math.Pow(LThigh.X, 2)+Math.Pow(LThigh.Y, 2)+Math.Pow(LThigh.Z, 2));
            double LTibia_lenght = Math.Sqrt(Math.Pow(LTibia.X, 2)+Math.Pow(LTibia.Y, 2)+Math.Pow(LTibia.Z, 2));
            double LAngleBetweenBones = Math.Acos(LDotProduct/( LThighNegate_lenght*LTibia_lenght ));
            double LAngleBetweenBonesDegree = Math.Round(MathUtil.RadiansToDegrees((float)LAngleBetweenBones), 0);

            //prawe udo a lewy piszczel
            Vector3 RThigh = Vector3.Subtract(Person[JointType.KneeRight.ToString()], Person[JointType.HipRight.ToString()]);
            Vector3 RTibia = Vector3.Subtract(Person[JointType.KneeRight.ToString()], Person[JointType.AnkleRight.ToString()]);
            //Iloczyn skalarny 
            float RDotProduct = Vector3.Dot(RThigh, RTibia);
            double RThighNegate_lenght = Math.Sqrt(Math.Pow(RThigh.X, 2)+Math.Pow(RThigh.Y, 2)+Math.Pow(RThigh.Z, 2));
            double RTibia_lenght = Math.Sqrt(Math.Pow(RTibia.X, 2)+Math.Pow(RTibia.Y, 2)+Math.Pow(RTibia.Z, 2));
            double RAngleBetweenBones = Math.Acos(RDotProduct/( RThighNegate_lenght*RTibia_lenght ));
            double RAngleBetweenBonesDegree = Math.Round(MathUtil.RadiansToDegrees((float)RAngleBetweenBones), 0);

            bool left_knee_bool = false;
            bool right_knee_bool = false;
            if (TeacherAnglesBetweenBones!=null)
            {
                var LeftKneeTeacherAngle = Math.Round(MathUtil.RadiansToDegrees((float)TeacherAnglesBetweenBones[new Tuple<string, string, string>("left", "LThighNegate", "LTibia")]), 0);
                var RightKneeTeacherAngle = Math.Round(MathUtil.RadiansToDegrees((float)TeacherAnglesBetweenBones[new Tuple<string, string, string>("right", "RThighNegate", "RTibia")]), 0);

                var LeftKneeAnglesDifference = LeftKneeTeacherAngle-LAngleBetweenBonesDegree;
                var RightKneeAnglesDifference = RightKneeTeacherAngle-RAngleBetweenBonesDegree;


                //lewe kolano
                if (Math.Abs(LeftKneeAnglesDifference)<accuracy)
                {
                    command+=Environment.NewLine;
                    command+=" Noga w lewym kolanie dobrze ugięta ";
                    left_knee_bool=true;
                }
                if (Math.Abs(LeftKneeAnglesDifference)>=accuracy)
                {
                    if (LeftKneeAnglesDifference<0)
                    {
                        command+=Environment.NewLine;
                        command+="ugnij bardziej noge w lewym kolanie ";
                    }
                    if (LeftKneeAnglesDifference>0)
                    {
                        command+=Environment.NewLine;
                        command+="rozprostuj bardziej noge w lewym kolanie ";
                    }
                    left_knee_bool=false;
                }


                //prawe kolano
                if (Math.Abs(RightKneeAnglesDifference)<accuracy)
                {
                    command+=Environment.NewLine;
                    command+=" Noga w prawym kolanie dobrze ugięta ";
                    right_knee_bool=true;
                }
                if (Math.Abs(RightKneeAnglesDifference)>=accuracy)
                {
                    if (RightKneeAnglesDifference<0)
                    {
                        command+=Environment.NewLine;
                        command+="ugnij bardziej noge w prawym kolanie ";
                    }
                    if (RightKneeAnglesDifference>0)
                    {
                        command+=Environment.NewLine;
                        command+="rozprostuj bardziej noge w prawym kolanie ";
                    }
                    right_knee_bool=false;
                }
            }
            else
            {
                command+=Environment.NewLine;
                command="Nie wybrano nauczyciela !!! ";
                command+=Environment.NewLine;
                command+=Environment.NewLine;

                command+="kąt pomiedzy lewe udo a lewy piszczel = "+Math.Round(MathUtil.RadiansToDegrees((float)LAngleBetweenBones), 0)+" stopni";

                command+=Environment.NewLine;
                command+="kąt pomiedzy prawe udo a prawe piszczel = "+Math.Round(MathUtil.RadiansToDegrees((float)RAngleBetweenBones), 0)+" stopni";
            }

            if (right_knee_bool==true&&left_knee_bool==true)
                PositioningBool["Kat_w_kolanach"].Item1["KneeAnglesComparison"]=true;
            else
                PositioningBool["Kat_w_kolanach"].Item1["KneeAnglesComparison"]=false;

            return command;
        }

        private string RightFoodPositioning(Dictionary<string, Vector3> Person, float accuracy)
        {
            string command = string.Empty;
            command=" Przesuń prawą stopę ";
            double FootRight_Lenght = Math.Round(Vector3.Distance(Person[JointType.FootRight.ToString()], Person[JointType.AnkleRight.ToString()])*100, 4);
            Vector3 FootRight = Person[JointType.FootRight.ToString()];
            Vector3 AnkelLeft = Person[JointType.AnkleLeft.ToString()];
            var Destiny = AnkelLeft.Z*100+FootRight_Lenght*2;
            var FootRight_position_to_destiny = Destiny-FootRight.Z*100;
            if (Math.Abs(FootRight_position_to_destiny)<accuracy)
            {
                command+=Environment.NewLine;
                command+="Prawa stopa ustawiona";
                PositioningBool["Prawa_stopa"].Item1["RightFoodPositioning"]=true;

            }
            else if (Math.Abs(FootRight_position_to_destiny)>=accuracy)
            {
                if (FootRight_position_to_destiny>0)
                {
                    command+=Environment.NewLine;
                    command+="przesuń czubek prawej stopy do tyłu ";
                }
                else if (FootRight_position_to_destiny<0)
                {
                    command+=Environment.NewLine;
                    command+="przesuń czubek prawej stopy do przodu ";
                }
                PositioningBool["Prawa_stopa"].Item1["RightFoodPositioning"]=false;
            }
            return command;
        }

        private string CenterOfGravityCommands(Dictionary<string, Vector3> Teacher, Vector3 TeacherCenterOfGravity, Dictionary<string, Vector3> Student, Vector3 StudentCenterOfGravity, float accuracy)
        {
            string command = string.Empty;
            Vector3[] Contener = new Vector3[BonelLines.Count-10];
            int i = 1;
            float X_coordinate_of_center = 0;
            float Y_coordinate_of_center = 0;
            float Z_coordinate_of_center = 0;
            double SumAngles = 0;
            //foreach (var bonelLines in BonelLines)
            //{
            //    if (bonelLines.Key.Item1==JointType.ShoulderRight&&bonelLines.Key.Item2==JointType.ElbowRight
            //             ||bonelLines.Key.Item1==JointType.ElbowRight&&bonelLines.Key.Item2==JointType.WristRight
            //             ||bonelLines.Key.Item1==JointType.WristRight&&bonelLines.Key.Item2==JointType.HandRight
            //             ||bonelLines.Key.Item1==JointType.HandRight&&bonelLines.Key.Item2==JointType.HandTipRight
            //             ||bonelLines.Key.Item1==JointType.WristRight&&bonelLines.Key.Item2==JointType.ThumbRight
            //             ||bonelLines.Key.Item1==JointType.ShoulderLeft&&bonelLines.Key.Item2==JointType.ElbowLeft
            //             ||bonelLines.Key.Item1==JointType.ElbowLeft&&bonelLines.Key.Item2==JointType.WristLeft
            //             ||bonelLines.Key.Item1==JointType.WristLeft&&bonelLines.Key.Item2==JointType.HandLeft
            //             ||bonelLines.Key.Item1==JointType.HandLeft&&bonelLines.Key.Item2==JointType.HandTipLeft
            //             ||bonelLines.Key.Item1==JointType.WristLeft&&bonelLines.Key.Item2==JointType.ThumbLeft
            //             ||bonelLines.Key.Item1==JointType.HipRight&&bonelLines.Key.Item2==JointType.KneeRight
            //             ||bonelLines.Key.Item1==JointType.KneeRight&&bonelLines.Key.Item2==JointType.AnkleRight
            //             ||bonelLines.Key.Item1==JointType.AnkleRight&&bonelLines.Key.Item2==JointType.FootRight
            //             ||bonelLines.Key.Item1==JointType.WristLeft&&bonelLines.Key.Item2==JointType.KneeLeft
            //             ||bonelLines.Key.Item1==JointType.KneeLeft&&bonelLines.Key.Item2==JointType.AnkleLeft
            //             ||bonelLines.Key.Item1==JointType.AnkleLeft&&bonelLines.Key.Item2==JointType.FootLeft)
            //    {
            //        continue;
            //    }
            //    var subtracted = Vector3.Subtract(Student[bonelLines.Key.Item1.ToString()], Student[bonelLines.Key.Item2.ToString()]);
            //    //Iloczyn skalarny 
            //    var DotProduct = Vector3.Dot(subtracted, Vector3.UnitZ);
            //    var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            //    var Student_II_Bone_lenght = Math.Sqrt(Math.Pow(Vector3.UnitZ.X, 2)+Math.Pow(Vector3.UnitZ.Y, 2)+Math.Pow(Vector3.UnitZ.Z, 2));
            //    var AngleBetweenBones = 90-Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);
            //    SumAngles+=AngleBetweenBones;
            //    //var divided = Vector3.Divide(subtracted, 2);
            //    //var added = Vector3.Add(Person[bonelLines.Key.Item2.ToString()], divided);

            //    //Contener[i]=added;
            //    i++;
            //}
            //var AvarageAngle = SumAngles/i;
            var subtracted = Vector3.Subtract(Student[JointType.SpineMid.ToString()], Student[JointType.SpineBase.ToString()]);
            //Iloczyn skalarny 
            var DotProduct = Vector3.Dot(subtracted, Vector3.UnitZ);
            var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            var Student_II_Bone_lenght = Math.Sqrt(Math.Pow(Vector3.UnitZ.X, 2)+Math.Pow(Vector3.UnitZ.Y, 2)+Math.Pow(Vector3.UnitZ.Z, 2));
            var AngleBetweenBones = 90-Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            if (once_gone_thru_for_COG==false&&AngleBetweenBones>10)
            {
                AngleBetweenBones_to_once_gone_thru=AngleBetweenBones;
                once_gone_thru_for_COG=true;
            }

            var TeacherAnkleLeft = Teacher[JointType.AnkleLeft.ToString()];
            var TeacherAnkleRight = Teacher[JointType.AnkleRight.ToString()];

            var StudentAnkleLeft = Student[JointType.AnkleLeft.ToString()];
            var StudentAnkleRight = Student[JointType.AnkleRight.ToString()];

            command+=Environment.NewLine;
            command+="TeacherCenterOfGravity ->"+TeacherCenterOfGravity.ToString();


            Vector3 nowy_wektor = Vector3.Zero;
            if (once_gone_thru_for_COG==true)
            {
                Quaternion kwaternion;
                kwaternion=Quaternion.RotationAxis(Vector3.UnitX, -MathUtil.DegreesToRadians((float)AngleBetweenBones_to_once_gone_thru));
                StudentCenterOfGravity=Vector3.Transform(StudentCenterOfGravity, kwaternion);
                StudentAnkleLeft=Vector3.Transform(StudentAnkleLeft, kwaternion);
                StudentAnkleRight=Vector3.Transform(StudentAnkleRight, kwaternion);
                nowy_wektor=Vector3.Transform(Vector3.UnitY, kwaternion);
            }

            var DistanceBetweenAnklesTeacher = Math.Abs(TeacherAnkleLeft.Z-TeacherAnkleRight.Z);
            var DistanceBetweenAnklesStudent = Math.Abs(StudentAnkleLeft.Z-StudentAnkleRight.Z);

            var TeacherRightSideTension = ( 100/DistanceBetweenAnklesTeacher )*( TeacherCenterOfGravity.Z-TeacherAnkleLeft.Z );
            var TeacherLeftSideTension = ( 100/DistanceBetweenAnklesTeacher )*( -( TeacherCenterOfGravity.Z-TeacherAnkleRight.Z ) );

            var StudentRightSideTension = ( 100/DistanceBetweenAnklesStudent )*( StudentCenterOfGravity.Z-StudentAnkleLeft.Z );
            var StudentLeftSideTension = ( 100/DistanceBetweenAnklesStudent )*( -( StudentCenterOfGravity.Z-StudentAnkleRight.Z ) );

            var RightSideDifference = TeacherRightSideTension-StudentRightSideTension;
            var LeftSideDifference = TeacherLeftSideTension-StudentLeftSideTension;

            command+=Environment.NewLine;
            command+="TeacherCenterOfGravity ->"+TeacherCenterOfGravity.ToString();
            command+=Environment.NewLine;
            command+="AvarageAngle1 ->"+AngleBetweenBones+"   Quaternion   "+nowy_wektor.ToString();
            command+=Environment.NewLine;
            command+="TeacherLeftSideTension    "+TeacherLeftSideTension+"%";
            command+=Environment.NewLine;
            command+="TeacherRightSideTension    "+TeacherRightSideTension+"%";
            command+=Environment.NewLine;
            command+="StudentLeftSideTension    "+StudentLeftSideTension+"%";
            command+=Environment.NewLine;
            command+="StudentRightSideTension    "+StudentRightSideTension+"%";
            //command+=Environment.NewLine;
            //command+="LLeftSideDifference  "+LeftSideDifference;
            //command+=Environment.NewLine;
            //command+="RightSideDifference    "+RightSideDifference;

            if (Math.Abs(RightSideDifference)<accuracy)
            {
                command+=Environment.NewLine;
                command+="ŚRODEK CIĘŻKOŚCI DOBRZE USTAWIONY ";
                PositioningBool["Srodek_Ciezkosci"].Item1["CenterOfGravityCommands"]=true;
            }
            else if (Math.Abs(LeftSideDifference)>accuracy)
            {
                if (LeftSideDifference>0)
                {
                    command+=Environment.NewLine;
                    command+=" Pochyl się do  przodu  ";
                }
                else if (LeftSideDifference<0)
                {
                    command+=Environment.NewLine;
                    command+="pochyl się do tyłu  ";
                }
                PositioningBool["Srodek_Ciezkosci"].Item1["CenterOfGravityCommands"]=false;
            }
            return command;
        }

        private string HandsPositioning(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            string command = string.Empty;
            bool left_elbow = false;
            bool left_armpit = false;
            bool right_elbow = false;
            bool right_armpit = false;
            //-----------------------------------------lewa strona------------------------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a lewy bark
            Vector3 Teacher_cervical_spine = Vector3.Subtract(Teacher[JointType.SpineShoulder.ToString()], Teacher[JointType.SpineMid.ToString()]);
            Vector3 Teacher_Lshoulder = Vector3.Subtract(Teacher[JointType.SpineShoulder.ToString()], Teacher[JointType.ShoulderLeft.ToString()]);
            //Iloczyn skalarny 
            float DotProduct = Vector3.Dot(Teacher_cervical_spine, Teacher_Lshoulder);
            double Teacher_I_Bone_lenght = Math.Sqrt(Math.Pow(Teacher_cervical_spine.X, 2)+Math.Pow(Teacher_cervical_spine.Y, 2)+Math.Pow(Teacher_cervical_spine.Z, 2));
            double Teacher_II_Bone_lenght = Math.Sqrt(Math.Pow(Teacher_Lshoulder.X, 2)+Math.Pow(Teacher_Lshoulder.Y, 2)+Math.Pow(Teacher_Lshoulder.Z, 2));
            double AngleBetweenBones = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 0);
            var Teacher_left_cervical_spine_Lshoulder = AngleBetweenBones;


            //bark-ramię
            Vector3 Teacher_LshoulderNegate = Vector3.Negate(Teacher_Lshoulder);
            Vector3 Teacher_Larm = Vector3.Subtract(Teacher[JointType.ShoulderLeft.ToString()], Teacher[JointType.ElbowLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Teacher_LshoulderNegate, Teacher_Larm);
            Teacher_I_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_LshoulderNegate.X, 2)+Math.Pow(Teacher_LshoulderNegate.Y, 2)+Math.Pow(Teacher_LshoulderNegate.Z, 2));
            Teacher_II_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Larm.X, 2)+Math.Pow(Teacher_Larm.Y, 2)+Math.Pow(Teacher_Larm.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 0);
            var Teacher_left_LshoulderNegate_Larm = AngleBetweenBones;


            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Teacher_Lhumerus = Vector3.Subtract(Teacher[JointType.ElbowLeft.ToString()], Teacher[JointType.ShoulderLeft.ToString()]);
            Vector3 Teacher_Lulna = Vector3.Subtract(Teacher[JointType.ElbowLeft.ToString()], Teacher[JointType.WristLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Teacher_Lhumerus, Teacher_Lulna);
            Teacher_I_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Lhumerus.X, 2)+Math.Pow(Teacher_Lhumerus.Y, 2)+Math.Pow(Teacher_Lhumerus.Z, 2));
            Teacher_II_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Lulna.X, 2)+Math.Pow(Teacher_Lulna.Y, 2)+Math.Pow(Teacher_Lulna.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 0);
            var Teacher_left_Lhumerus_Lulna = AngleBetweenBones;

            //---------------------------------------------------prawa-strona ----------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a prawy bark
            Vector3 Teacher_Rshoulder = Vector3.Subtract(Teacher[JointType.SpineShoulder.ToString()], Teacher[JointType.ShoulderRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Teacher_cervical_spine, Teacher_Rshoulder);
            Teacher_I_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_cervical_spine.X, 2)+Math.Pow(Teacher_cervical_spine.Y, 2)+Math.Pow(Teacher_cervical_spine.Z, 2));
            Teacher_II_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Rshoulder.X, 2)+Math.Pow(Teacher_Rshoulder.Y, 2)+Math.Pow(Teacher_Rshoulder.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 0);
            var Teacher_right_cervical_spine_Rshoulder = AngleBetweenBones;

            //bark-ramię
            Vector3 Teacher_RshoulderNegate = Vector3.Subtract(Teacher[JointType.ShoulderRight.ToString()], Teacher[JointType.SpineShoulder.ToString()]);
            Vector3 Teacher_Rarm = Vector3.Subtract(Teacher[JointType.ShoulderRight.ToString()], Teacher[JointType.ElbowRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Teacher_RshoulderNegate, Teacher_Rarm);
            Teacher_I_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_RshoulderNegate.X, 2)+Math.Pow(Teacher_RshoulderNegate.Y, 2)+Math.Pow(Teacher_RshoulderNegate.Z, 2));
            Teacher_II_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Rarm.X, 2)+Math.Pow(Teacher_Rarm.Y, 2)+Math.Pow(Teacher_Rarm.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 0);
            var Teacher_right_RshoulderNegate_Rarm = AngleBetweenBones;

            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Teacher_Rhumerus = Vector3.Subtract(Teacher[JointType.ElbowRight.ToString()], Teacher[JointType.ShoulderRight.ToString()]);
            Vector3 Teacher_Rulna = Vector3.Subtract(Teacher[JointType.ElbowRight.ToString()], Teacher[JointType.WristRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Teacher_Rhumerus, Teacher_Rulna);
            Teacher_I_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Rhumerus.X, 2)+Math.Pow(Teacher_Rhumerus.Y, 2)+Math.Pow(Teacher_Rhumerus.Z, 2));
            Teacher_II_Bone_lenght=Math.Sqrt(Math.Pow(Teacher_Rulna.X, 2)+Math.Pow(Teacher_Rulna.Y, 2)+Math.Pow(Teacher_Rulna.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 0);
            var Teacher_right_Rhumerus_Rulna = AngleBetweenBones;





            //-----------------------------------------lewa strona------------------------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a lewy bark
            Vector3 Student_cervical_spine = Vector3.Subtract(Student[JointType.SpineShoulder.ToString()], Student[JointType.SpineMid.ToString()]);
            Vector3 Student_Lshoulder = Vector3.Subtract(Student[JointType.SpineShoulder.ToString()], Student[JointType.ShoulderLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Student_cervical_spine, Student_Lshoulder);
            var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(Student_cervical_spine.X, 2)+Math.Pow(Student_cervical_spine.Y, 2)+Math.Pow(Student_cervical_spine.Z, 2));
            var Student_II_Bone_lenght = Math.Sqrt(Math.Pow(Student_Lshoulder.X, 2)+Math.Pow(Student_Lshoulder.Y, 2)+Math.Pow(Student_Lshoulder.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 0);
            var Student_left_cervical_spine_Lshoulder = AngleBetweenBones;


            //bark-ramię
            Vector3 Student_LshoulderNegate = Vector3.Negate(Student_Lshoulder);
            Vector3 Student_Larm = Vector3.Subtract(Student[JointType.ShoulderLeft.ToString()], Student[JointType.ElbowLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Student_LshoulderNegate, Student_Larm);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(Student_LshoulderNegate.X, 2)+Math.Pow(Student_LshoulderNegate.Y, 2)+Math.Pow(Student_LshoulderNegate.Z, 2));
            Student_II_Bone_lenght=Math.Sqrt(Math.Pow(Student_Larm.X, 2)+Math.Pow(Student_Larm.Y, 2)+Math.Pow(Student_Larm.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 0);
            var Student_left_LshoulderNegate_Larm = AngleBetweenBones;


            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Student_Lhumerus = Vector3.Subtract(Student[JointType.ElbowLeft.ToString()], Student[JointType.ShoulderLeft.ToString()]);
            Vector3 Student_Lulna = Vector3.Subtract(Student[JointType.ElbowLeft.ToString()], Student[JointType.WristLeft.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Student_Lhumerus, Student_Lulna);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(Student_Lhumerus.X, 2)+Math.Pow(Student_Lhumerus.Y, 2)+Math.Pow(Student_Lhumerus.Z, 2));
            Student_II_Bone_lenght=Math.Sqrt(Math.Pow(Student_Lulna.X, 2)+Math.Pow(Student_Lulna.Y, 2)+Math.Pow(Student_Lulna.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 0);
            var Student_left_Lhumerus_Lulna = AngleBetweenBones;

            //---------------------------------------------------prawa-strona ----------------------------------------------------------------------------------------------------------
            //górna cześć kręgosłupa( cervical spine ) a prawy bark
            Vector3 Student_Rshoulder = Vector3.Subtract(Student[JointType.SpineShoulder.ToString()], Student[JointType.ShoulderRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Student_cervical_spine, Student_Rshoulder);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(Student_cervical_spine.X, 2)+Math.Pow(Student_cervical_spine.Y, 2)+Math.Pow(Student_cervical_spine.Z, 2));
            Student_II_Bone_lenght=Math.Sqrt(Math.Pow(Student_Rshoulder.X, 2)+Math.Pow(Student_Rshoulder.Y, 2)+Math.Pow(Student_Rshoulder.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 0);
            var Student_right_cervical_spine_Rshoulder = AngleBetweenBones;

            //bark-ramię
            Vector3 Student_RshoulderNegate = Vector3.Subtract(Student[JointType.ShoulderRight.ToString()], Student[JointType.SpineShoulder.ToString()]);
            Vector3 Student_Rarm = Vector3.Subtract(Student[JointType.ShoulderRight.ToString()], Student[JointType.ElbowRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Student_RshoulderNegate, Student_Rarm);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(Student_RshoulderNegate.X, 2)+Math.Pow(Student_RshoulderNegate.Y, 2)+Math.Pow(Student_RshoulderNegate.Z, 2));
            Student_II_Bone_lenght=Math.Sqrt(Math.Pow(Student_Rarm.X, 2)+Math.Pow(Student_Rarm.Y, 2)+Math.Pow(Student_Rarm.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 0);
            var Student_right_RshoulderNegate_Rarm = AngleBetweenBones;

            //kosc ramienna - kosc łokciowa
            //humerus kość ramienna 
            //ulna kość łokciwa
            Vector3 Student_Rhumerus = Vector3.Subtract(Student[JointType.ElbowRight.ToString()], Student[JointType.ShoulderRight.ToString()]);
            Vector3 Student_Rulna = Vector3.Subtract(Student[JointType.ElbowRight.ToString()], Student[JointType.WristRight.ToString()]);
            //Iloczyn skalarny 
            DotProduct=Vector3.Dot(Student_Rhumerus, Student_Rulna);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(Student_Rhumerus.X, 2)+Math.Pow(Student_Rhumerus.Y, 2)+Math.Pow(Student_Rhumerus.Z, 2));
            Student_II_Bone_lenght=Math.Sqrt(Math.Pow(Student_Rulna.X, 2)+Math.Pow(Student_Rulna.Y, 2)+Math.Pow(Student_Rulna.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 0);
            var Student_right_Rhumerus_Rulna = AngleBetweenBones;

            var Difference_Teacher_left_cervical_spine_Lshoulder_Student_left_cervical_spine_Lshoulder = Teacher_left_cervical_spine_Lshoulder-Student_left_cervical_spine_Lshoulder;
            var Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm = Teacher_left_LshoulderNegate_Larm-Student_left_LshoulderNegate_Larm;
            var Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna = Teacher_left_Lhumerus_Lulna-Student_left_Lhumerus_Lulna;
            var Difference_Teacher_right_cervical_spine_Rshoulder_Student_right_cervical_spine_Rshoulder = Teacher_right_cervical_spine_Rshoulder-Student_right_cervical_spine_Rshoulder;
            var Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm = Teacher_right_RshoulderNegate_Rarm-Student_right_RshoulderNegate_Rarm;
            var Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna = Teacher_right_Rhumerus_Rulna-Student_right_Rhumerus_Rulna;


            //1    
            //2
            if (Math.Abs(Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm)<accuracy)
            {
                command+=Environment.NewLine;
                command+=" Kat w lewej pacha dobrze ustawiony "+Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm;
                left_armpit=true;
            }
            else if (Math.Abs(Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm)>accuracy)
            {
                if (Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm>0)
                {
                    command+=Environment.NewLine;
                    command+=" rozprostuj ręke w lewym pacha "+Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm;
                    left_armpit=false;

                }
                else if (Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm<0)
                {
                    command+=Environment.NewLine;
                    command+=" zegnij ręke w lewym pacha  "+Difference_Teacher_left_LshoulderNegate_Larm_Student_left_LshoulderNegate_Larm;
                    left_armpit=false;
                }
            }
            //3
            if (Math.Abs(Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna)<accuracy)
            {
                command+=Environment.NewLine;
                command+=" Kat w lewym łokciu dobrze ustawiony  "+Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna;
                left_elbow=true;
            }
            else if (Math.Abs(Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna)>accuracy)
            {
                if (Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna>0)
                {
                    command+=Environment.NewLine;
                    command+=" rozprostuj ręke w lewym łokciu  "+Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna;
                    left_elbow=false;
                }
                else if (Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna<0)
                {
                    command+=Environment.NewLine;
                    command+="  zegnij ręke w lewym łokciu   "+Difference_Teacher_left_Lhumerus_Lulna_Student_left_Lhumerus_Lulna;
                    left_elbow=false;
                }
            }
            //4
            //5
            if (Math.Abs(Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm)<accuracy)
            {
                command+=Environment.NewLine;
                command+=" Kat w prawej pacha dobrze ustawiony "+Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm;
                right_armpit=true;
            }
            else if (Math.Abs(Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm)>accuracy)
            {
                if (Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm>0)
                {
                    command+=Environment.NewLine;
                    command+=" rozprostuj  ręke w prawej pacha "+Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm;
                    right_armpit=true;
                }
                else if (Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm<0)
                {
                    command+=Environment.NewLine;
                    command+=" zegnij ręke w prawej pacha  "+Difference_Teacher_right_RshoulderNegate_Rarm_Student_right_RshoulderNegate_Rarm;
                    right_armpit=true;
                }
            }

            //6
            if (Math.Abs(Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna)<accuracy)
            {
                command+=Environment.NewLine;
                command+=" Kat w prawym łokciu dobrze ustawiony  "+Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna;
                right_elbow=true;
            }
            else if (Math.Abs(Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna)>accuracy)
            {
                if (Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna>0)
                {
                    command+=Environment.NewLine;
                    command+=" rozprostuj ręke w prawym łokciu  "+Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna;
                    right_elbow=false;

                }
                else if (Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna<0)
                {
                    command+=Environment.NewLine;
                    command+=" zegnij  ręke w prawym łokciu   "+Difference_Teacher_right_Rhumerus_Rulna_Student_right_Rhumerus_Rulna;
                    right_elbow=false;
                }
            }
            if (left_armpit==true&&left_elbow==true&&right_armpit==true&&right_elbow==true)
            {
                PositioningBool["Ustawianie_Rak"].Item1["HandsPositioning"]=true;
            }
            return command;
        }

        private string RightKneeDeflection(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            string command = string.Empty;
            //Teacher
            //leftknee
            //string command = string.Empty;
            //Vector3 Teacher_I_Bone = Vector3.Subtract(Teacher[JointType.KneeLeft.ToString()], Teacher[JointType.HipLeft.ToString()]);
            //Vector3 Teacher_II_Bone = Vector3.Subtract(Teacher[JointType.KneeLeft.ToString()], Teacher[JointType.AnkleLeft.ToString()]);
            //float DotProduct = Vector3.Dot(Teacher_I_Bone, Teacher_II_Bone);
            //double Teacher_I_Bone_lenght = Math.Sqrt(Math.Pow(Teacher_I_Bone.X, 2)+Math.Pow(Teacher_I_Bone.Y, 2)+Math.Pow(Teacher_I_Bone.Z, 2));
            //double Teacher_II_Bone_lenght = Math.Sqrt(Math.Pow(Teacher_II_Bone.X, 2)+Math.Pow(Teacher_II_Bone.Y, 2)+Math.Pow(Teacher_II_Bone.Z, 2));
            //double AngleBetweenBones = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 1);
            //var teacher_knee_left_deflection = AngleBetweenBones;

            //Rightknee
            var Teacher_I_Bone = Vector3.Subtract(Teacher[JointType.KneeRight.ToString()], Teacher[JointType.HipRight.ToString()]);
            var Teacher_II_Bone = Vector3.Subtract(Teacher[JointType.KneeRight.ToString()], Teacher[JointType.AnkleRight.ToString()]);
            var DotProduct = Vector3.Dot(Teacher_I_Bone, Teacher_II_Bone);
            var Teacher_I_Bone_lenght = Math.Sqrt(Math.Pow(Teacher_I_Bone.X, 2)+Math.Pow(Teacher_I_Bone.Y, 2)+Math.Pow(Teacher_I_Bone.Z, 2));
            var Teacher_II_Bone_lenght = Math.Sqrt(Math.Pow(Teacher_II_Bone.X, 2)+Math.Pow(Teacher_II_Bone.Y, 2)+Math.Pow(Teacher_II_Bone.Z, 2));
            var AngleBetweenBones = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Teacher_I_Bone_lenght*Teacher_II_Bone_lenght ))), 1);
            var teacher_knee_right_deflection = AngleBetweenBones;

            //Student
            //leftknee
            //Vector3 Student_I_Bone = Vector3.Subtract(Student[JointType.KneeLeft.ToString()], Student[JointType.HipLeft.ToString()]);
            //Vector3 Student_II_Bone = Vector3.Subtract(Student[JointType.KneeLeft.ToString()], Student[JointType.AnkleLeft.ToString()]);
            // DotProduct = Vector3.Dot(Student_I_Bone, Student_II_Bone);
            //double Student_I_Bone_lenght = Math.Sqrt(Math.Pow(Student_I_Bone.X, 2)+Math.Pow(Student_I_Bone.Y, 2)+Math.Pow(Student_I_Bone.Z, 2));
            //double Student_II_Bone_lenght = Math.Sqrt(Math.Pow(Student_II_Bone.X, 2)+Math.Pow(Student_II_Bone.Y, 2)+Math.Pow(Student_II_Bone.Z, 2));
            //AngleBetweenBones = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 1);
            //var student_knee_left_deflection = AngleBetweenBones;

            //Rightknee
            var Student_I_Bone = Vector3.Subtract(Student[JointType.KneeRight.ToString()], Student[JointType.HipRight.ToString()]);
            var Student_II_Bone = Vector3.Subtract(Student[JointType.KneeRight.ToString()], Student[JointType.AnkleRight.ToString()]);
            DotProduct=Vector3.Dot(Student_I_Bone, Student_II_Bone);
            var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(Student_I_Bone.X, 2)+Math.Pow(Student_I_Bone.Y, 2)+Math.Pow(Student_I_Bone.Z, 2));
            var Student_II_Bone_lenght = Math.Sqrt(Math.Pow(Student_II_Bone.X, 2)+Math.Pow(Student_II_Bone.Y, 2)+Math.Pow(Student_II_Bone.Z, 2));
            AngleBetweenBones=Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 1);
            var student_knee_right_deflection = AngleBetweenBones;

            var Difference = teacher_knee_right_deflection-student_knee_right_deflection;

            if (Math.Abs(Difference)<accuracy)
            {
                command+=Environment.NewLine;
                command+="ugięcie w PRAWYM  kolanie poprawne"+student_knee_right_deflection;
                PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]=true;
            }
            else if (Math.Abs(Difference)>=accuracy)
            {
                if (Difference>0)
                {
                    command+=Environment.NewLine;
                    command+="Rozprostuj noge w prawym kolanie"+student_knee_right_deflection;

                }
                else if (Difference<0)
                {
                    command+=Environment.NewLine;
                    command+="Ugnij noge w prowym kolanie "+student_knee_right_deflection;
                }
                PositioningBool["Wyprowadzenie_ciosu"].Item1["RightKneeDeflection"]=false;
            }
            return command;
        }
        private string HipsTwist(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            string command = string.Empty;
            var subtracted = Vector3.Subtract(Student[JointType.SpineBase.ToString()], Student[JointType.HipRight.ToString()]);
            var DotProduct = Vector3.Dot(subtracted, Vector3.Negate(Vector3.UnitX));
            var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            var Student_II_Bone_lenght = 1;
            var StudentRightHipAngle = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            subtracted=Vector3.Subtract(Student[JointType.SpineBase.ToString()], Student[JointType.HipLeft.ToString()]);
            DotProduct=Vector3.Dot(subtracted, Vector3.UnitX);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            Student_II_Bone_lenght=1;
            var StudentLeftHipAngle = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            subtracted=Vector3.Subtract(Teacher[JointType.SpineBase.ToString()], Teacher[JointType.HipRight.ToString()]);
            DotProduct=Vector3.Dot(subtracted, Vector3.Negate(Vector3.UnitX));
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            Student_II_Bone_lenght=1;
            var TeacherRightHipAngle = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            subtracted=Vector3.Subtract(Teacher[JointType.SpineBase.ToString()], Teacher[JointType.HipLeft.ToString()]);
            DotProduct=Vector3.Dot(subtracted, Vector3.UnitX);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            Student_II_Bone_lenght=1;
            var TeacherLeftHipAngle = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            var LeftHipAngleDifference = TeacherLeftHipAngle-StudentLeftHipAngle;
            var RightHipAngleDifference = TeacherRightHipAngle-StudentRightHipAngle;

            //command+=Environment.NewLine;
            //command+="StudentRightHipAngle "+StudentRightHipAngle;
            //command+=Environment.NewLine;
            //command+="StudentLeftHipAngle "+StudentLeftHipAngle;
            //command+=Environment.NewLine;
            //command+="TeacherRightHipAngle "+TeacherRightHipAngle;
            //command+=Environment.NewLine;
            //command+="TeacherLeftHipAngle "+TeacherLeftHipAngle;

            //command+=Environment.NewLine;
            //command+="LeftHipAngleDifference "+LeftHipAngleDifference;
            //command+=Environment.NewLine;
            //command+="StudentLeftHipAngle "+StudentLeftHipAngle;
            //command+=Environment.NewLine;
            //command+="TeacherLeftHipAngle "+TeacherLeftHipAngle;

            //if(StudentAngleZ>(100+accuracy) && TeacherAngleZ<( 100+accuracy ))
            //{
            //    command="Cofnij PRAWE biodro do tyłu.  Zacznij od wyprawadzania do przodu lewego biodra";
            //    PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=false;
            //    return command;
            //}
            if (Math.Abs(LeftHipAngleDifference)<accuracy)
            {
                command+=Environment.NewLine;
                command+="LEWE Biodro na dobrej pozycji";
                PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=true;
            }
            else if (Math.Abs(LeftHipAngleDifference)>=accuracy)
            {
                if (LeftHipAngleDifference>0)
                {
                    command+=Environment.NewLine;
                    command+="LEWE Biodro do przodu";
                }
                else if (LeftHipAngleDifference<0)
                {
                    command+=Environment.NewLine;
                    command+="Cofnij LEWE biodro";
                }
                PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=false;
            }
            //var subtracted = Vector3.Subtract(Teacher[JointType.HipRight.ToString()], Teacher[JointType.HipLeft.ToString()]);
            //var DotProduct = Vector3.Dot(subtracted, Vector3.UnitX*10);
            //var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            //var Student_II_Bone_lenght = 10;
            //var TeacherAngleX = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            //subtracted=Vector3.Subtract(Teacher[JointType.HipRight.ToString()], Teacher[JointType.HipLeft.ToString()]);
            //DotProduct=Vector3.Dot(subtracted, Vector3.UnitZ*10);
            //Student_I_Bone_lenght=Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            //Student_II_Bone_lenght=10;
            //var TeacherAngleZ = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            //subtracted = Vector3.Subtract(Student[JointType.HipRight.ToString()], Student[JointType.HipLeft.ToString()]);
            //DotProduct= Vector3.Dot(subtracted, Vector3.UnitX*10);
            //Student_I_Bone_lenght = Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            //Student_II_Bone_lenght =10;
            //var StudentAngleX= Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            //subtracted=Vector3.Subtract(Student[JointType.HipRight.ToString()], Student[JointType.HipLeft.ToString()]);
            //DotProduct=Vector3.Dot(subtracted, Vector3.UnitZ*10);
            //Student_I_Bone_lenght=Math.Sqrt(Math.Pow(subtracted.X, 2)+Math.Pow(subtracted.Y, 2)+Math.Pow(subtracted.Z, 2));
            //Student_II_Bone_lenght=10;
            //var StudentAngleZ = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            //var Xdifference = TeacherAngleX-StudentAngleX;
            //var Zdifference = TeacherAngleZ-StudentAngleZ;

            //if(StudentAngleZ>(100+accuracy) && TeacherAngleZ<( 100+accuracy ))
            //{
            //    command="Cofnij PRAWE biodro do tyłu.  Zacznij od wyprawadzania do przodu lewego biodra";
            //    PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=false;
            //    return command;
            //}
            //if(Math.Abs(Xdifference)<accuracy)
            //{
            //    command+=Environment.NewLine;
            //    command+="Biodro na dobrej pozycji";
            //    PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=true;
            //}
            //else if(Math.Abs(Xdifference)>=accuracy)
            //{
            //    if(Xdifference>0)
            //    {
            //        command+=Environment.NewLine;
            //        command+="Biodro do przodu";
            //    }
            //    else if(Xdifference<0)
            //    {
            //        command+=Environment.NewLine;
            //        command+="Cofnij biodro";
            //    }
            //PositioningBool["Wyprowadzenie_ciosu"].Item1["HipsTwist"]=false;
            //}

            return command;
        }

        private string StraightLeftPunchAngles(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            string command = string.Empty;
            var I_bone = Vector3.Subtract(Teacher[JointType.ElbowLeft.ToString()], Teacher[JointType.WristLeft.ToString()]);
            var II_bone = Vector3.Subtract(Teacher[JointType.ElbowLeft.ToString()], Teacher[JointType.ShoulderLeft.ToString()]);
            var DotProduct = Vector3.Dot(I_bone, II_bone);
            var Student_I_Bone_lenght = Math.Sqrt(Math.Pow(I_bone.X, 2)+Math.Pow(I_bone.Y, 2)+Math.Pow(I_bone.Z, 2));
            var Student_II_Bone_lenght = Math.Sqrt(Math.Pow(II_bone.X, 2)+Math.Pow(II_bone.Y, 2)+Math.Pow(II_bone.Z, 2));
            var TeacherLeftElbowAngle = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            I_bone=Vector3.Subtract(Student[JointType.ElbowLeft.ToString()], Student[JointType.WristLeft.ToString()]);
            II_bone=Vector3.Subtract(Student[JointType.ElbowLeft.ToString()], Student[JointType.ShoulderLeft.ToString()]);
            DotProduct=Vector3.Dot(I_bone, II_bone);
            Student_I_Bone_lenght=Math.Sqrt(Math.Pow(I_bone.X, 2)+Math.Pow(I_bone.Y, 2)+Math.Pow(I_bone.Z, 2));
            Student_II_Bone_lenght=Math.Sqrt(Math.Pow(II_bone.X, 2)+Math.Pow(II_bone.Y, 2)+Math.Pow(II_bone.Z, 2));
            var StudentLeftElbowAngle = Math.Round(MathUtil.RadiansToDegrees((float)Math.Acos(DotProduct/( Student_I_Bone_lenght*Student_II_Bone_lenght ))), 3);

            var AngleDifference = TeacherLeftElbowAngle-StudentLeftElbowAngle;

            if (Math.Abs(AngleDifference)<accuracy)
            {
                command+=Environment.NewLine;
                command+="Kąt w łokciu poprawny";
                PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchAngles"]=true;
            }
            else if (Math.Abs(AngleDifference)>=accuracy)
            {
                if (AngleDifference>0)
                {
                    command+=Environment.NewLine;
                    command+="Rozprostuj reke w lewym łokciu ";
                }
                else if (AngleDifference<0)
                {
                    command+=Environment.NewLine;
                    command+="Ugnij ręke w lewym łokciu";
                }
                PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchAngles"]=false;
            }
            return command;
        }

        private string StraightLeftPunchHeight(Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy)
        {
            string command = string.Empty;
            bool X_AX = false;
            bool Y_AX = false;
            command="Pamiętaj aby trzymac pięść na wysokości twarzy";

            // trzeba wyliczyc gdzie jest mniej wiecej twarz
            // na początku przyjmijmy ze jest to punkt pomiedzy głową a szyją
            var FacePosition = new Vector3(( Student[JointType.Head.ToString()].X+Student[JointType.Neck.ToString()].X )/2, ( Student[JointType.Head.ToString()].Y+Student[JointType.Neck.ToString()].Y )/2, ( Student[JointType.Head.ToString()].Z+Student[JointType.Neck.ToString()].Z )/2);
            var X_Difference = FacePosition.X-Student[JointType.HandLeft.ToString()].X;
            var Y_Difference = FacePosition.Y-Student[JointType.HandLeft.ToString()].Y;

            if (Math.Abs(X_Difference)<accuracy)
            {
                command+=Environment.NewLine;
                command+="Pięść dobrze ustawiona na X";
                X_AX=true;

            }
            else if (Math.Abs(X_Difference)>=accuracy)
            {
                if (X_Difference>0)
                {
                    command+=Environment.NewLine;
                    command+="przesuń pięść w prawo";
                }
                else if (X_Difference<0)
                {
                    command+=Environment.NewLine;
                    command+="przesuń pięść w lewo";
                }
                X_AX=false;

            }

            if (Math.Abs(Y_Difference)<accuracy)
            {
                command+=Environment.NewLine;
                command+="Pięść dobrze ustawiona na Y";
                Y_AX=true;
            }
            else if (Math.Abs(Y_Difference)>=accuracy)
            {
                if (Y_Difference>0)
                {
                    command+=Environment.NewLine;
                    command+="przesuń pięść do góry ";

                }
                else if (Y_Difference<0)
                {
                    command+=Environment.NewLine;
                    command+="przesuń pięść do dołu";
                }
                Y_AX=false;
            }
            if (X_AX==true&&Y_AX==true)
            {
                PositioningBool["Wyprowadzenie_ciosu"].Item1["StraightLeftPunchHeight"]=true;
            }
            return command;
        }



        //private string RightFoodPositioning(Dictionary<string, Vector3> Person, float accuracy)
        //{
        //    string command = string.Empty;
        //    return command;
        //}
        //Dictionary<string, Vector3> Teacher, Dictionary<string, Vector3> Student, float accuracy
        //if(Math.Abs())
        //  {

        //  }
        //  else if(Math.Abs())
        //  {
        //      if()
        //      {


        //      }
        //      else if ()
        //      {

        //      }
        //  }
        //--------------------------------------------------------------------------------------------//do ustawiania człowieczka koniec
        public void ShowMovementOnTeacher(int Start, int End)
        {
            SecondPage.TeacherStartPosition=Start;
            SecondPage.TeacherActualPosition=Start;
            SecondPage.TeacherEndPosition=End;
            SecondPage.ShowTime=true;
        }
        public void SetStartingPosition(int Start)
        {
            SecondPage.TeacherStartPosition=Start;
            SecondPage.SetStartPosition=true;
        }
        void TimerForString_Tick(object sender, object e)
        {
            CommandsToJointsIndex=0;
            TimerForString.Stop();
        }
        void TimerFor_jestgit_Tick(object sender, object e)
        {
            CommandsToJointsIndex++;
            cangetthru=false;
            przeszło=0;
            TimerFor_jestgit.Stop();
        }
        public void StartTimer()
        {
            TimerFor_jestgit=new DispatcherTimer();
            TimerFor_jestgit.Interval=new TimeSpan(00, 0, 0, 1);
            TimerFor_jestgit.Tick+=TimerFor_jestgit_Tick;
            TimerFor_jestgit.Start();
        }
        public void SetSecondPageInMeasurement(SecondPage secondpage_as_param)
        {
            SecondPageObj=secondpage_as_param;
        }
        public void StartStudentRecording()
        {
            //SecondPageObj.RecPeerButtonClick();
        }
        //public void RecPeerButtonClick()
        //{
        //    //ButtonAutomationPeer peer=new ButtonAutomationPeer(SecondPage.Rec);
        //    //IInvokeProvider invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
        //    //invokeProv.Invoke();
        //}

    }
}
