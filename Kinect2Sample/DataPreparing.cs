﻿using System;
using System.Collections.Generic;
using WindowsPreview.Kinect;
using System.IO;
using Windows.Storage;
using Windows.ApplicationModel;
using System.Threading.Tasks;
using System.Text;

namespace Kinect2Sample
{

    class DataPreparing
    {
        public static int ilosc_wystapien=0;
        String formula;
        Dictionary<String, Tuple<float, float, float>> body_joints_position;
        Tuple<float, float, float> position_coordinates;
        //DictionaryObject[] dictionary_object;

        //public DataPreparing(Dictionary<String, Tuple<float, float, float>> body_joints_position, Tuple<float, float, float> position)
        //{
        //    body_joints_position=new Dictionary<string, Tuple<float, float, float>>();
        //}

        public String joints_position(Body[] bodies)
        {
            formula=String.Empty;
            ilosc_wystapien++;
            String tmp_help = "tmp_help = null ";
            formula+=string.Format(Environment.NewLine);
            formula+="ilosc wystapien metody  "+ilosc_wystapien;
                 
            int body_count = 0;
            int joint_count = 0;

            foreach (var body in bodies)
            {
                body_count++;
                joint_count=0;

                formula+=string.Format(Environment.NewLine);
                formula+="Body Nr="+body_count;

                if (body!=null)
                {
                    formula+=" body!=null ";

                    body_joints_position= new Dictionary<string, Tuple<float, float, float>>();
                    if (body.IsTracked)
                    {
                        formula+="  body is tracked  ";                 
                        foreach (var joint_type in body.Joints.Keys)
                        {
                            joint_count++;                       
                            position_coordinates=new Tuple<float, float, float>(body.Joints[joint_type].Position.X, body.Joints[joint_type].Position.Y, body.Joints[joint_type].Position.Z);

                            body_joints_position.Add(joint_type.ToString(), position_coordinates);

                            tmp_help="Joint Nr "+joint_count+"      "+joint_type.ToString()+" X="+body.Joints[joint_type].Position.X.ToString()+" Y="+body.Joints[joint_type].Position.Y.ToString()+" Z="+body.Joints[joint_type].Position.Z.ToString();

                            if (tmp_help!=null)
                            {
                                formula+=string.Format(Environment.NewLine);
                                formula+=tmp_help.ToString();
                            }
                        }                        
                    }
                }
            }           
            return formula;
        }         

    }
}
