﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using WindowsPreview.Kinect;
using System.ComponentModel;
using Windows.Storage.Streams;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Windows.UI.Xaml.Shapes;
using Windows.Storage;
using System.Threading.Tasks;
using SharpDX;
using Windows.UI.Xaml.Automation.Peers;
using Windows.UI.Xaml.Automation.Provider;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;

namespace Kinect2Sample
{
    public enum DisplayFrameType
    {
        Infrared,
        Color,
        Depth,
        BodyJoints
    }
  
    public  partial class MainPage : Page, INotifyPropertyChanged
    {
        public static ThreadPoolTimer DelayTimer;
        private Body[] Body_pointer;
        private DataPreparing Datapreparing;

        public string jointsposition_in_string = " nic !!!";

        private const DisplayFrameType DEFAULT_DISPLAYFRAMETYPE = DisplayFrameType.Infrared;

        /// <summary>
        /// The highest value that can be returned in the InfraredFrame.
        /// It is cast to a float for readability in the visualization code.
        /// </summary>
        private const float InfraredSourceValueMaximum = (float)ushort.MaxValue;

        /// <summary>
        /// Used to set the lower limit, post processing, of the
        /// infrared data that we will render.
        /// Increasing or decreasing this value sets a brightness 
        /// "wall" either closer or further away.
        /// </summary>
        private const float InfraredOutputValueMinimum = 0.01f;

        /// <summary>
        /// The upper limit, post processing, of the
        /// infrared data that will render.
        /// </summary>
        private const float InfraredOutputValueMaximum = 1.0f;

        /// <summary>
        /// The InfraredSceneValueAverage value specifies the average infrared 
        /// value of the scene. This value was selected by analyzing the average 
        /// pixel intensity for a given scene. 
        /// This could be calculated at runtime to handle different IR conditions
        /// of a scene (outside vs inside).
        /// </summary>
        private const float InfraredSceneValueAverage = 0.08f;

        /// <summary>
        /// The InfraredSceneStandardDeviations value specifies the number of 
        /// standard deviations to apply to InfraredSceneValueAverage. 
        /// This value was selected by analyzing data from a given scene.
        /// This could be calculated at runtime to handle different IR conditions
        /// of a scene (outside vs inside).
        /// </summary>
        private const float InfraredSceneStandardDeviations = 3.0f;

        // Size of the RGB pixel in the bitmap
        private const int BytesPerPixel = 4;

        public static KinectSensor kinectSensor = null;
        private string statusText = null;
        private WriteableBitmap bitmap = null;
        private FrameDescription currentFrameDescription;
        private DisplayFrameType currentDisplayFrameType;
        private MultiSourceFrameReader multiSourceFrameReader = null;
        private CoordinateMapper coordinateMapper = null;
        private BodiesManager bodiesManager = null;

        public PositionBuffor positionBuffor = new PositionBuffor();

        //Infrared Frame 
        private ushort[] infraredFrameData = null;
        private byte[] infraredPixels = null;

        //Depth Frame
        private ushort[] depthFrameData = null;
        private byte[] depthPixels = null;
        public int howmanytimes = 0;

        //BodyMask Frames
        //private DepthSpacePoint[] colorMappedToDepthPoints = null;

        //Body Joints are drawn here
        private Canvas drawingCanvas;
        private int frame_counter;
        private bool buttonWasClicked = false;
        public double TimeCounter { get; set; }
        public DispatcherTimer Timer;
        public DispatcherTimer delay;



        public event PropertyChangedEventHandler PropertyChanged;

        public string StatusText
        {
            get { return statusText; }
            set
            {
                if (statusText!= value)
                {
                    statusText= value;
                    if (PropertyChanged!= null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        public FrameDescription CurrentFrameDescription
        {
            get { return currentFrameDescription; }
            set
            {
                if (currentFrameDescription!= value)
                {
                    currentFrameDescription= value;
                    if (PropertyChanged!= null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CurrentFrameDescription"));
                    }
                }
            }
        }

        public MainPage()
        {
            frame_counter=0;
            // one sensor is currently supported
            kinectSensor= KinectSensor.GetDefault();

            SetupCurrentDisplay(DEFAULT_DISPLAYFRAMETYPE);

            coordinateMapper=kinectSensor.CoordinateMapper;

            multiSourceFrameReader=kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Infrared | FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.BodyIndex | FrameSourceTypes.Body);
            
            // wire handler for frame arrival
            multiSourceFrameReader.MultiSourceFrameArrived +=Reader_MultiSourceFrameArrived;
            //multiSourceFrameReader.MultiSourceFrameArrived+=SensorSkeletonFrameReady;
            multiSourceFrameReader.MultiSourceFrameArrived+=WritingData;


            // set IsAvailableChanged event notifier
            kinectSensor.IsAvailableChanged +=Sensor_IsAvailableChanged;

            // use the window object as the view model in this simple example
            DataContext= this;

            Datapreparing=new DataPreparing();

            // open the sensor
            kinectSensor.Open();
            InitializeComponent();
            //this.Suspending+=OnSuspending;
            CreateDelayTimer();
        }

        private void SetupCurrentDisplay(DisplayFrameType newDisplayFrameType)
        {
            currentDisplayFrameType = newDisplayFrameType;
            // Frames used by more than one type are declared outside the switch
            FrameDescription colorFrameDescription = null;
            // reset the display methods
            if (BodyJointsGrid!= null)
            {
                BodyJointsGrid.Visibility = Visibility.Collapsed;
            }
            if (FrameDisplayImage!= null)
            {
                FrameDisplayImage.Source = null;
            }
            switch (currentDisplayFrameType)
            {
                case DisplayFrameType.Infrared:
                    FrameDescription infraredFrameDescription = kinectSensor.InfraredFrameSource.FrameDescription;
                    CurrentFrameDescription= infraredFrameDescription;
                    // allocate space to put the pixels being received and converted
                    infraredFrameData= new ushort[infraredFrameDescription.Width * infraredFrameDescription.Height];
                    infraredPixels= new byte[infraredFrameDescription.Width * infraredFrameDescription.Height * BytesPerPixel];
                    bitmap= new WriteableBitmap(infraredFrameDescription.Width, infraredFrameDescription.Height);
                    break;

                case DisplayFrameType.Color:
                    colorFrameDescription =kinectSensor.ColorFrameSource.FrameDescription;
                    CurrentFrameDescription= colorFrameDescription;
                    // create the bitmap to display
                    bitmap= new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height);
                    break;

                case DisplayFrameType.Depth:
                    FrameDescription depthFrameDescription = kinectSensor.DepthFrameSource.FrameDescription;
                    CurrentFrameDescription= depthFrameDescription;
                    // allocate space to put the pixels being received and converted
                    depthFrameData= new ushort[depthFrameDescription.Width * depthFrameDescription.Height];
                    depthPixels= new byte[depthFrameDescription.Width * depthFrameDescription.Height * BytesPerPixel];
                    bitmap= new WriteableBitmap(depthFrameDescription.Width, depthFrameDescription.Height);
                    break;

                case DisplayFrameType.BodyJoints:
                    // instantiate a new Canvas
                    drawingCanvas= new Canvas();
                    // set the clip rectangle to prevent rendering outside the canvas
                    drawingCanvas.Clip = new RectangleGeometry();
                    drawingCanvas.Clip.Rect = new Rect(0.0, 0.0, BodyJointsGrid.Width, BodyJointsGrid.Height);
                    drawingCanvas.Width =BodyJointsGrid.Width;
                    drawingCanvas.Height =BodyJointsGrid.Height;
                    // reset the body joints grid
                    BodyJointsGrid.Visibility = Visibility.Visible;
                    BodyJointsGrid.Children.Clear();
                    // add canvas to DisplayGrid
                    BodyJointsGrid.Children.Add(drawingCanvas);
                    bodiesManager = new BodiesManager(coordinateMapper, drawingCanvas, kinectSensor.BodyFrameSource.BodyCount);
                    break;
                default:
                    break;
            }
        }

        private void Sensor_IsAvailableChanged(KinectSensor sender, IsAvailableChangedEventArgs args)
        {
            StatusText=kinectSensor.IsAvailable ? "Running" : "Not Available";
        }

        private void Reader_MultiSourceFrameArrived(MultiSourceFrameReader sender, MultiSourceFrameArrivedEventArgs e)
        {

            MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();

            // If the Frame has expired by the time we process this event, return.
            if (multiSourceFrame == null)
            {
                return;
            }
            DepthFrame depthFrame = null;
            ColorFrame colorFrame = null;
            InfraredFrame infraredFrame = null;
            BodyFrame bodyFrame = null;
            //BodyIndexFrame bodyIndexFrame = null;
            //IBuffer depthFrameData = null;
            //IBuffer bodyIndexFrameData = null;
            // Com interface for unsafe byte manipulation
            //IBufferByteAccess bodyIndexByteAccess = null;

            switch (currentDisplayFrameType)
            {
                case DisplayFrameType.Infrared:
                    using (infraredFrame = multiSourceFrame.InfraredFrameReference.AcquireFrame())
                    {
                        ShowInfraredFrame(infraredFrame);
                    }
                    break;
                case DisplayFrameType.Color:
                    using (colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
                    {
                        ShowColorFrame(colorFrame);
                    }
                    break;
                case DisplayFrameType.Depth:
                    using (depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
                    {
                        ShowDepthFrame(depthFrame);
                    }
                    break;     
                case DisplayFrameType.BodyJoints:
                    using (bodyFrame = multiSourceFrame.BodyFrameReference.AcquireFrame())
                    {
                        ShowBodyJoints(bodyFrame);
                    }
                    break;
                default:
                    break;                  
            }
        }

        public void WritingData(MultiSourceFrameReader sender, MultiSourceFrameArrivedEventArgs e)
        {   
                if (buttonWasClicked)
                {
                    MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();
                    BodyFrame bodyFrame = null;
                    using (bodyFrame=multiSourceFrame.BodyFrameReference.AcquireFrame())
                    {
                        Body[] bodies = new Body[kinectSensor.BodyFrameSource.BodyCount];
                        bool dataReceived = false;
                        if (bodyFrame!=null)
                        {
                            bodyFrame.GetAndRefreshBodyData(bodies);
                            dataReceived=true;
                        }

                        if (dataReceived)
                        {
                            positionBuffor.UpdateBodies(bodies,howmanytimes);
                            howmanytimes++;
                        }
                    } 
                }
            buttonWasClicked = false;
        }

        private  void ShowBodyJoints(BodyFrame bodyFrame)
        {
            Body[] bodies = new Body[kinectSensor.BodyFrameSource.BodyCount];
            bool dataReceived = false;
            if (bodyFrame!=null)
            {
                bodyFrame.GetAndRefreshBodyData(bodies);
                dataReceived=true;
            }

            if (dataReceived)
            {
                bodiesManager.UpdateBodiesAndEdges(bodies);
            }
        }

        //unsafe private void ShowMappedBodyFrame(int depthWidth, int depthHeight, IBuffer bodyIndexFrameData, IBufferByteAccess bodyIndexByteAccess)
        //{
        //    bodyIndexByteAccess = (IBufferByteAccess)bodyIndexFrameData;
        //    byte* bodyIndexBytes = null;
        //    bodyIndexByteAccess.Buffer(out bodyIndexBytes);

        //    fixed (DepthSpacePoint* colorMappedToDepthPointsPointer = this.colorMappedToDepthPoints)
        //    {
        //        IBufferByteAccess bitmapBackBufferByteAccess = (IBufferByteAccess)this.bitmap.PixelBuffer;

        //        byte* bitmapBackBufferBytes = null;
        //        bitmapBackBufferByteAccess.Buffer(out bitmapBackBufferBytes);

        //        // Treat the color data as 4-byte pixels
        //        uint* bitmapPixelsPointer = (uint*)bitmapBackBufferBytes;

        //        // Loop over each row and column of the color image
        //        // Zero out any pixels that don't correspond to a body index
        //        int colorMappedLength = this.colorMappedToDepthPoints.Length;
        //        for (int colorIndex = 0; colorIndex < colorMappedLength; ++colorIndex)
        //        {
        //            float colorMappedToDepthX = colorMappedToDepthPointsPointer[colorIndex].X;
        //            float colorMappedToDepthY = colorMappedToDepthPointsPointer[colorIndex].Y;

        //            // The sentinel value is -inf, -inf, meaning that no depth pixel corresponds to this color pixel.
        //            if (!float.IsNegativeInfinity(colorMappedToDepthX) &&
        //                !float.IsNegativeInfinity(colorMappedToDepthY))
        //            {
        //                // Make sure the depth pixel maps to a valid point in color space
        //                int depthX = (int)(colorMappedToDepthX + 0.5f);
        //                int depthY = (int)(colorMappedToDepthY + 0.5f);

        //                // If the point is not valid, there is no body index there.
        //                if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
        //                {
        //                    int depthIndex = (depthY * depthWidth) + depthX;

        //                    // If we are tracking a body for the current pixel, do not zero out the pixel
        //                    if (bodyIndexBytes[depthIndex] != 0xff)
        //                    {
        //                        // this bodyIndexByte is good and is a body, loop again.
        //                        continue;
        //                    }
        //                }
        //            }
        //            // this pixel does not correspond to a body so make it black and transparent
        //            bitmapPixelsPointer[colorIndex] = 0;
        //        }
        //    }

        //    this.bitmap.Invalidate();
        //    FrameDisplayImage.Source = this.bitmap;

        //}

        private void ShowDepthFrame(DepthFrame depthFrame)
        {
            bool depthFrameProcessed = false;
            ushort minDepth = 0;
            ushort maxDepth = 0;

            if (depthFrame != null)
            {
                FrameDescription depthFrameDescription = depthFrame.FrameDescription;

                // verify data and write the new infrared frame data to the display bitmap
                if (((depthFrameDescription.Width * depthFrameDescription.Height)
                    ==infraredFrameData.Length) &&
                    (depthFrameDescription.Width ==bitmap.PixelWidth) &&
                    (depthFrameDescription.Height ==bitmap.PixelHeight))
                {
                    // Copy the pixel data from the image to a temporary array
                    depthFrame.CopyFrameDataToArray(depthFrameData);

                    minDepth = depthFrame.DepthMinReliableDistance;
                    maxDepth = depthFrame.DepthMaxReliableDistance;
                    //maxDepth = 8000;

                    depthFrameProcessed = true;
                }
            }

            // we got a frame, convert and render
            if (depthFrameProcessed)
            {
                ConvertDepthDataToPixels(minDepth, maxDepth);
                RenderPixelArray(depthPixels);
            }
        }

        private void ConvertDepthDataToPixels(ushort minDepth, ushort maxDepth)
        {
            int colorPixelIndex = 0;
            // Shape the depth to the range of a byte
            int mapDepthToByte = maxDepth / 256;

            for (int i = 0; i <depthFrameData.Length; ++i)
            {
                // Get the depth for this pixel
                ushort depth = depthFrameData[i];

                // To convert to a byte, we're mapping the depth value to the byte range.
                // Values outside the reliable depth range are mapped to 0 (black).
                byte intensity = (byte)(depth >= minDepth &&
                    depth <= maxDepth ? (depth / mapDepthToByte) : 0);

                depthPixels[colorPixelIndex++] = intensity; //Blue
                depthPixels[colorPixelIndex++] = intensity; //Green
                depthPixels[colorPixelIndex++] = intensity; //Red
                depthPixels[colorPixelIndex++] = 255; //Alpha
            }
        }

        private void ShowColorFrame(ColorFrame colorFrame)
        {
            bool colorFrameProcessed = false;

            if (colorFrame != null)
            {
                FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                // verify data and write the new color frame data to the Writeable bitmap
                if ((colorFrameDescription.Width ==bitmap.PixelWidth) && (colorFrameDescription.Height ==bitmap.PixelHeight))
                {
                    if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                    {
                        colorFrame.CopyRawFrameDataToBuffer(bitmap.PixelBuffer);
                    }
                    else
                    {
                        colorFrame.CopyConvertedFrameDataToBuffer(bitmap.PixelBuffer, ColorImageFormat.Bgra);
                    }

                    colorFrameProcessed = true;
                }
            }

            if (colorFrameProcessed)
            {
                bitmap.Invalidate();
                FrameDisplayImage.Source =bitmap;
            }
        }

        private void ShowInfraredFrame(InfraredFrame infraredFrame)
        {
            bool infraredFrameProcessed = false;

            if (infraredFrame != null)
            {
                FrameDescription infraredFrameDescription = infraredFrame.FrameDescription;

                // verify data and write the new infrared frame data to the display bitmap
                if (((infraredFrameDescription.Width * infraredFrameDescription.Height)
                    ==infraredFrameData.Length) &&
                    (infraredFrameDescription.Width ==bitmap.PixelWidth) &&
                    (infraredFrameDescription.Height ==bitmap.PixelHeight))
                {
                    // Copy the pixel data from the image to a temporary array
                    infraredFrame.CopyFrameDataToArray(infraredFrameData);

                    infraredFrameProcessed = true;
                }
            }

            // we got a frame, convert and render
            if (infraredFrameProcessed)
            {
                ConvertInfraredDataToPixels();
                RenderPixelArray(infraredPixels);
            }
        }

        private void ConvertInfraredDataToPixels()
        {
            // Convert the infrared to RGB
            int colorPixelIndex = 0;
            for (int i = 0; i <infraredFrameData.Length; ++i)
            {
                // normalize the incoming infrared data (ushort) to a float ranging from 
                // [InfraredOutputValueMinimum, InfraredOutputValueMaximum] by
                // 1. dividing the incoming value by the source maximum value
                float intensityRatio = (float)infraredFrameData[i] / InfraredSourceValueMaximum;

                // 2. dividing by the (average scene value * standard deviations)
                intensityRatio /= InfraredSceneValueAverage * InfraredSceneStandardDeviations;

                // 3. limiting the value to InfraredOutputValueMaximum
                intensityRatio = Math.Min(InfraredOutputValueMaximum, intensityRatio);

                // 4. limiting the lower value InfraredOutputValueMinimum
                intensityRatio = Math.Max(InfraredOutputValueMinimum, intensityRatio);

                // 5. converting the normalized value to a byte and using the result
                // as the RGB components required by the image
                byte intensity = (byte)(intensityRatio * 255.0f);
                infraredPixels[colorPixelIndex++] = intensity; //Blue
                infraredPixels[colorPixelIndex++] = intensity; //Green
                infraredPixels[colorPixelIndex++] = intensity; //Red
                infraredPixels[colorPixelIndex++] = 255;       //Alpha
            }
        }

        private void RenderPixelArray(byte[] pixels)
        {
            pixels.CopyTo(bitmap.PixelBuffer);
            bitmap.Invalidate();
            FrameDisplayImage.Source =bitmap;
        }



        private void InfraredButton_Click(object sender, RoutedEventArgs e)
        {
            SetupCurrentDisplay(DisplayFrameType.Infrared);
        }

        private void ColorButton_Click(object sender, RoutedEventArgs e)
        {
            SetupCurrentDisplay(DisplayFrameType.Color);
        }

        private void DepthButton_Click(object sender, RoutedEventArgs e)
        {
            SetupCurrentDisplay(DisplayFrameType.Depth);
        }

        private  void BodyJointsButton_Click(object sender, RoutedEventArgs e)
        {
            SetupCurrentDisplay(DisplayFrameType.BodyJoints);
        }

//        [Guid("905a0fef-bc53-11df-8c49-001e4fc686da"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]

        //interface IBufferByteAccess
        //{
        //    unsafe void Buffer(out byte* pByte);
        //}


        private async Task WriteToFile(String data)
        {
            // Get the text data from the textbox. 
            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(jointsposition_in_string.ToCharArray());

            // Get the local folder.
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

            //textBlock.TextAlignment.Text.=Windows.Storage.ApplicationData.Current.LocalFolder.Path.ToString();
            System.Diagnostics.Debug.WriteLine("----------------------------------------"+Windows.Storage.ApplicationData.Current.LocalFolder.Path.ToString());
            // Create a new folder name DataFolder.
            var dataFolder = await local.CreateFolderAsync("DataFolder", CreationCollisionOption.OpenIfExists);

            // Create a new file named DataFile.txt.
            var file = await dataFolder.CreateFileAsync("DataFileDict.txt", CreationCollisionOption.ReplaceExisting);

            // Write the data from the textbox.
            using (var s = await file.OpenStreamForWriteAsync())
            {
                s.Write(fileBytes, 0, fileBytes.Length);
            }
        }

        private void textBlock_SelectionChanged_1(object sender, RoutedEventArgs e)
        {
        }

        private void Zapis_do_buffora_Click(object sender, RoutedEventArgs e)
        {
            buttonWasClicked=true;
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            kinectSensor.Close();
            this.Frame.Navigate(typeof(SecondPage), null);
        }

        private void Auto_flash_Click(object sender, RoutedEventArgs e)
        {
            TimeCounter=0;
            PositionBuffor.Sequentions.Clear();
            Start_timer();
        }
        public void Start_timer()
        {
            Timer=new DispatcherTimer();
            delay=new DispatcherTimer();
            Timer.Tick+=timer_Tick;
            delay.Tick += Timer2_Stop_Timer_Start;
            Timer.Interval=new TimeSpan(00, 0, 0, 0, 20);
            delay.Interval=new TimeSpan(00, 0, 0, 10);
            delay.Start();
            //Timer.Start();
        }

        void Timer2_Stop_Timer_Start(object sender, object e)
        {
            delay.Stop();
            textBlock.Text = "NAGRYWA";
            Timer.Start();
        }

        void timer_Tick(object sender, object e)
        {
            int i = 1;
            if (SomeTextToTimer!=null)
                i=SomeTextToTimer;
            TimeCounter += 1;
            textBlock.Text = (i-TimeCounter).ToString();
            buttonWasClicked = true;
            if (TimeCounter>=i)
            {
                Timer.Stop();
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }
        public int SomeTextToTimer { get; set; }

        private void appBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text!="")
            {
                string tmptext = textBox.Text;
                tmptext.ToCharArray();
                for (int i = 0; i<tmptext.Length; i++)
                {
                    if (!( tmptext[i]>='0'&&tmptext[i]<='9' ))
                    {
                        return;
                    }
                }
                SomeTextToTimer=int.Parse(textBox.Text);
                textBlock.Text = SomeTextToTimer.ToString();
            }
        }
        public void PeerCheckBoxClick()
        {
            ButtonAutomationPeer peer = new ButtonAutomationPeer(Next);
            IInvokeProvider invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
            invokeProv.Invoke();
        }
        public void CreateDelayTimer()
        {
            DelayTimer=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            if (DelayTimer!=null)
                            {
                                DelayTimer.Cancel();
                            }
                            //PeerCheckBoxClick();
                        });
                },
                TimeSpan.FromMilliseconds(3));
        }
  //    private void OnSuspending(object sender, SuspendingEventArgs e)
  //        {
  //            var deferral = e.SuspendingOperation.GetDeferral();
  //            //TODO: Save application state and stop any background activity
  //            deferral.Complete();
  //        }
  //      protected override void OnLaunched(LaunchActivatedEventArgs args)
  //       {
  //           Frame rootFrame = Window.Current.Content as Frame;
 
  //           // Do not repeat app initialization when the Window already has content,
  //           // just ensure that the window is active
  //           if (rootFrame == null)
  //           {
  //               // Create a Frame to act as the navigation context and navigate to the first page
  //               rootFrame = new Frame();
 
  //               if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
  //               {
  //                   //TODO: Load state from previously suspended application
  //               }
 
  //               // Place the frame in the current Window
  //               Window.Current.Content = rootFrame;
  //           }
 
  //           if (rootFrame.Content == null)
  //           {
  //               // When the navigation stack isn't restored navigate to the first page,
  //               // configuring the new page by passing required information as a navigation
  //               // parameter
  //               if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
  //               {
  //                   throw new Exception("Failed to create initial page");
  //               }
  //           }
  //// Ensure the current window is active
  //           Window.Current.Activate();
  //       }
}
}

