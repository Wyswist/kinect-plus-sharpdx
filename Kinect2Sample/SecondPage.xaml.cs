﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Input;
using System.Windows.Input;
using Windows.Media.Core;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Input;
using WindowsPreview.Kinect;
using Newtonsoft.Json;
using System.Threading.Tasks; // for Task class
using System.Threading; // for Thread class
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml.Automation.Peers;
using Windows.UI.Xaml.Automation.Provider;


//using System.Windows.Forms;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Kinect2Sample
{
    public partial class SecondPage : Page
    {
        public static KinectSensor SPkinectSensor = null;
        private MultiSourceFrameReader multiSourceFrameReader = null;
        //private stri9ng statusText = null;
        //private WriteableBitmap bitmap = null;
        //private FrameDescription currentFrameDescription;
        //private DisplayFrameType currentDisplayFrameType;

        //private CoordinateMapper coordinateMapper = null;
        //private BodiesManager bodiesManager = null;

        private Device device;
        Mesh mesh = new Mesh("Cube", 60, 60);
        Camera mera = new Camera();
        Measurement distance;

        private bool IsLeftClicked=false;
        private bool IsRightClicked=false;
        private bool IsMiddleClicked=false;
        public Point PointerCurrentPosition { get; set; }
        public Point PointerClickedCurrentPosition { get; set; }
        public Point PointerRealsedCurrentPosition { get; set; }
        public float StartingRotationOnX { get; set; }
        public float StartingRotationOnY { get; set; }
        public float StartingTargetOnX { get; set; }
        public float StartingTargetOnY { get; set; }
        public float WheelDelta { get; set; }
        public static bool move_to_next_position = false;
        public  TupleList<JointType, JointType> bones =new BodyInfo(Colors.Aqua, 2).Bones;
        public Dictionary<int, Dictionary<string, Tuple<float, float, float>>> VicariusDictionary ;
        public Dictionary<string, Tuple<float, float, float>> HelperVicDictionary;
        public Dictionary<int, Dictionary<string, Tuple<float, float, float>>> dic { get; set; }
        public static int SelectedIndex { get; set; }
        public static int StudentRecComboBoxSelectedIndex { get; set; }
        public static int StudentRecComboBoxActualSelectedIndex { get; set; }
        public static int BigHybridActualIndex { get; set; }


        public string WchichButtonIPressed { get; set; }
        public static bool Draw_axis_Click_bool { get; set; }
        public static bool Positioning_bool;
        public static int how_many_dots_in_half_ax = 9;
        private Resizing Resizing;
        public static int TeacherStartPosition;
        public static int TeacherEndPosition ;
        public static int StudentStartPosition ;
        public static int StudentRecEndPosition ;
        public static int TeacherActualPosition { get; set; }
        public static int StudentActualPosition { get; set; }
        public static bool SetStartPosition { get; set; }
        private Timers Timer = new Timers();
        public static bool once { get; set; }
        public static ThreadPoolTimer DelayTimerForCommands;
        public static ThreadPoolTimer DelayTimerForShowMovement;
        public static ThreadPoolTimer DelayTimerForStudentRecordShowMovement;
        public static ThreadPoolTimer DelayTimerForTeacher;
        public static ThreadPoolTimer DelayTimerForTeacherDelay;
        public static ThreadPoolTimer DelayTimerForStudentRecDelay;


        public bool once_for_delay  { get; set; }
         public bool ShowTime_Once  { get; set; }
        public static bool ShowTime { get; set; }
        public bool SECOND_ShowTime { get; set; }
        public double RecTimeCounter { get; set; }
        public DispatcherTimer RecTimer;
        public DispatcherTimer RecDelay;
        private bool WasWritingToBuffor = false;
        private int Countdown;
        public PositionBuffor SPPositionBuffor = new PositionBuffor();
        private int Rysuj_Nauczyciela_Click_counter = 0;
        private int Draw_platform_Click_counter = 0;
        private int Draw_axis_Click_counter = 0;
        private int SetOnPosition_Click_counter = 0;
        private int Rysuj_Hybryde_Click_counter = 0;






        private int Ile_Razy_StudentRec=0;
        private bool once_go_thru_for_RecTimer = true;

        //------------------------------------------------------------------------------------------------------------------------------------------------
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Choose the back buffer resolution here
            WriteableBitmap bmp = new WriteableBitmap((int)Border.Width,(int)Border.Height-20);

            device = new Device(bmp);

            // Our Image XAML control
            frontBuffer.Source = bmp;

            mera.Position = new Vector3(0, 0,-230);
            mera.Target = new Vector3(0, 0, 0);

            // Registering to the XAML rendering loop
            CompositionTarget.Rendering += CompositionTarget_Rendering;
            CompositionTarget.Rendering += ChangingRotation;
            CompositionTarget.Rendering += ChangingTargetPosition;
        }

        // Rendering loop handler
        void CompositionTarget_Rendering(object sender, object e)
        {
            device.Clear(0, 0, 0, 255);

            // rotating slightly the cube during each frame rendered
            //mesh.Rotation=new Vector3(mesh.Rotation.X, mesh.Rotation.Y+0.01f, mesh.Rotation.Z);
            //mesh.Rotation=new Vector3(0, 0.3f, mesh.Rotation.Z);

            if (Kinect_Stream.IsChecked==true&&!SPkinectSensor.IsOpen)
            {
                SPkinectSensor.Open();
            }
            if (Kinect_Stream.IsChecked==false&&SPkinectSensor.IsOpen)
            {
                Device.which="";
                Device.Draw_Teacher="draw_Teacher";
                SPkinectSensor.Close();
            }
            //if (Measure.IsChecked==true&&Positioning_bool==false)
            //{
            //    //TeacherStudentRefManDistance();
            //    textBlock2.Text=distance.SettingFootPosition(mesh.ResizedTeacher, mesh.Student, 0.03f);
            //}
            //if (Measure.IsChecked==false)
            //{
            //}
            //if (move_to_next_position)
            //{
            //    MoveToNextPositionFunction();
            //    if (Listbox_combobox!=null)
            //        Listbox_combobox.SelectedIndex++;
            //    mesh.StartSessionForTeacher(SelectedIndex, dic);
            //    move_to_next_position=false;
            //}
            if(SetStartPosition)
            {
                SetPosition();
            }
            //if (Positioning_bool)
            //{
            //    //textBlock2.Text="";
            //    //textBlock2.Text=distance.CommandsToJoints(mesh.FullResizedTeacher, mesh.Student, 0.03f);
            //}

            //if (mesh.Teacher!=null)
            //   distance.AnglesBetweenBonesCommands(mesh.Teacher, "Teacher");

            //if (mesh.Student!=null)
            //    distance.AnglesBetweenBonesCommands(mesh.Student, "Student");

            //if (Kinect_Stream.IsChecked==true&&distance.StudentAnglesBetweenBones!=null&&distance.TeacherAnglesBetweenBones!=null&&mesh.Student!=null)
            //{
            //    AnglesComparisonTextBlock.Text=distance.AnglesComparisonTheSecondV(mesh.Teacher, mesh.Student, 0.03f);
            //}

            //if (Kinect_Stream.IsChecked==true/*&&mesh.Student!=null*/)
            //{
            //    if (once_for_delay)
            //        CreateDelayTimer();
            //}
            if (ShowTime)
            {
                if (ShowTime_Once==true)
                {
                    CreateDelayTimerForShowMovement();
                }
            }

            // Doing the various matrix operations
            device.Render(mera, mesh);
            // Flushing the back buffer into the front buffer
            device.Present();
        }

        public SecondPage()
        {
            this.InitializeComponent();
            StartingRotationOnX = mesh.Rotation.X;
            StartingRotationOnY = mesh.Rotation.Y;
            Positioning_bool=false;
            // one sensor is currently supported
            SPkinectSensor=KinectSensor.GetDefault();
            multiSourceFrameReader=SPkinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Infrared|FrameSourceTypes.Color|FrameSourceTypes.Depth|FrameSourceTypes.BodyIndex|FrameSourceTypes.Body);
            // wire handler for frame arrival
            multiSourceFrameReader.MultiSourceFrameArrived+=Reader_MultiSourceFrameArrived;
            multiSourceFrameReader.MultiSourceFrameArrived+=WritingData;

            Resizing=new Resizing();
            SetStartPosition=false;
            once=true;
            once_for_delay = true;
            ShowTime_Once = true;
            ShowTime=false;
            SECOND_ShowTime=false;
            distance=new Measurement(this);
            TeacherStartPosition = 0;
            //TeacherEndPosition = 0;
            StudentStartPosition = 0;
            StudentRecEndPosition= 0;
    }                     


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        public void pointer_MOVEMENT(object sender, PointerRoutedEventArgs e)
        {
            PointerPoint point = e.GetCurrentPoint(Border);
            PointerCurrentPosition = point.Position;
            var x = point.Position.X.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            var y = point.Position.Y.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            var text = string.Format("Current poistion of pointer x:{0}, y:{1}", x, y);
            if (text == null) throw new ArgumentNullException(nameof(text));
        }

        public void pointer_CLICKED(object sender, PointerRoutedEventArgs e)
        {
            IsLeftClicked = e.GetCurrentPoint(Border).Properties.IsLeftButtonPressed;
            IsRightClicked=e.GetCurrentPoint(Border).Properties.IsRightButtonPressed;
            IsMiddleClicked=e.GetCurrentPoint(Border).Properties.IsMiddleButtonPressed;

            PointerClickedCurrentPosition= PointerCurrentPosition;
            if(IsLeftClicked||IsRightClicked||IsMiddleClicked)
            {
                //Angles_textBlock3
            }           
        }

        public void pointer_RELEASED(object sender, PointerRoutedEventArgs e)
        {
            IsLeftClicked = e.GetCurrentPoint(Border).Properties.IsLeftButtonPressed;
            IsRightClicked = e.GetCurrentPoint(Border).Properties.IsRightButtonPressed;
            IsMiddleClicked=e.GetCurrentPoint(Border).Properties.IsMiddleButtonPressed;

            if (!IsRightClicked)
            {
                StartingTargetOnX=mera.Target.X;
                StartingTargetOnY=mera.Target.Y;
            }
            if (!IsLeftClicked)
            {
                StartingRotationOnX=mesh.Rotation.X;
                StartingRotationOnY=mesh.Rotation.Y;
            }
            PointerRealsedCurrentPosition=PointerCurrentPosition;
        }
        public void pointer_WHEELE(object sender, PointerRoutedEventArgs e)
        {
            WheelDelta = e.GetCurrentPoint(Border).Properties.MouseWheelDelta;
            mera.Position=new Vector3(mera.Position.X, mera.Position.Y, mera.Position.Z + WheelDelta/12);
        }

        public Vector2 move()
        {
            var sensitivity = 1000;
            var changing_on_x = (float) ((PointerClickedCurrentPosition.X - PointerCurrentPosition.X)/sensitivity);
            var changing_on_y = (float) ((PointerClickedCurrentPosition.Y - PointerCurrentPosition.Y)/sensitivity);
            return (new Vector2(changing_on_y, changing_on_x));
        }

        void ChangingRotation(object sender, object e)
        {
            var move = this.move();
            if (IsLeftClicked == true)
            {
                mesh.Rotation=new Vector3(StartingRotationOnX+move.X, StartingRotationOnY+move.Y,0);
            }
        }
        void ChangingTargetPosition(object sender, object e)
        {
            var move = this.move();
            if (IsRightClicked==true)
            {
                mera.Target=new Vector3(StartingTargetOnX+move.Y, StartingTargetOnY+move.X, mera.Target.Z);
            }
        }

        private void Reader_MultiSourceFrameArrived(MultiSourceFrameReader sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();
            // If the Frame has expired by the time we process this event, return.
            if (multiSourceFrame==null)
            {
                return;
            }
            BodyFrame bodyFrame = null;
            using (bodyFrame=multiSourceFrame.BodyFrameReference.AcquireFrame())
            {
                SPShowBodyJoints(bodyFrame);
            }
        }
        
        private void SPShowBodyJoints(BodyFrame bodyFrame)
        {
            Body[] bodies = new Body[SPkinectSensor.BodyFrameSource.BodyCount];
            bool dataReceived = false;
            if (bodyFrame!=null)
            {
                bodyFrame.GetAndRefreshBodyData(bodies);
                dataReceived=true;
            }
            if (dataReceived)
            {
                UpdateBodiesAndEdges(bodies);
            }
        }

        private void UpdateBodiesAndEdges(Body[] bodies)
        {
            bool hasTrackedBody = false;
            // iterate through each body
            for (int bodyIndex = 0; bodyIndex < bodies.Length; bodyIndex++)
            {
                Body body = bodies[bodyIndex];

                if (body.IsTracked)
                {
                    //textBlock1.Text += "--UpadateBody";
                    UpdateBody(body, bodyIndex);
                }
            }
        }
        private void UpdateBody(Body body, int bodyIndex)
        {
            // update all joints
            mesh.StartSessionForStudent();
            foreach (var jointType in body.Joints.Keys)
            {
                CameraSpacePoint position = body.Joints[jointType].Position;
                mesh.Student.Add(jointType.ToString(), new Vector3(position.X-mesh.MoveStudent, position.Y, position.Z));
            }
            mesh.StudentCenterOfGravity=distance.DeterminationOfTheCenterOfGravity(mesh.Student);
            if (Device.which == "")
            {
                Device.which="Rysuj_aktualnego_studenta";
            }
            if (mesh.Student != null && mesh.Teacher!=null)
            {
                mesh.how_much_resized_teacher_height=Resizing.TeacherHeightResizing(mesh.Teacher, mesh.Student);
                mesh.ResizedTeacherHeight();
                mesh.ResizedTeacherCenterOfGravity=distance.DeterminationOfTheCenterOfGravity(mesh.ResizedTeacher);
                if (mesh.FullResizedTeacher!=null)
                {
                    mesh.how_much_resized_teacher_full=Resizing.TeacherHeightResizing(mesh.FullResizedTeacher, mesh.Student);
                    mesh.FullResizedTeacherHeight();
                }

                if (Device.Draw_resized_Teacher=="")
                {
                    Device.Draw_resized_Teacher="draw_resized_Teacher";
                    Device.Draw_Teacher="";
                }
            }
            mesh.how_far_move_platform_for_Student=Resizing.PlatformMove(mesh.Student);
            mesh.SetPlatform(how_many_dots_in_half_ax, mesh.how_far_move_platform_for_Student);
        }

        public void setting_points_of_position()
        {
            if (WchichButtonIPressed == "take_position")
            {
                if (PositionBuffor.Sequentions != null)
                {
                    if (SelectedIndex > -1)
                    {
                        mesh.StartSessionForTeacher(SelectedIndex, null, PositionBuffor.Sequentions);
                        if (Kinect_Stream.IsChecked==false)
                            Device.Draw_Teacher="draw_Teacher";
                        mesh.how_far_move_platform_for_Teacher = Resizing.PlatformMove(mesh.Teacher);
                        mesh.SetPlatform(how_many_dots_in_half_ax,mesh.how_far_move_platform_for_Teacher);
                    }
                }
            }
            if (WchichButtonIPressed == "Load")
            {
                if (dic != null)
                {
                    if (SelectedIndex > -1)
                    {
                        mesh.StartSessionForTeacher(SelectedIndex, dic);
                        if (Kinect_Stream.IsChecked==false)
                            Device.Draw_Teacher="draw_Teacher";
                        mesh.how_far_move_platform_for_Teacher=Resizing.PlatformMove(mesh.Teacher);
                        mesh.SetPlatform(how_many_dots_in_half_ax,mesh.how_far_move_platform_for_Teacher);
                        //distance.Make_TeacheAnglesBetweenBonesDictionary(mesh.Teacher);
                    }
                }
            }
        }

        private void take_position_Click(object sender, RoutedEventArgs e)
        {
            WchichButtonIPressed = "take_position";
            Listbox_combobox.ItemsSource=null;
            Listbox_combobox.Items.Clear();
            PositionBuffor.howmanytimes=0;
            //if (PositionBuffor.Sequentions!=null)
            //    Listbox_combobox.ItemsSource=PositionBuffor.Sequentions.Select(kvp => new CustomKeyValuePair<int, string> { Key=kvp.Key, Value="Pozycja nr -- " });
         
            if (PositionBuffor.Sequentions!=null)
            {
                foreach (var item in PositionBuffor.Sequentions)
                {
                    Listbox_combobox.Items.Insert(item.Key, "Pozycja nr "+item.Key.ToString());
                }
            }
        }

        private void go_back_Click(object sender, RoutedEventArgs e)
        {
            if (SPkinectSensor.IsOpen)
            {
                SPkinectSensor.Close();
            }
            MainPage.kinectSensor.Open();
            this.Frame.Navigate(typeof (MainPage), null);
        }

        //private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    textBlock.Text=string.Empty;
        //    SelectedIndex=Listbox_combobox.SelectedIndex;
        //    textBlock.Text="NUMER AKTUALNEJ POZYCJI "+SelectedIndex.ToString();
        //    DataContext=this;
        //    setting_points_of_position();
        //}

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
        
            if (VicariusDictionary==null)
            {
                VicariusDictionary=new Dictionary<int, Dictionary<string, Tuple<float, float, float>>>();
            }
            else
            {
                VicariusDictionary.Clear();
            }
            if (HelperVicDictionary == null)
            {
                HelperVicDictionary=new Dictionary<string, Tuple<float, float, float>>();
            }
            else
            {
                HelperVicDictionary.Clear();
            }

            foreach (var Ikey in PositionBuffor.Sequentions.Keys)
            {
                HelperVicDictionary=new Dictionary<string, Tuple<float, float, float>>();
                foreach (var IIkey in PositionBuffor.Sequentions[Ikey])
                {
                    HelperVicDictionary.Add(IIkey.Key.ToString(),new Tuple<float, float, float>(IIkey.Value.X, IIkey.Value.Y, IIkey.Value.Z));
                }
                VicariusDictionary.Add(Ikey, HelperVicDictionary);
            }
            string zserializowany = MojeSerialize(VicariusDictionary);
            var openPicker = new FolderPicker { SuggestedStartLocation=PickerLocationId.Desktop };
            openPicker.FileTypeFilter.Add(".txt");
            openPicker.FileTypeFilter.Add(".dat");
            StorageFolder selectedFolder = await openPicker.PickSingleFolderAsync();
            StorageFile file = await selectedFolder.CreateFileAsync("mojplik.txt", CreationCollisionOption.ReplaceExisting);
            Stream stream = await file.OpenStreamForWriteAsync();
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(zserializowany.ToCharArray());
            writer.Flush();
        }

        private async void Load_Click(object sender, RoutedEventArgs e)
        {
            WchichButtonIPressed = "Load";
            //Represents a UI element that lets the user choose and open files
            //Reprezentuje element interfejsu graficznego, który pozwala użytkownikowi na wybranie i otwarcie plków
            var openPicker = new FileOpenPicker();
            //Identifies the storage location that the file picker presents to the user.
            openPicker.SuggestedStartLocation=PickerLocationId.Desktop;
            //Gets the collection of file types that the file open picker displays.
            openPicker.FileTypeFilter.Add(".txt");
            openPicker.FileTypeFilter.Add(".dat");
            StorageFile selectedFile = await openPicker.PickSingleFileAsync();
            if (selectedFile==null)
                return;
            Stream stream = await selectedFile.OpenStreamForReadAsync();
            BinaryReader reader = new BinaryReader(stream);
            var length = int.Parse(reader.BaseStream.Length.ToString());
            char[] mem = new char[length];
            for (int i = 0; i<length; i++)
            {
                mem[i]=reader.ReadChar();
            }
            string napis = new string(mem);
            dic = new Dictionary<int, Dictionary<string, Tuple<float, float, float>>>();
            dic=MojeDeSerialize(napis);
            //Listbox_combobox.ItemsSource=dic.Select(kvp => new CustomKeyValuePair<int, string> { Key=kvp.Key, Value="Pozycja nr -- " });
            if (dic!=null)
            {
                Listbox_combobox.Items.Clear();
                foreach (var item in dic)
                {
                    Listbox_combobox.Items.Insert(item.Key, "Pozycja nr "+item.Key.ToString());
                }
            }
            if(Listbox_combobox.Items.Count>0)
            {
                Listbox_combobox.SelectedIndex=0;
                Nauczycie_Od.Text=0.ToString();
                Nauczycie_Do.Text=( Listbox_combobox.Items.Count-1 ).ToString();
                TeacherStartPosition=0;
                TeacherEndPosition=Listbox_combobox.Items.Count-1;
            }
        }

        public String MojeSerialize(Dictionary<int, Dictionary<string,Tuple<float,float,float>>> all)
        {
            String abc = JsonConvert.SerializeObject(all, Formatting.None, new JsonSerializerSettings
            {
                TypeNameHandling=TypeNameHandling.Objects,
                TypeNameAssemblyFormat=System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            });
            return abc;
        }

        public Dictionary<int, Dictionary<string, Tuple<float, float, float>>> MojeDeSerialize(String text)
        {

            Dictionary<int, Dictionary<string, Tuple<float, float, float>>> abc;
            abc=JsonConvert.DeserializeObject<Dictionary<int, Dictionary<string, Tuple<float, float, float>>>>(text, new JsonSerializerSettings
            {
                TypeNameHandling=TypeNameHandling.Objects,
                TypeNameAssemblyFormat=System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple,
            });
            return abc;
        }

        private void Make_Ref_Click(object sender, RoutedEventArgs e)
        {
         
            //if (WchichButtonIPressed=="take_position")
            //{
            //    if (PositionBuffor.Sequentions!=null)
            //    {
            //        if (SelectedIndex>-1)
            //        {
            //            mesh.StartSessionForRefMan(SelectedIndex, null,PositionBuffor.Sequentions);
            //        }
            //    }
            //}
            ////tuuuuuuuuuuuuuuuuuuuu zroooooooooooobic tak zeby sie co kilka sekund odmalowywało
            //if (WchichButtonIPressed=="Load")
            //{
            //    if (dic!=null)
            //    {
            //        if (SelectedIndex>-1)
            //        {
            //            mesh.StartSessionForRefMan(SelectedIndex, dic);
            //        }
            //    }
            //}
            //if (Kinect_Stream.IsChecked==true)
            //{
            //    if (mesh.Student!=null)
            //    {
            //        mesh.StartSessionForRefMan(0, null, null, mesh.Student);

            //    }
            //}
        }

        public void TeacherStudentRefManDistance()
        {
            if (mesh.Student!=null && mesh.Teacher!=null)
            {
                //textBlock1.Text="";
                foreach (var jointType in mesh.Teacher)
                {
                    Vector3 odleglosc = distance.measure(mesh.Teacher[jointType.Key], mesh.Student[jointType.Key], 3,true);
                    if (jointType.Key==JointType.Head.ToString()||jointType.Key==JointType.FootLeft.ToString()||
                        jointType.Key==JointType.FootRight.ToString()||
                        jointType.Key==JointType.HandLeft.ToString()|jointType.Key==JointType.HandRight.ToString())
                    {
                        //textBlock1.Text+=jointType.Key.ToString()+"\n";
                        //textBlock1.Text+="Odl_w_X="+odleglosc.X+"\t Odl_w_Y="+odleglosc.Y+"\t Odl_w_Z="+
                                           //odleglosc.Z+"\n\n";
                    }
                }
            }
            else if (mesh.RefMan!=null&&mesh.Student!=null)
            {
                //textBlock1.Text="";
                foreach (var jointType in mesh.Teacher)
                {
                    Vector3 odleglosc = distance.measure(mesh.RefMan[jointType.Key], mesh.Student[jointType.Key], 3,true);
                    if (jointType.Key==JointType.Head.ToString()||jointType.Key==JointType.FootLeft.ToString()||
                        jointType.Key==JointType.FootRight.ToString()||
                        jointType.Key==JointType.HandLeft.ToString()|jointType.Key==JointType.HandRight.ToString())
                    {
                        //textBlock1.Text+=jointType.Key.ToString()+"\n";
                        //textBlock1.Text+="Odl_w_X="+odleglosc.X+"\t Odl_w_Y="+odleglosc.Y+"\t Odl_w_Z="+
                        //                   odleglosc.Z+"\n\n";
                    }
                }
            }
            else if (mesh.RefMan != null && mesh.Teacher != null)
            {
                //textBlock1.Text = "";
                foreach (var jointType in mesh.RefMan)
                {
                    Vector3 odleglosc = distance.measure(mesh.RefMan[jointType.Key], mesh.Teacher[jointType.Key], 3,true);
                    if (jointType.Key == JointType.Head.ToString() || jointType.Key == JointType.FootLeft.ToString() ||
                        jointType.Key == JointType.FootRight.ToString() ||
                        jointType.Key == JointType.HandLeft.ToString() | jointType.Key == JointType.HandRight.ToString())
                    {
                        //textBlock1.Text += jointType.Key.ToString() + "\n";
                        //textBlock1.Text += "Odl_w_X=" + odleglosc.X + "\t Odl_w_Y=" + odleglosc.Y + "\t Odl_w_Z=" +
                                           //odleglosc.Z + "\n\n";
                    }
                }
            }
        }

        private  void Draw_axis_Click(object sender, RoutedEventArgs e)
        {
            if (Draw_axis_Click_counter==0)
            {
                Draw_axis_Click_bool=true;
                mesh.SetAxis();
                Draw_axis_Click_counter=1;
            }
            else if (Draw_axis_Click_counter==1)
            {
                Draw_axis_Click_bool=false;
                Draw_axis_Click_counter=0;
            }
        }

        private void Draw_platform_Click(object sender, RoutedEventArgs e)
        {

            if (Draw_platform_Click_counter==0)
            {
                mesh.SetPlatform(how_many_dots_in_half_ax, mesh.how_far_move_platform_for_Student);
                device.Draw_dots="Can_Draw_Platform";
                Draw_platform_Click_counter=1;
            }
            else if (Draw_platform_Click_counter==1)
            {
                device.Draw_dots="";
                Draw_platform_Click_counter=0;
            }

        }

        //private void Measure_Checked(object sender, RoutedEventArgs e)
        //{
        //    //if(Measure.==true)
        //    //{
        //    //    distance.Setting_XYZCorrectnessDictionary();
        //    //}
        //}
        private void Measure_Clicked(object sender, RoutedEventArgs e)
        {
            distance.Setting_XYZCorrectnessDictionary();
        }

        private void FullResizingButton_Click(object sender, RoutedEventArgs e)
        {
            int NOd;
            if (int.TryParse(Nauczycie_Od.Text, out NOd))
            {
                if (NOd>=0&&NOd<Listbox_combobox.Items.Count)
                {
                    Listbox_combobox.SelectedIndex=NOd;
                    TeacherStartPosition=NOd;
                }
            }
            else
            {
                Listbox_combobox.SelectedIndex=0;
                TeacherStartPosition=0;
            }
            int NDo;
            if (int.TryParse(Nauczycie_Do.Text, out NDo))
            {
                TeacherEndPosition=NDo;
            }
            else
            {
                TeacherEndPosition=Listbox_combobox.Items.Count-1;
            }
            int SOd;
            if (int.TryParse(Student_rec_Od.Text, out SOd))
            {
                if (SOd>=0&&SOd<StudentcomboBox.Items.Count)
                {
                    StudentStartPosition=SOd;
                    StudentcomboBox.SelectedIndex=SOd;
                }
            }
            else
            {
                StudentStartPosition=0;
                StudentcomboBox.SelectedIndex=0;
            }
            int SDo;
            if (int.TryParse(Student_rec_Do.Text, out SDo))
            {
                if (SDo>=0&&SDo<StudentcomboBox.Items.Count)
                {
                    StudentRecEndPosition=SDo;
                }
            }
            else
            {
                StudentRecEndPosition=StudentcomboBox.Items.Count-1;

            }
            //StudentRecComboBoxSelectedIndex=TeacherStartPosition;
            mesh.StartSessionForHybrid(dic);
            StudentRecComboBoxSelectedIndex=StudentcomboBox.SelectedIndex;

        }

        private void Positioning_Click(object sender, RoutedEventArgs e)
        {
            if (Positioning_bool==false)
            {
                Positioning_bool=true;
                return;
            }
            if (Positioning_bool==true)
            {
                Positioning_bool=false;
                return;
            }        
        }
        public  void MoveToNextPositionFunction()
        {
            //textBlock.Text=string.Empty;
            //textBlock.Text="NUMER AKTUALNEJ POZYCJI      ";

            if (Listbox_combobox.SelectedIndex<Listbox_combobox.Items.Count-1)
            {
                Listbox_combobox.SelectedIndex++;
                //SelectedIndex=Listbox_combobox.SelectedIndex;
                //setting_points_of_position();
            }
            if (move_to_next_position==true)
            {
                move_to_next_position=false;
                //textBlock.Text+=SelectedIndex.ToString();
            }
        }
        public  void ShowMovement()
        {
        }

        public  void SetPosition()
        {
            if (Listbox_combobox.SelectedIndex<Listbox_combobox.Items.Count-1)
            {
                Listbox_combobox.SelectedIndex=TeacherStartPosition;
            }
            SetStartPosition=false;
        }
        private void CreateDelayTimer()
        {
            once_for_delay=false;
            DelayTimerForCommands=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            AnglesComparisonTextBlock.Text=AnglesComparisonTextBlock.Text=distance.AnglesComparisonTheSecondV(mesh.ResizedTeacher, mesh.ResizedTeacherCenterOfGravity, mesh.Student, mesh.StudentCenterOfGravity, 0.03f);
                            if (DelayTimerForCommands!=null)
                            {
                                DelayTimerForCommands.Cancel();                               
                            }
                            once_for_delay=true;
                            return;
                        });
                },
                TimeSpan.FromMilliseconds(800));
        }
        public void CreateDelayTimerForShowMovement()
        {
            if(TeacherActualPosition>=TeacherEndPosition)
            {
                ShowTime=false;
                return;
            }
            ShowTime_Once=false;
            DelayTimerForShowMovement=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            TeacherActualPosition++;
                            Listbox_combobox.SelectedIndex=TeacherActualPosition;
                            if (DelayTimerForShowMovement!=null)
                            {
                                DelayTimerForShowMovement.Cancel();
                            }
                            ShowTime_Once=true; 

                            return;
                        });
                },
                TimeSpan.FromMilliseconds(30));
        }
       

        public void Rec_Click(object sender, RoutedEventArgs e)
        {
            RecTimeCounter=0;
            if (PositionBuffor.Sequentions!=null)
            PositionBuffor.Sequentions.Clear();
            int Od;
            if (int.TryParse(Nauczycie_Od.Text, out Od))
            {
                if (Od>=0&&Od<Listbox_combobox.Items.Count)
                {
                    Listbox_combobox.SelectedIndex=Od;
                    TeacherStartPosition=Od;
                }
            }
            int Do;
            if (int.TryParse(Nauczycie_Do.Text, out Do))
            {
                TeacherEndPosition=Do;
            }
            int delay_sec;
            if (int.TryParse(Opoznienie_student_rec.Text, out delay_sec))
            {
                AnglesComparisonTextBlock.Text=delay_sec.ToString();
                CreateDelayTimerForStudentRec(delay_sec*1000);
            }
            else
            {
              RecStart_timer();
            }
        }
        private void Stop_Rec_Click(object sender, RoutedEventArgs e)
        {
            RecTimer.Stop();
            Angles_textBlock3.Text="Stop Rec";
            once_go_thru_for_RecTimer=true;
            StudentcomboBox.ItemsSource=null;
            StudentcomboBox.Items.Clear();
            PositionBuffor.howmanytimes=0;
            if (PositionBuffor.Sequentions!=null)
            {
                foreach (var item in PositionBuffor.Sequentions)
                {
                    StudentcomboBox.Items.Insert(item.Key, "Pozycja nr "+item.Key.ToString());
                }
            }
            if (StudentcomboBox.Items.Count>0)
            {
                StudentcomboBox.SelectedIndex=0;
            }
            if (StudentcomboBox.Items.Count>0)
            {
                StudentcomboBox.SelectedIndex=0;
                Student_rec_Od.Text=0.ToString();
                Student_rec_Do.Text=( StudentcomboBox.Items.Count-1 ).ToString();
                StudentStartPosition=0;
                StudentRecEndPosition=StudentcomboBox.Items.Count-1;
            }
        }
        public void RecStart_timer()
        {
            if(Z_Nauczycielem.IsChecked==true)
            {
                Listbox_combobox.SelectedIndex=TeacherStartPosition;
            }
            int.TryParse(Ile_powtorzen.Text, out Ile_Razy_StudentRec);
            RecTimer=new DispatcherTimer();
            //RecDelay=new DispatcherTimer();
            RecTimer.Tick+=RecTimer_Tick;
            //RecDelay.Tick+=RecDelay_Tick;
            //nie powinno by mniejsze niz 33 milisekundy
            RecTimer.Interval=new TimeSpan(00, 0, 0, 0, 20);
            //RecDelay.Interval=new TimeSpan(00, 0, 0, 1);
            //Countdown=1;
            //RecDelay.Start();
            //Timer.Start();
            Angles_textBlock3.Text="  NAGRYWAM ";
            RecTimer.Start();
        }
        void RecDelay_Tick(object sender, object e)
        {
            if (Countdown==0)
            {
                Angles_textBlock3.Text=Countdown.ToString()+"  NAGRYWA ";
                RecDelay.Stop();
                RecTimer.Start();
                return;
            }
            Angles_textBlock3.Text="Nagrywanie rozpocznie się za "+Countdown.ToString();
            Countdown--;
        }

        void RecTimer_Tick(object sender, object e)
        {
            //int i = 1;
            //if (SomeTextToTimer!=null)
            //    i=SomeTextToTimer;
            //TimeCounter+=1;
            //textBlock.Text=( i-TimeCounter ).ToString();

            if(Z_Nauczycielem.IsChecked==true && once_go_thru_for_RecTimer)
            {
                if(Listbox_combobox.SelectedIndex<TeacherEndPosition)
                {
                    Listbox_combobox.SelectedIndex++;
                }
                else if(Listbox_combobox.SelectedIndex>=TeacherEndPosition)
                {
                    if (Ile_Razy_StudentRec==0)
                    {
                        once_go_thru_for_RecTimer=false;
                    }
                    else if(Ile_Razy_StudentRec>0)
                    {
                        Listbox_combobox.SelectedIndex=TeacherStartPosition;
                        Ile_Razy_StudentRec--;
                        Ile_powtorzen.Text=Ile_Razy_StudentRec.ToString();
                    }
                }
            }
            WasWritingToBuffor=true;
        }

        public void WritingData(MultiSourceFrameReader sender, MultiSourceFrameArrivedEventArgs e)
        {
            if (WasWritingToBuffor)
            {
                MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();
                BodyFrame bodyFrame = null;
                using (bodyFrame=multiSourceFrame.BodyFrameReference.AcquireFrame())
                {
                    Body[] bodies = new Body[SPkinectSensor.BodyFrameSource.BodyCount];
                    bool dataReceived = false;
                    if (bodyFrame!=null)
                    {
                        bodyFrame.GetAndRefreshBodyData(bodies);
                        dataReceived=true;
                    }

                    if (dataReceived)
                    {
                        SPPositionBuffor.UpdateBodies(bodies, PositionBuffor.howmanytimes);
                    }
                }
            }
         WasWritingToBuffor=false;
        }

        private void Take_Student_Positions_Click(object sender, RoutedEventArgs e)
        {
            StudentcomboBox.ItemsSource=null;
            StudentcomboBox.Items.Clear();
            PositionBuffor.howmanytimes=0;
            if (PositionBuffor.Sequentions!=null)
            {
                foreach (var item in PositionBuffor.Sequentions)
                {
                    StudentcomboBox.Items.Insert(item.Key,"Pozycja nr "+item.Key.ToString());
                }
            }
        }
        public  void PeerDopasujSzkieletyClick()
        {
            ButtonAutomationPeer peer=new ButtonAutomationPeer(FullResizingButton);
            IInvokeProvider invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
            invokeProv.Invoke();
        }

        private void StudentcomboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = StudentcomboBox.SelectedIndex;
            //if (index>=TeacherStartPosition&&index<=TeacherEndPosition)
            //{
                StudentRecComboBoxSelectedIndex=index;
            //}
            StudentRecComboBoxSelectedIndex=index;

            mesh.StartSessionForStudentRecord(StudentcomboBox.SelectedIndex, PositionBuffor.Sequentions);
            StudentRecComboBoxActualSelectedIndex=StudentcomboBox.SelectedIndex;
        }

        private void Show_Your_Movement_Click(object sender, RoutedEventArgs e)
        {
            int NOd;
            if (int.TryParse(Nauczycie_Od.Text, out NOd))
            {
                if (NOd>=0&&NOd<Listbox_combobox.Items.Count)
                {
                    Listbox_combobox.SelectedIndex=NOd;
                    TeacherStartPosition=NOd;
                }
            }
            else
            {
                if (Listbox_combobox.Items.Count>0)
                {
                    Listbox_combobox.SelectedIndex=0;
                    TeacherStartPosition=0; 
                }
            }
            int NDo;
            if (int.TryParse(Nauczycie_Do.Text, out NDo))
            {
                TeacherEndPosition=NDo;
            }
            else
            {
                if (Listbox_combobox.Items.Count>0)
                {
                    TeacherEndPosition=Listbox_combobox.Items.Count-1; 
                }
            }
            int SOd;
            if (int.TryParse(Student_rec_Od.Text, out SOd))
            {
                if (SOd>=0&&SOd<StudentcomboBox.Items.Count)
                {
                    StudentStartPosition=SOd;
                    StudentcomboBox.SelectedIndex=SOd;
                }
            }
            else
            {
                if (StudentcomboBox.Items.Count>0)
                {
                    StudentStartPosition=0;
                    StudentcomboBox.SelectedIndex=0; 
                }
            }
            int SDo;
            if (int.TryParse(Student_rec_Do.Text, out SDo))
            {
                if (SDo>=0&&SDo<StudentcomboBox.Items.Count)
                {
                    StudentRecEndPosition=SDo;
                }
            }
            else
            {
                if (StudentcomboBox.Items.Count>0)
                {
                    StudentRecEndPosition=StudentcomboBox.Items.Count-1; 
                }

            }
            if(mesh.Hybrid!=null)
            {
                Device.Draw_Hybrid="draw_Hybrid";
                Device.Draw_BigHybrid="";
            }
           
            CreateDelayTimerForStudentShowMovement();
        }
        public void CreateDelayTimerForStudentShowMovement()
        {
           
            int Interval;
            if (int.TryParse(StudentRecTempo.Text, out Interval))
            {

            }
            else
                Interval=50;
            if (StudentcomboBox.SelectedIndex>=StudentRecEndPosition)
            {
                if (DelayTimerForStudentRecordShowMovement!=null)
                {
                    DelayTimerForStudentRecordShowMovement.Cancel();
                }
                return;
            }
            DelayTimerForStudentRecordShowMovement=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            StudentcomboBox.SelectedIndex++;
                            if (mesh.HybridAll!=null)
                            {
                                if (Z_Dopasowaniem.IsChecked==true&&Z_Nauczycielem.IsChecked==true)
                                {
                                    Device.Draw_BigHybrid="draw_BigHybrid";
                                }
                            }
                            if (mesh.Hybrid!=null)
                            {
                                if (Z_Nauczycielem.IsChecked==true)
                                {
                                    Listbox_combobox.SelectedIndex++;
                                }
                            }
                           
                            if (DelayTimerForStudentRecordShowMovement!=null)
                            {
                                DelayTimerForStudentRecordShowMovement.Cancel();
                            }
                            CreateDelayTimerForStudentShowMovement();
                            return;
                        });
                },
                TimeSpan.FromMilliseconds(Interval));
        }

        private void SetOnPosition_Click(object sender, RoutedEventArgs e)
        {
            
            if (SetOnPosition_Click_counter==0)
            {
                mesh.MoveTeacher=1.5f;
                mesh.MoveStudent=-1.5f;
                setting_points_of_position();
                SetOnPosition_Click_counter=1;
            }
            else if (SetOnPosition_Click_counter==1)
            {
                mesh.MoveTeacher=0;
                mesh.MoveStudent=-0;
                setting_points_of_position();
                SetOnPosition_Click_counter=0;
            }
        }
       
        private void Listbox_combobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Z_Dopasowaniem.IsChecked==true)
            {
                Device.Draw_BigHybrid="draw_BigHybrid";
                Device.Draw_Hybrid="";

                BigHybridActualIndex=Listbox_combobox.SelectedIndex;
            }
            SelectedIndex =Listbox_combobox.SelectedIndex;
            DataContext=this;
            setting_points_of_position();
        }

        private void Rysuj_Nauczyciela_Click(object sender, RoutedEventArgs e)
        {
            if (Rysuj_Nauczyciela_Click_counter==0)
            {
                Device.Draw_Teacher="draw_Teacher";
                if (SelectedIndex<=-1)
                {
                    Listbox_combobox.SelectedIndex=0;
                }
                Listbox_combobox.SelectedIndex=SelectedIndex;
                Rysuj_Nauczyciela_Click_counter=1;
            }
            else if(Rysuj_Nauczyciela_Click_counter==1)
            {
                Device.Draw_Teacher="";
                Rysuj_Nauczyciela_Click_counter=0;
            }

        }

        private void Pokaż_ruchy_Nauczyciela_Click(object sender, RoutedEventArgs e)
        {
            int Od;
            if (int.TryParse(Nauczycie_Od.Text, out Od))
            {
                if (Od>=0&&Od<Listbox_combobox.Items.Count)
                {
                    Listbox_combobox.SelectedIndex=Od;
                    TeacherStartPosition=Od;
                }
            }
            int Do;
            if (int.TryParse(Nauczycie_Do.Text, out Do))
            {
                TeacherEndPosition=Do;
            }
            int ile_powtorzen=0;
            int.TryParse(Ile_powtorzen.Text, out ile_powtorzen);   
            int delay_sec;
            if (int.TryParse(Pokaz_ruchy_nauczyciela_delay.Text, out delay_sec))
            {
                AnglesComparisonTextBlock.Text=delay_sec.ToString();
                CreateDelayTimerForTeacher(delay_sec*1000,ile_powtorzen);
            }
            else 
            {
                CreateTimerForTeacher(ile_powtorzen);
            }
        }
        public void CreateTimerForTeacher(int ile_powtorzen)
        {
            int Interval;
            if(int.TryParse(Tempo.Text, out Interval))
            {

            }
            else
               Interval=50;

            if (Listbox_combobox.SelectedIndex>=TeacherEndPosition)
            {
                if (ile_powtorzen==0)
                    return;
                else
                {
                    Listbox_combobox.SelectedIndex=TeacherStartPosition;
                    if (DelayTimerForTeacher!=null)
                    {
                        DelayTimerForTeacher.Cancel();
                    }
                    ile_powtorzen--;
                    CreateTimerForTeacher(ile_powtorzen);

                }
            }
            DelayTimerForTeacher=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            Listbox_combobox.SelectedIndex++;
                            if (DelayTimerForTeacher!=null)
                            {
                                DelayTimerForTeacher.Cancel();
                            }
                            //ShowTime_Once=true;
                            CreateTimerForTeacher(ile_powtorzen);
                            return;
                        });
                },
                TimeSpan.FromMilliseconds(Interval));
        }
        public void CreateDelayTimerForTeacher( int delay_sec,int ile_powtorzen)
        {
            if(delay_sec==0)
            {
                if (DelayTimerForTeacherDelay!=null)
                {
                    DelayTimerForTeacherDelay.Cancel();
                }
                CreateTimerForTeacher(ile_powtorzen);
                return;
            }
            DelayTimerForTeacherDelay=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            delay_sec = delay_sec-1000;
                            AnglesComparisonTextBlock.Text=delay_sec.ToString(); ;
                            if (DelayTimerForTeacherDelay!=null)
                            {
                                DelayTimerForTeacherDelay.Cancel();
                            }
                            CreateDelayTimerForTeacher(delay_sec, ile_powtorzen);
                        });
                },
                TimeSpan.FromMilliseconds(1000));
        }


        private void Nauczycie_Od_TextChanged(object sender, TextChangedEventArgs e)
        {
            int Od;
            if (int.TryParse(Nauczycie_Od.Text,out Od))
            {
                if(Od>=0 && Od<Listbox_combobox.Items.Count)
                {
                    Listbox_combobox.SelectedIndex=Od;
                    TeacherStartPosition=Od;
                }
            }
        }

        private void Nauczycie_Do_TextChanged(object sender, TextChangedEventArgs e)
        {
            int Do;
            if (int.TryParse(Nauczycie_Od.Text, out Do))
            {
                if (Do>=0&&Do<Listbox_combobox.Items.Count)
                {
                    TeacherEndPosition=Do;
                }
            }
        }

        private void Student_rec_Od_TextChanged(object sender, TextChangedEventArgs e)
        {
            int Od;
            if (int.TryParse(Student_rec_Od.Text, out Od))
            {
                if (Od>=0&&Od<StudentcomboBox.Items.Count)
                {
                    StudentcomboBox.SelectedIndex=Od;
                }
            }
        }

        private void Student_rec_Do_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Zatrzymaj_animacje_Click(object sender, RoutedEventArgs e)
        {
            if (DelayTimerForStudentRecordShowMovement!=null)
            {
                DelayTimerForStudentRecordShowMovement.Cancel();
            }
        }

        private void Zatrzymaj_animacje_nauczyciel_Click(object sender, RoutedEventArgs e)
        {
            if (DelayTimerForTeacher!=null)
            {
                DelayTimerForTeacher.Cancel();
            }
        }

        public void CreateDelayTimerForStudentRec(int delay_sec)
        {
            if (delay_sec==0)
            {
                if (DelayTimerForStudentRecDelay!=null)
                {
                    DelayTimerForStudentRecDelay.Cancel();
                }
                RecStart_timer();
                return;
            }
            DelayTimerForStudentRecDelay=ThreadPoolTimer.CreateTimer(
                async (timer) =>
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            delay_sec=delay_sec-1000;
                            AnglesComparisonTextBlock.Text=(delay_sec/1000).ToString(); 
                            if (DelayTimerForStudentRecDelay!=null)
                            {
                                DelayTimerForStudentRecDelay.Cancel();
                            }
                            CreateDelayTimerForStudentRec(delay_sec);
                        });
                },
                TimeSpan.FromMilliseconds(1000));
        }

        private void Rysuj_Hybryde_Click(object sender, RoutedEventArgs e)
        {
            if(Rysuj_Hybryde_Click_counter==0)
            {
                Device.Draw_Hybrid="draw_Hybrid";
            }
            else if(Rysuj_Hybryde_Click_counter==1)
            {
                Device.Draw_Hybrid="";
            }
        }

        private void Lustro_Click(object sender, RoutedEventArgs e)
        {
            if(Device.mirror==-1)
            {
                Device.mirror=1;
            }
            else if(Device.mirror==1)
            {
                Device.mirror=-1;
            }
        }


    }

    public class CustomKeyValuePair<TKey, TValue>

    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }
    }

 
}
