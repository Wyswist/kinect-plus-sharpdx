﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsPreview.Kinect;
using SharpDX;

namespace Kinect2Sample
{
    class Resizing
    {
        public float PlatformMove(Dictionary<string,Vector3> Dictionary )
        {
            Vector3 footLeft, footRight;
            footLeft = footRight = Vector3.Zero;
            float HowFarMove = 0;
            foreach (var Ikey in Dictionary)
            {
                if (Ikey.Key == JointType.FootLeft.ToString())
                {
                     footLeft = Ikey.Value;
                }
                if (Ikey.Key==JointType.FootRight.ToString())
                {
                    footRight=Ikey.Value;
                }
                if (footRight != Vector3.Zero && footLeft != Vector3.Zero)
                {
                    if (footRight.Y <= footLeft.Y)
                    {
                        HowFarMove = footRight.Y;
                    }
                    if (footLeft.Y<=footRight.Y)
                    {
                        HowFarMove=footLeft.Y;
                    }
                }
            }
            return HowFarMove;
        }

        public float TeacherHeightResizing(Dictionary<string,Vector3>Teacher, Dictionary<string, Vector3> Student)
        {
            float teacher_lowest_Point = PlatformMove(Teacher);
            float student_lowest_Point = PlatformMove(Student);
            float diffrence = teacher_lowest_Point - student_lowest_Point;
            return diffrence;
        }
    }
}
