﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using System.Diagnostics;

namespace Kinect2Sample
{
    class Timers
    {

        public double TimeCounter { get; set; }
        public DispatcherTimer Timer;
        static public bool I_can_not_go_further { get; set; }
        static public bool Timer_start { get; set; }
        public static bool Loop_escape { get; set; }
        public DispatcherTimer Delay;

        public Timers()
        {
            Timer_start=false;
        }

        //------Zwykły timer do Measurement-------------------------------------------------------------
        public void Start_timer()
        {
            I_can_not_go_further=true;
            Timer_start=true;
            TimeCounter =0;
            Timer =new DispatcherTimer();
            Timer.Tick+=timer_Tick;
            Timer.Interval=new TimeSpan(00, 0, 0, 1);
            Timer.Start();
        }
        
        void timer_Tick(object sender, object e )
        {
            TimeCounter+=1;
            if (TimeCounter>=2)
            {


                Timer.Stop();
                I_can_not_go_further=false;
                //Timer_start=false;
            }
        }
        //------Zwykły timer do Measurement-------------------------------------------------------------

        //-----------------D-E-l-A-Y--------------------------------------------------------------------
        /// <summary>
        /// Delay
        /// </summary>
        /// <param name="i">delay in seconds</param>
        /// <param name="j">dealy in milliseconds</param>
        public void Start_Delay(int i ,int j)
        {
            Delay=new DispatcherTimer();
            Delay.Tick+=Delay_Tick;
            Delay.Interval=new TimeSpan(00, 0, 0, i, j);
            Delay.Start();
        }

        void Delay_Tick(object sender, object e)
        {
            Loop_escape=true;
            Delay.Stop();
        }
        //-----------------D-E-l-A-Y--------------------------------------------------------------------


    }
}
