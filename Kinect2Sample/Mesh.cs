﻿// Mesh.cs

using System;
using System.Collections.Generic;
using Windows.Devices.Geolocation;
using WindowsPreview.Kinect;
using SharpDX;
using System.Diagnostics;
using Windows.UI;
using Windows.UI.Xaml.Shapes;
using System.Linq;

namespace Kinect2Sample
{
    public class Mesh
    {
        public string Name { get; set; }
        public Vector3[] Vertices { get; private set; }
        public Face[] Faces { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public Dictionary<string, Vector3> Student { get; set; }
        public Dictionary<string, Vector3> Teacher { get; set; }
        public Dictionary<int, Dictionary<string, Vector3>>BigTeacher { get; set;}
        public Dictionary<int, Dictionary<string, Vector3>> BigStudent { get; set; }
        public Dictionary<string, Vector3> ResizedTeacher { get; set; }
        public Dictionary<string, Vector3> TMPFullResizedTeacher { get; set; }
        public Dictionary<string, Vector3> FullResizedTeacher { get; set; }
        public Dictionary<string, Vector3> StudentRecord { get; set; }
        public Dictionary<string, Vector3> RefMan { get; set; }
        public Dictionary<int,Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double>>> TeacherAngles { get; set; }
        public Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double>>> TeacherAnglesAll { get; set; }

        public Dictionary<int, Dictionary<Tuple<JointType, JointType>,Vector3>> StudentNewBones { get; set; }
        public Dictionary<int, Dictionary<Tuple<JointType, JointType>,Vector3>> StudentNewBonesAll { get; set; }


        public Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>>> StudentBoneAnglesAndVectors { get; set; }
        public Dictionary<int,Dictionary<string, Vector3>> Hybrid { get; set; }
        public Dictionary<int, Dictionary<string, Vector3>> HybridAll { get; set; }

        public Dictionary<string, Tuple<int,int, Vector3>> AxisDictionary { get; set;}
        public Dictionary<Tuple<JointType, JointType>, Line> BonelLines = new BodyInfo(Colors.Aqua, 2).BoneLines;
        public Dictionary<int, Vector3> PlatformDictionary { get; set; }
        public float how_far_move_platform_for_Teacher { get; set; }
        public float how_far_move_platform_for_Student { get; set; }
        public float how_much_resized_teacher_height { get; set; }
        public float how_much_resized_teacher_full { get; set; }
        public Vector3 ResizedTeacherCenterOfGravity { get; set; }
        public Vector3 StudentCenterOfGravity { get; set; }
        public float MoveTeacher = 0;
        public float MoveStudent = 0;
        public float MoveFullResizedTeacher = 0;
        public float MoveColorizedStudent = 0;
        public Dictionary<int,Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>> TeacherBoneLength { get; set; }
        public Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>> TeacherBoneLengthAll { get; set; }

        public Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>> StudentBoneLength { get; set; }
        public List<int> DicKeysNewList;
        public List<int> PositionBufforSequentionsListNewList;

        public Mesh(string name, int verticesCount, int facesCount)
        {
            how_far_move_platform_for_Student=how_far_move_platform_for_Teacher =how_much_resized_teacher_height= 0;
            Vertices = new Vector3[verticesCount];
            Faces = new Face[facesCount];
            Name = name;
        }
        public void StartSessionForStudent()
        {
            if (Student==null)
            {
                Student = new Dictionary<string, Vector3>();
            }
            else
            {
                Student.Clear();
            }
        }
        public void StartSessionForTeacher(int index, Dictionary<int, Dictionary<string, Tuple<float, float, float>>> mineDic = null, Dictionary<int, Dictionary<JointType, CameraSpacePoint>> K2S_Dic = null)
        {
            if (K2S_Dic!=null&&mineDic!=null)
                return;
            if (K2S_Dic!=null)
            {
                if (index>-1)
                {
                    Teacher=new Dictionary<string, Vector3>();
                    foreach (var iKey in K2S_Dic[index])
                    {
                        Teacher.Add(iKey.Key.ToString(), new Vector3(iKey.Value.X+MoveTeacher, iKey.Value.Y-how_much_resized_teacher_height, iKey.Value.Z));
                    }
                }
            }
            if (mineDic!=null)
            {
                if (index>-1)
                {
                    Teacher=new Dictionary<string, Vector3>();
                    Teacher.Clear();
                    foreach (var iKey in mineDic[index])
                    {
                        Teacher.Add(iKey.Key.ToString(), new Vector3(iKey.Value.Item1-MoveTeacher, iKey.Value.Item2-how_much_resized_teacher_height, iKey.Value.Item3));
                        //System.Diagnostics.Debug.WriteLine(iKey.Key.ToString()+"\n");
                    }
                }
            }
        }

        public void StartSessionForRefMan(int index, Dictionary<int, Dictionary<string, Tuple<float, float, float>>> mineDic = null, Dictionary<int, Dictionary<JointType, CameraSpacePoint>> K2S_Dic = null,Dictionary<string,Vector3>Student_Dic=null)
        {
            if (K2S_Dic!=null&&mineDic!=null&&Student_Dic!=null)
                return;
            if (K2S_Dic!=null)
            {
                if (index>-1)
                {
                    RefMan=new Dictionary<string, Vector3>();
                    foreach (var iKey in K2S_Dic[index])
                    {
                        RefMan.Add(iKey.Key.ToString(), new Vector3(iKey.Value.X, iKey.Value.Y, iKey.Value.Z));
                    }
                }
            }
            if (mineDic!=null)
            {
                if (index>-1)
                {
                    RefMan=new Dictionary<string, Vector3>();
                    RefMan.Clear();
                    foreach (var iKey in mineDic[index])
                    {
                        RefMan.Add(iKey.Key.ToString(), new Vector3(iKey.Value.Item1, iKey.Value.Item2, iKey.Value.Item3));
                    }
                }
            }
            if (Student_Dic!=null)
            {
                if (RefMan==null)
                    RefMan=new Dictionary<string, Vector3>();
                else
                    RefMan.Clear();

                Debug.WriteLine("foreach");

                foreach (var iKey in Student_Dic)
                {
                    RefMan.Add(iKey.Key.ToString(), new Vector3(iKey.Value.X, iKey.Value.Y, iKey.Value.Z));
                }
            }
        }
        public void StartSessionForStudentRecord(int index, Dictionary<int, Dictionary<JointType, CameraSpacePoint>> K2S_Dic)
        {
            if (K2S_Dic==null)
                return;
            if (K2S_Dic!=null)
            {
                if (index>-1)
                {
                    StudentRecord=new Dictionary<string, Vector3>();
                    foreach (var iKey in K2S_Dic[index])
                    {
                        StudentRecord.Add(iKey.Key.ToString(), new Vector3(iKey.Value.X, iKey.Value.Y, iKey.Value.Z));
                    }
                }
            }
            Device.Draw_Student_record="draw_Student_record";
        }

        public void SetAxis()
        {
            AxisDictionary= new Dictionary<string, Tuple<int, int, Vector3>>();
            var next = 0;
            for (int cur = 0; cur < 10; cur++)
            {
                if (cur == 9)
                    next = 9;
                else
                    next = cur + 1;

                AxisDictionary.Add("X"+cur, new Tuple<int,int, Vector3>(cur,next, new Vector3(cur/2, 0, 0)));
                AxisDictionary.Add("Y"+cur, new Tuple<int,int, Vector3>(cur,next, new Vector3(0, cur/2, 0)));
                AxisDictionary.Add("Z"+cur, new Tuple<int,int, Vector3>(cur,next, new Vector3(0, 0, cur/2)));
                if (cur == next)
                {
                    var arrow_pos = cur-2;

                    cur+= 1;
                    AxisDictionary.Add("X"+cur, new Tuple<int, int, Vector3>(cur, next, new Vector3(x: arrow_pos/2, y: 0.5f, z: 0f)));
                    AxisDictionary.Add("Y"+cur, new Tuple<int, int, Vector3>(cur, next, new Vector3(0, y: arrow_pos/2, z: 0.5f)));
                    AxisDictionary.Add("Z"+cur, new Tuple<int, int, Vector3>(cur, next, new Vector3(0, 0.5f, z: arrow_pos/2)));

                    cur+=1;
                    AxisDictionary.Add("X"+cur, new Tuple<int, int, Vector3>(cur, next, new Vector3(x: arrow_pos/2, y: -0.5f, z: 0f)));
                    AxisDictionary.Add("Y"+cur, new Tuple<int, int, Vector3>(cur, next, new Vector3(0, y: arrow_pos/2, z: -0.5f)));
                    AxisDictionary.Add("Z"+cur, new Tuple<int, int, Vector3>(cur, next, new Vector3(0, -0.5f, z: arrow_pos/2)));
                    return;
                }
            }
        }
        //how many dosts dotyczy kropek na jednej osi czyli w sumie narysuje ich how_many_dots^4
        public void  SetPlatform(int how_many_dots, float how_far_move_platform)
        {
            int key_for_dictionary = 0;
            if (PlatformDictionary != null)
            {
                PlatformDictionary.Clear();
            }
            PlatformDictionary= new Dictionary<int, Vector3>();
            for (int i = -how_many_dots; i <= how_many_dots; i++)
            {
                for (int j = -how_many_dots; j <=how_many_dots; j++)
                {
                    PlatformDictionary.Add(key_for_dictionary, new Vector3((float)i/3,(float)how_far_move_platform, (float)j/3));
                    key_for_dictionary++;
                }
            }
        }

        public void ResizedTeacherHeight()
        {
            if (Teacher!=null)
            {
                ResizedTeacher=new Dictionary<string, Vector3>();
                foreach (var Ikey in Teacher)
                {
                    //tu trzeba zrobic update dla teachera i nie tworzyc nowego resizedteacher 
                    //cos nie pykło z z update
                    //ResizedTeacher[Ikey.Key]=new Vector3(Teacher[Ikey.Key].X, Teacher[Ikey.Key].Y-how_much_resized_teacher_height, Teacher[Ikey.Key].Z);
                    //Teacher[Ikey.Key]=new Vector3(123f,13f,313f);

                    ResizedTeacher[Ikey.Key]=new Vector3(Teacher[Ikey.Key].X, Teacher[Ikey.Key].Y-how_much_resized_teacher_height, Teacher[Ikey.Key].Z);
                } 
            }
        }
        public void FullResizedTeacherHeight()
        {
                Dictionary<string, Vector3> Tmp_Dictionary_For_Height_Resizing_FullResizedTeacher = new Dictionary<string, Vector3>();
                foreach (var Item in FullResizedTeacher)
                {
                    Vector3 tmp_new_position = new Vector3(FullResizedTeacher[Item.Key].X, FullResizedTeacher[Item.Key].Y-how_much_resized_teacher_full, FullResizedTeacher[Item.Key].Z);
                    Tmp_Dictionary_For_Height_Resizing_FullResizedTeacher.Add(Item.Key, tmp_new_position);
                }

                foreach (var Item in Tmp_Dictionary_For_Height_Resizing_FullResizedTeacher)
                {
                    FullResizedTeacher[Item.Key]=Item.Value;
                }
        }
     
        public void StartSessionForHybrid(Dictionary<int, Dictionary<string, Tuple<float, float, float>>> dic)
        {
            if(dic==null)
            {
                return;
            }
            BonesMeasurment(dic);
            AnglesDeterminationForHybrid(dic);

            if (Hybrid==null)
                Hybrid=new Dictionary<int, Dictionary<string, Vector3>>();
            else
                Hybrid.Clear();

            if (HybridAll==null)
                HybridAll=new Dictionary<int, Dictionary<string, Vector3>>();
            else
                HybridAll.Clear();

            var PositionBufforSequentionsList = PositionBuffor.Sequentions.Keys;
            PositionBufforSequentionsListNewList = PositionBufforSequentionsList.Where(zmienna => zmienna>=SecondPage.StudentStartPosition&&zmienna<=SecondPage.StudentRecEndPosition).ToList<int>();

            var DicKeysList = dic.Keys;
            DicKeysNewList = DicKeysList.Where(zmienna => zmienna>=SecondPage.TeacherStartPosition&&zmienna<=SecondPage.TeacherEndPosition).ToList<int>();
            var TeacherPartOfHybrid = DicKeysNewList.First();
            foreach (var index in PositionBufforSequentionsListNewList)
            {
                var tmp_Hybrid =new Dictionary<string, Vector3>();
                var tmp_StudentNewBones = StudentNewBones[index];
             

                var StudentRecord = PositionBuffor.Sequentions[index];
                tmp_Hybrid.Add(JointType.Head.ToString(), new Vector3(StudentRecord[JointType.Head].X, StudentRecord[JointType.Head].Y, StudentRecord[JointType.Head].Z));
                foreach (var boneline in BonelLines.Keys)
                {

                    if (boneline.Item1==JointType.ShoulderRight&&boneline.Item2==JointType.ElbowRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));

                    }
                    else if (boneline.Item1==JointType.ElbowRight&&boneline.Item2==JointType.WristRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristRight&&boneline.Item2==JointType.HandRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HandRight&&boneline.Item2==JointType.HandTipRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristRight&&boneline.Item2==JointType.ThumbRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.ShoulderLeft&&boneline.Item2==JointType.ElbowLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.ElbowLeft&&boneline.Item2==JointType.WristLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristLeft&&boneline.Item2==JointType.HandLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HandLeft&&boneline.Item2==JointType.HandTipLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristLeft&&boneline.Item2==JointType.ThumbLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HipRight&&boneline.Item2==JointType.KneeRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.KneeRight&&boneline.Item2==JointType.AnkleRight)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HipLeft&&boneline.Item2==JointType.KneeLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.KneeLeft&&boneline.Item2==JointType.AnkleLeft)
                    {
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else
                    {
                        var boneline_start = new Vector3(StudentRecord[boneline.Item1].X, StudentRecord[boneline.Item1].Y, StudentRecord[boneline.Item1].Z);
                        var boneline_end = new Vector3(StudentRecord[boneline.Item2].X, StudentRecord[boneline.Item2].Y, StudentRecord[boneline.Item2].Z);

                        var StudentRec_sub_vector = Vector3.Subtract(boneline_end, boneline_start);
                        tmp_Hybrid.Add(boneline.Item2.ToString(), Vector3.Add(tmp_Hybrid[boneline.Item1.ToString()], StudentRec_sub_vector));
                    }
                }

                var StudentRecordFootLeft = new Vector3(StudentRecord[JointType.FootLeft].X, StudentRecord[JointType.FootLeft].Y, StudentRecord[JointType.FootLeft].Z);
                var StudentRecordFootRight = new Vector3(StudentRecord[JointType.FootRight].X, StudentRecord[JointType.FootRight].Y, StudentRecord[JointType.FootRight].Z);

                var LeftFootDistanceToMove = Vector3.Subtract(StudentRecordFootLeft, tmp_Hybrid[JointType.FootLeft.ToString()]);
                var RightFootDistanceToMove = Vector3.Subtract(StudentRecordFootRight, tmp_Hybrid[JointType.FootRight.ToString()]);

                tmp_Hybrid[JointType.FootLeft.ToString()]=Vector3.Add(tmp_Hybrid[JointType.FootLeft.ToString()], LeftFootDistanceToMove);
                tmp_Hybrid[JointType.AnkleLeft.ToString()]=Vector3.Add(tmp_Hybrid[JointType.AnkleLeft.ToString()], LeftFootDistanceToMove);
                tmp_Hybrid[JointType.KneeLeft.ToString()]=Vector3.Add(tmp_Hybrid[JointType.KneeLeft.ToString()], LeftFootDistanceToMove);
                tmp_Hybrid[JointType.HipLeft.ToString()]=Vector3.Add(tmp_Hybrid[JointType.HipLeft.ToString()], LeftFootDistanceToMove);

                tmp_Hybrid[JointType.FootRight.ToString()]=Vector3.Add(tmp_Hybrid[JointType.FootRight.ToString()], RightFootDistanceToMove);
                tmp_Hybrid[JointType.AnkleRight.ToString()]=Vector3.Add(tmp_Hybrid[JointType.AnkleRight.ToString()], RightFootDistanceToMove);
                tmp_Hybrid[JointType.KneeRight.ToString()]=Vector3.Add(tmp_Hybrid[JointType.KneeRight.ToString()], RightFootDistanceToMove);
                tmp_Hybrid[JointType.HipRight.ToString()]=Vector3.Add(tmp_Hybrid[JointType.HipRight.ToString()], RightFootDistanceToMove);

                Hybrid.Add(index, tmp_Hybrid);
                if(TeacherPartOfHybrid<DicKeysNewList.Last())
                     TeacherPartOfHybrid++;
            }
            //------duza hybryda\/
            foreach (var index in StudentNewBonesAll.Keys)
            {
                var tmp_HybridAll = new Dictionary<string, Vector3>();
                var tmp_StudentNewBones = StudentNewBonesAll[index];

                var StudentRecord = PositionBuffor.Sequentions[SecondPage.StudentStartPosition];
                tmp_HybridAll.Add(JointType.Head.ToString(), new Vector3(StudentRecord[JointType.Head].X, StudentRecord[JointType.Head].Y, StudentRecord[JointType.Head].Z));
                foreach (var boneline in BonelLines.Keys)
                {
                    if (boneline.Item1==JointType.ShoulderRight&&boneline.Item2==JointType.ElbowRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.ElbowRight&&boneline.Item2==JointType.WristRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristRight&&boneline.Item2==JointType.HandRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HandRight&&boneline.Item2==JointType.HandTipRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristRight&&boneline.Item2==JointType.ThumbRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.ShoulderLeft&&boneline.Item2==JointType.ElbowLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.ElbowLeft&&boneline.Item2==JointType.WristLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristLeft&&boneline.Item2==JointType.HandLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HandLeft&&boneline.Item2==JointType.HandTipLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.WristLeft&&boneline.Item2==JointType.ThumbLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HipRight&&boneline.Item2==JointType.KneeRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.KneeRight&&boneline.Item2==JointType.AnkleRight)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.HipLeft&&boneline.Item2==JointType.KneeLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else if (boneline.Item1==JointType.KneeLeft&&boneline.Item2==JointType.AnkleLeft)
                    {
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], -tmp_StudentNewBones[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)]));
                    }
                    else
                    {
                        var boneline_start = new Vector3(StudentRecord[boneline.Item1].X, StudentRecord[boneline.Item1].Y, StudentRecord[boneline.Item1].Z);
                        var boneline_end = new Vector3(StudentRecord[boneline.Item2].X, StudentRecord[boneline.Item2].Y, StudentRecord[boneline.Item2].Z);

                        var StudentRec_sub_vector = Vector3.Subtract(boneline_end, boneline_start);
                        tmp_HybridAll.Add(boneline.Item2.ToString(), Vector3.Add(tmp_HybridAll[boneline.Item1.ToString()], StudentRec_sub_vector));
                    }
                }

                var StudentRecordFootLeft = new Vector3(StudentRecord[JointType.FootLeft].X, StudentRecord[JointType.FootLeft].Y, StudentRecord[JointType.FootLeft].Z);
                var StudentRecordFootRight = new Vector3(StudentRecord[JointType.FootRight].X, StudentRecord[JointType.FootRight].Y, StudentRecord[JointType.FootRight].Z);

                var LeftFootDistanceToMove = Vector3.Subtract(StudentRecordFootLeft, tmp_HybridAll[JointType.FootLeft.ToString()]);
                var RightFootDistanceToMove = Vector3.Subtract(StudentRecordFootRight, tmp_HybridAll[JointType.FootRight.ToString()]);

                tmp_HybridAll[JointType.FootLeft.ToString()]=Vector3.Add(tmp_HybridAll[JointType.FootLeft.ToString()], LeftFootDistanceToMove);
                tmp_HybridAll[JointType.AnkleLeft.ToString()]=Vector3.Add(tmp_HybridAll[JointType.AnkleLeft.ToString()], LeftFootDistanceToMove);
                tmp_HybridAll[JointType.KneeLeft.ToString()]=Vector3.Add(tmp_HybridAll[JointType.KneeLeft.ToString()], LeftFootDistanceToMove);
                tmp_HybridAll[JointType.HipLeft.ToString()]=Vector3.Add(tmp_HybridAll[JointType.HipLeft.ToString()], LeftFootDistanceToMove);

                tmp_HybridAll[JointType.FootRight.ToString()]=Vector3.Add(tmp_HybridAll[JointType.FootRight.ToString()], RightFootDistanceToMove);
                tmp_HybridAll[JointType.AnkleRight.ToString()]=Vector3.Add(tmp_HybridAll[JointType.AnkleRight.ToString()], RightFootDistanceToMove);
                tmp_HybridAll[JointType.KneeRight.ToString()]=Vector3.Add(tmp_HybridAll[JointType.KneeRight.ToString()], RightFootDistanceToMove);
                tmp_HybridAll[JointType.HipRight.ToString()]=Vector3.Add(tmp_HybridAll[JointType.HipRight.ToString()], RightFootDistanceToMove);

                HybridAll.Add(index, tmp_HybridAll);
                if (TeacherPartOfHybrid<DicKeysNewList.Last())
                    TeacherPartOfHybrid++;
            }
            Device.Draw_Hybrid="draw_Hybrid";
        }
        public void BonesMeasurment(Dictionary<int, Dictionary<string, Tuple<float, float, float>>> dic)
        {
            if (StudentBoneLength==null)
                StudentBoneLength=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>>();
                StudentBoneLength.Clear();

            if (TeacherBoneLength==null)
                TeacherBoneLength=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>>();
            else
                TeacherBoneLength.Clear();

            if (TeacherBoneLengthAll==null)
                TeacherBoneLengthAll=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>>();
            else
                TeacherBoneLengthAll.Clear();

            var DicKeysList = dic.Keys;
            var DicKeysNewList = DicKeysList.Where(zmienna => zmienna>=SecondPage.TeacherStartPosition&&zmienna<=SecondPage.TeacherEndPosition).ToList<int>();
            foreach (var index in DicKeysNewList)
            {
                Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> TMP_TeacherBoneLength = new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();
                var tmp_dic = dic[index];
                foreach (var boneline in BonelLines.Keys)
                {
                    // tu stworzyc wektor
                    var boneline_start = new Vector3(tmp_dic[boneline.Item1.ToString()].Item1, tmp_dic[boneline.Item1.ToString()].Item2, tmp_dic[boneline.Item1.ToString()].Item3);
                    var boneline_end   = new Vector3(tmp_dic[boneline.Item2.ToString()].Item1, tmp_dic[boneline.Item2.ToString()].Item2, tmp_dic[boneline.Item2.ToString()].Item3);

                    Vector3 XYZ_lenght = Vector3.Subtract(boneline_start, boneline_end);
                    float lenght = (float)Math.Sqrt(Math.Pow(XYZ_lenght.X, 2)+Math.Pow(XYZ_lenght.Y, 2)+Math.Pow(XYZ_lenght.Z, 2));
                    TMP_TeacherBoneLength.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<Vector3, float>(XYZ_lenght, lenght));
                }
                TeacherBoneLength.Add(index,TMP_TeacherBoneLength);
            }
            //Debug.WriteLine("ehh");

            foreach (var index in dic.Keys)
            {
                Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> TMP_TeacherBoneLength = new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();
                var tmp_dic = dic[index];
                foreach (var boneline in BonelLines.Keys)
                {
                    var boneline_start = new Vector3(tmp_dic[boneline.Item1.ToString()].Item1, tmp_dic[boneline.Item1.ToString()].Item2, tmp_dic[boneline.Item1.ToString()].Item3);
                    var boneline_end = new Vector3(tmp_dic[boneline.Item2.ToString()].Item1, tmp_dic[boneline.Item2.ToString()].Item2, tmp_dic[boneline.Item2.ToString()].Item3);

                    Vector3 XYZ_lenght = Vector3.Subtract(boneline_start, boneline_end);
                    float lenght = (float)Math.Sqrt(Math.Pow(XYZ_lenght.X, 2)+Math.Pow(XYZ_lenght.Y, 2)+Math.Pow(XYZ_lenght.Z, 2));
                    TMP_TeacherBoneLength.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<Vector3, float>(XYZ_lenght, lenght));
                }
                TeacherBoneLengthAll.Add(index, TMP_TeacherBoneLength);
            }

            var PositionBufforSequentionsList= PositionBuffor.Sequentions.Keys;
            var PositionBufforSequentionsListNewList = PositionBufforSequentionsList.Where(zmienna => zmienna>=SecondPage.StudentStartPosition && zmienna<=SecondPage.StudentRecEndPosition).ToList<int>();
            foreach (var index in PositionBufforSequentionsListNewList)
            {
                Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> TMP_StudentBoneLength = new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();
                var tmp_positionbuffor = PositionBuffor.Sequentions[index];
                foreach (var boneline in BonelLines.Keys)
                {
                    var boneline_start = new Vector3(tmp_positionbuffor[boneline.Item1].X, tmp_positionbuffor[boneline.Item1].Y, tmp_positionbuffor[boneline.Item1].Z);
                    var boneline_end = new Vector3(tmp_positionbuffor[boneline.Item2].X, tmp_positionbuffor[boneline.Item2].Y, tmp_positionbuffor[boneline.Item2].Z);
                    Vector3 XYZ_lenght = Vector3.Subtract(boneline_start, boneline_end);
                    float lenght = (float)Math.Sqrt(Math.Pow(XYZ_lenght.X, 2)+Math.Pow(XYZ_lenght.Y, 2)+Math.Pow(XYZ_lenght.Z, 2));
                    TMP_StudentBoneLength.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<Vector3, float>(XYZ_lenght, lenght));
                }
                StudentBoneLength.Add(index,TMP_StudentBoneLength);
            }
        }

        public void AnglesDeterminationForHybrid(Dictionary<int, Dictionary<string, Tuple<float, float, float>>> dic)
        {
            if (TeacherBoneLength!=null&&StudentBoneLength!=null)
            {
                if (TeacherAngles==null)
                    TeacherAngles=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double>>>();
                else
                    TeacherAngles.Clear();

                if (TeacherAnglesAll==null)
                    TeacherAnglesAll=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double>>>();
                else
                    TeacherAnglesAll.Clear();

                if (StudentNewBones==null)
                    StudentNewBones=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Vector3>>();
                else
                    StudentNewBones.Clear();

                if (StudentNewBonesAll==null)
                    StudentNewBonesAll=new Dictionary<int, Dictionary<Tuple<JointType, JointType>, Vector3>>();
                else
                    StudentNewBonesAll.Clear();
                //Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>> TMP_TeacherBoneLength = new Dictionary<Tuple<JointType, JointType>, Tuple<Vector3, float>>();

                foreach (var index in TeacherBoneLength.Keys)
                {
                    var tmp_TeacherBoneLength = TeacherBoneLength[index];
                    var tmp_dic = new Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double>>();
                    foreach (var boneline in BonelLines.Keys)
                    {
                        double beta = Math.Acos(( tmp_TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Y )/( tmp_TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 ));
                        //przekatna na płaszzyznie XZ
                        double Diagonal_XZ_Plane = tmp_TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
                        double alpha = Math.Acos(( tmp_TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.X )/( Diagonal_XZ_Plane ));
                        double gamma = Math.Acos(( tmp_TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Z )/( Diagonal_XZ_Plane ));
                        tmp_dic.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2),new Tuple<double, double, double>(alpha,beta,gamma));
                    }

                    TeacherAngles.Add(index, tmp_dic);
                }



                foreach (var index in TeacherBoneLengthAll.Keys)
                {
                    var tmp_TeacherBoneLengthAll = TeacherBoneLengthAll[index];
                    var tmp_dicAll = new Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double>>();
                    foreach (var boneline in BonelLines.Keys)
                    {
                        double beta = Math.Acos(( tmp_TeacherBoneLengthAll[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Y )/( tmp_TeacherBoneLengthAll[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 ));
                        //przekatna na płaszzyznie XZ
                        double Diagonal_XZ_Plane = tmp_TeacherBoneLengthAll[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
                        double alpha = Math.Acos(( tmp_TeacherBoneLengthAll[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.X )/( Diagonal_XZ_Plane ));
                        double gamma = Math.Acos(( tmp_TeacherBoneLengthAll[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Z )/( Diagonal_XZ_Plane ));
                        tmp_dicAll.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<double, double, double>(alpha, beta, gamma));
                    }
                    TeacherAnglesAll.Add(index, tmp_dicAll);
                }

                //NIE KASOWAC POKI CO
                var index_for_TeacherAngles = TeacherAngles.Keys.First();

                foreach (var studentindex in StudentBoneLength.Keys)
                {
                    var tmp_StudentBoneLength = StudentBoneLength[studentindex];
                    var WhichAngles = TeacherAngles[index_for_TeacherAngles];
                    var tmp_dic = new Dictionary<Tuple<JointType, JointType>, Vector3>();

                    foreach (var boneline in BonelLines.Keys)
                    {
                        var alpha = WhichAngles[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1;
                        var beta = WhichAngles[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2;
                        var gamma = WhichAngles[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item3;

                        double Y_length = Math.Cos(beta)*( tmp_StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 );
                        double Diagonal_XZ_PlaneForSubstractingPiece = tmp_StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
                        double X_length = Math.Cos(alpha)*( Diagonal_XZ_PlaneForSubstractingPiece );
                        double Z_length = Math.Cos(gamma)*( Diagonal_XZ_PlaneForSubstractingPiece );
                        tmp_dic.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Vector3((float)X_length, (float)Y_length, (float)Z_length));
                    }
                    StudentNewBones.Add(studentindex, tmp_dic);
                    if (index_for_TeacherAngles<TeacherAngles.Keys.Last())
                        index_for_TeacherAngles++;
                }


                Debug.WriteLine("EHHHHH");
                var studentindexAll = StudentBoneLength.Keys.First();
                foreach (var index in TeacherBoneLengthAll.Keys)
                {
                    if (index>=SecondPage.TeacherStartPosition&&index<=SecondPage.TeacherEndPosition)
                    {
                        //if (studentindexAll<StudentBoneLength.Keys.Last())
                            //studentindexAll++;
                    }
                     Debug.WriteLine("EHHHHH2"+index);

                    var tmp_StudentBoneLength = StudentBoneLength[studentindexAll];
                    var WhichAngles = TeacherAnglesAll[index];
                    var tmp_dic = new Dictionary<Tuple<JointType, JointType>, Vector3>();

                    foreach (var boneline in BonelLines.Keys)
                    {
                        var alpha = WhichAngles[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1;
                        var beta = WhichAngles[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2;
                        var gamma = WhichAngles[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item3;

                        double Y_length = Math.Cos(beta)*( tmp_StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 );
                        double Diagonal_XZ_PlaneForSubstractingPiece = tmp_StudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
                        double X_length = Math.Cos(alpha)*( Diagonal_XZ_PlaneForSubstractingPiece );
                        double Z_length = Math.Cos(gamma)*( Diagonal_XZ_PlaneForSubstractingPiece );
                        tmp_dic.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Vector3((float)X_length, (float)Y_length, (float)Z_length));
                    }
                    Debug.WriteLine("EHHHHH3");

                    StudentNewBonesAll.Add(index, tmp_dic);
                    //if (index_for_TeacherAngles<TeacherAngles.Keys.Last())
                    //    index_for_TeacherAngles++;
                }
            }
        }

        //public void FullTeacherResizing(Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double,Vector3>> TeacherAnglesAndSubtractingPieces)
        //{
        //    if (TeacherAnglesAndSubtractingPieces!=null)
        //    {
        //        Dictionary<int, string> TMP_TMPFullResizedTeacherHelp_TMP = new Dictionary<int, string>();
        //        if (TMPFullResizedTeacher==null)
        //        {
        //            TMPFullResizedTeacher=new Dictionary<string, Vector3>();
        //            foreach(var item in ResizedTeacher)
        //            {
        //                TMPFullResizedTeacher.Add(item.Key, item.Value);
        //            }
        //        }
        //        else
        //        {
        //            TMPFullResizedTeacher.Clear();
        //            foreach (var item in ResizedTeacher)
        //            {
        //                TMPFullResizedTeacher.Add(item.Key, item.Value);
        //            }
        //        }
        //        //________________________________________________________________________________________________________

        //        //Vector3 ten_wektor = TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(JointType.Head, JointType.Neck)].Item4;

        //        //TMPFullResizedTeacher.Add(JointType.Head.ToString(), ResizedTeacher[JointType.Head.ToString()]);
        //        //Vector3 tmp_vecor_sub = Vector3.Subtract(ResizedTeacher[JointType.Head.ToString()],new Vector3(ten_wektor.X, ten_wektor.Y, ten_wektor.Z));
        //        //Debug.WriteLine("Dzien dobry  "+tmp_vecor_sub);
        //        //TMPFullResizedTeacher.Add(JointType.Head.ToString()+"prim", tmp_vecor_sub);

        //        //foreach (var item in TeacherAnglesAndSubtractingPieces)
        //        //{
        //        //    TMPFullResizedTeacher.Add(item.Key.Item2.ToString(), item.Value.Item4);
        //        //}                

        //        //________________________________________________________________________________________________________

        //        //resizing--w-pętli -------------------------------------------------------------------------------------- 
        //        int i = 0;
        //        TMP_TMPFullResizedTeacherHelp_TMP.Add(i, JointType.Head.ToString());
        //        bool go_thru = false;
        //        foreach (var bonelLines in BonelLines)
        //        {
        //            Vector3 Teacher_vector = ResizedTeacher[bonelLines.Key.Item2.ToString()];
        //            Debug.WriteLine("Teacher_vector name   "+bonelLines.Key.Item2.ToString());
        //            Vector3 Sub_vector = TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(bonelLines.Key.Item1, bonelLines.Key.Item2)].Item4;
        //            Vector3 new_position = Vector3.Subtract(Teacher_vector, Sub_vector);
        //            TMPFullResizedTeacher[bonelLines.Key.Item2.ToString()]=new_position;
        //            i++;
        //            TMP_TMPFullResizedTeacherHelp_TMP.Add(i, bonelLines.Key.Item2.ToString());
        //            foreach (var bonelLines2 in BonelLines)
        //            {
        //                for (int j = 0; j<=i; j++)
        //                {
        //                    //Debug.WriteLine("TMP_TMPFullResizedTeacherHelp_TMP     "+TMP_TMPFullResizedTeacherHelp_TMP[j].ToString());
        //                    if (TMP_TMPFullResizedTeacherHelp_TMP[j]==bonelLines2.Key.Item2.ToString())
        //                    {
        //                        go_thru=true;
        //                        break;
        //                    }
        //                    else
        //                        go_thru=false;
        //                }
        //                //Debug.WriteLine("\n ");

        //                if (go_thru)
        //                {
        //                    continue;
        //                }
        //                TMPFullResizedTeacher[bonelLines2.Key.Item2.ToString()]=Vector3.Subtract(ResizedTeacher[bonelLines2.Key.Item2.ToString()], Sub_vector);
        //            }
        //        }
        //        //resizing------------------------------------------------------------------------------------------------ 
        //        Device.Draw_full_resized_Teacher="draw_full_resized_Teacher";
        //        foreach(var item in TMP_TMPFullResizedTeacherHelp_TMP)
        //        {
        //            //Debug.WriteLine("TMP_TMPFullResizedTeacherHelp_TMP     "+item.Value);
        //        }
        //    }   
        //}

        //public void reczny_FullTeacherResizing(Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>> TeacherAnglesAndSubtractingPieces)
        //{
        //    if (TeacherAnglesAndSubtractingPieces!=null)
        //    {

        //        if (TMPFullResizedTeacher==null)
        //        {
        //            TMPFullResizedTeacher=new Dictionary<string, Vector3>();
        //            foreach (var item in ResizedTeacher)
        //            {
        //                TMPFullResizedTeacher.Add(item.Key, item.Value);
        //            }
        //        }
        //        else
        //        {
        //            TMPFullResizedTeacher.Clear();
        //            foreach (var item in ResizedTeacher)
        //            {
        //                TMPFullResizedTeacher.Add(item.Key, item.Value);
        //            }
        //        }

        //        //Neck
        //        Vector3 Teacher_vector = ResizedTeacher[JointType.Neck.ToString()];
        //        Vector3 Sub_vector = TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(JointType.Head,JointType.Neck)].Item4;
        //        Vector3 new_position = Vector3.Subtract(Teacher_vector, Sub_vector);
        //        TMPFullResizedTeacher[JointType.Neck.ToString()]=new_position;
        //        foreach (var item in ResizedTeacher)
        //        {
        //            if (!(item.Key.ToString()==JointType.Neck.ToString()))
        //            {
        //                TMPFullResizedTeacher[item.Key.ToString()]=Vector3.Subtract(ResizedTeacher[item.Key.ToString()], -Sub_vector);
        //            }
        //        }

        //        //Spine Shoulder 
        //        Teacher_vector = ResizedTeacher[JointType.SpineShoulder.ToString()];
        //        Sub_vector = TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder)].Item4;
        //        new_position = Vector3.Subtract(Teacher_vector, Sub_vector);
        //        TMPFullResizedTeacher[JointType.SpineShoulder.ToString()]=new_position;
        //        foreach (var item in ResizedTeacher)
        //        {
        //            if (!( item.Key.ToString()==JointType.SpineShoulder.ToString() || item.Key.ToString()==JointType.Neck.ToString() ))
        //            {
        //                TMPFullResizedTeacher[item.Key.ToString()]=Vector3.Subtract(ResizedTeacher[item.Key.ToString()], Sub_vector);
        //            }
        //        }

        //        //Spine Mid 
        //        Teacher_vector=ResizedTeacher[JointType.SpineMid.ToString()];
        //        Sub_vector=TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid)].Item4;
        //        new_position=Vector3.Subtract(Teacher_vector, Sub_vector);
        //        TMPFullResizedTeacher[JointType.SpineMid.ToString()]=new_position;
        //        foreach (var item in ResizedTeacher)
        //        {
        //            if (!( item.Key.ToString()==JointType.SpineMid.ToString()||item.Key.ToString()==JointType.SpineShoulder.ToString() ||item.Key.ToString()==JointType.Neck.ToString() ))
        //            {
        //                TMPFullResizedTeacher[item.Key.ToString()]=Vector3.Subtract(ResizedTeacher[item.Key.ToString()], Sub_vector);
        //            }
        //        }

        //        //Spine Base 
        //        Teacher_vector=ResizedTeacher[JointType.SpineBase.ToString()];
        //        Sub_vector=TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase)].Item4;
        //        new_position=Vector3.Subtract(Teacher_vector, Sub_vector);
        //        TMPFullResizedTeacher[JointType.SpineBase.ToString()]=new_position;
        //        foreach (var item in ResizedTeacher)
        //        {
        //            if (!( item.Key.ToString()==JointType.SpineMid.ToString()||item.Key.ToString()==JointType.SpineShoulder.ToString()||item.Key.ToString()==JointType.Neck.ToString() ||item.Key.ToString()==JointType.SpineBase.ToString()))
        //            {
        //                TMPFullResizedTeacher[item.Key.ToString()]=Vector3.Subtract(ResizedTeacher[item.Key.ToString()], Sub_vector);
        //            }
        //        }

        //        //Shoulder right 
        //        Teacher_vector=ResizedTeacher[JointType.ShoulderRight.ToString()];
        //        Sub_vector=TeacherAnglesAndSubtractingPieces[new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight)].Item4;
        //        new_position=Vector3.Subtract(Teacher_vector, Sub_vector);
        //        TMPFullResizedTeacher[JointType.ShoulderRight.ToString()]=new_position;
        //        foreach (var item in ResizedTeacher)
        //        {
        //            if (!( item.Key.ToString()==JointType.SpineMid.ToString()||item.Key.ToString()==JointType.SpineShoulder.ToString()||item.Key.ToString()==JointType.Neck.ToString()||item.Key.ToString()==JointType.SpineBase.ToString()||item.Key.ToString()==JointType.ShoulderRight.ToString() ))
        //            {
        //                TMPFullResizedTeacher[item.Key.ToString()]=Vector3.Subtract(ResizedTeacher[item.Key.ToString()], Sub_vector);
        //            }
        //        }

        //        Device.Draw_full_resized_Teacher="draw_full_resized_Teacher";
        //    }
        //}

        //public void reczny_FullTeacherResizing_druga_wersja(Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>> TeacherAnglesAndStudentNewBones)
        //{
        //    if(TeacherAnglesAndStudentNewBones!=null)
        //    {
        //        if (TMPFullResizedTeacher==null)
        //        {
        //            TMPFullResizedTeacher=new Dictionary<string, Vector3>();
        //        }
        //        else
        //        {
        //            TMPFullResizedTeacher.Clear();
        //        }
        //        foreach (var bonelLines in BonelLines)
        //        {
        //            Vector3 Teacher_sub_vector = Vector3.Subtract(ResizedTeacher[bonelLines.Key.Item1.ToString()], ResizedTeacher[bonelLines.Key.Item2.ToString()]);
        //            Vector3 Add_vector = TeacherAnglesAndStudentNewBones[new Tuple<JointType, JointType>(bonelLines.Key.Item1, bonelLines.Key.Item2)].Item4;
        //            Vector3 bridge = Vector3.Add(ResizedTeacher[bonelLines.Key.Item2.ToString()], Teacher_sub_vector);
        //            Vector3 new_position = Vector3.Add(bridge, Add_vector);
        //            TMPFullResizedTeacher.Add(bonelLines.Key.Item2.ToString(), ResizedTeacher[bonelLines.Key.Item2.ToString()]);
        //            TMPFullResizedTeacher.Add(bonelLines.Key.Item2.ToString()+"prim",new_position);
        //        }
        //        FullResizedTeacher=new Dictionary<string, Vector3>();
        //        FullResizedTeacher.Add(JointType.Head.ToString(), TMPFullResizedTeacher[JointType.Neck.ToString()+"prim"]);
        //        FullResizedTeacher.Add(JointType.Neck.ToString(), TMPFullResizedTeacher[JointType.Neck.ToString()]);
        //        foreach (var boneline in BonelLines)
        //        {
        //            if (!(boneline.Key.Item1==JointType.Head))
        //            {
        //                //Debug.WriteLine("Tu mnie wywala "+boneline.Key.Item1.ToString()+"---drugi"+boneline.Key.Item2.ToString());

        //                Vector3 sub_vector = Vector3.Subtract(FullResizedTeacher[boneline.Key.Item1.ToString()], TMPFullResizedTeacher[boneline.Key.Item2.ToString()+"prim"]);
        //                Vector3 new_prim_position = Vector3.Add(TMPFullResizedTeacher[boneline.Key.Item2.ToString()+"prim"], sub_vector);
        //                Vector3 new_position = Vector3.Add(TMPFullResizedTeacher[boneline.Key.Item2.ToString()], sub_vector);
        //                //FullResizedTeacher.Add(boneline.Key.Item2.ToString()+"prim", new_prim_position);
        //                FullResizedTeacher.Add(boneline.Key.Item2.ToString(), new_position);
        //            }
        //        }
        //        Device.Draw_full_resized_Teacher="draw_full_resized_Teacher";
        //    }
        //}

        //public void TeacherSkeletonAdjustmentToStudentRec()
        //{
        //    if (TeacherBoneLength!=null&&StudentBoneLength!=null&&DifferenceBetweenTeacherBoneLengthStudentBoneLength!=null)
        //    {

        //        if (TeacherAnglesAndSubtractingPieces==null)
        //            TeacherAnglesAndSubtractingPieces=new Dictionary<Tuple<JointType, JointType>, Tuple<double, double, double, Vector3>>();
        //        else
        //            TeacherAnglesAndSubtractingPieces.Clear();

        //        foreach (var boneline in BonelLines.Keys)
        //        {
        //            double beta = Math.Acos(( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Y )/( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 ));

        //            //przekatna na płaszzyznie XZ
        //            double Diagonal_XZ_Plane = TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);

        //            double alpha = Math.Acos(( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.X )/( Diagonal_XZ_Plane ));
        //            double gamma = Math.Acos(( TeacherBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item1.Z )/( Diagonal_XZ_Plane ));

        //            double Y_length = Math.Cos(beta)*( DifferenceBetweenTeacherBoneLengthStudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2 );
        //            double Diagonal_XZ_PlaneForSubstractingPiece = DifferenceBetweenTeacherBoneLengthStudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2*Math.Sin(beta);
        //            double X_length = Math.Cos(alpha)*( Diagonal_XZ_PlaneForSubstractingPiece );
        //            double Z_length = Math.Cos(gamma)*( Diagonal_XZ_PlaneForSubstractingPiece );

        //            //tylko do sprawdzenia _________________________________________________________________________________________________________________________________________
        //            double checking_beta = Math.Acos(Y_length/DifferenceBetweenTeacherBoneLengthStudentBoneLength[new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2)].Item2);
        //            double checking_alpha = Math.Acos(X_length/Diagonal_XZ_PlaneForSubstractingPiece);
        //            double checking_gamma = Math.Acos(Z_length/Diagonal_XZ_PlaneForSubstractingPiece);
        //            //_____________________________________________________________________________________________________________________________________________________________________

        //            float lenght = (float)Math.Sqrt(Math.Pow(X_length, 2)+Math.Pow(Y_length, 2)+Math.Pow(Z_length, 2));
        //            TeacherAnglesAndSubtractingPieces.Add(new Tuple<JointType, JointType>(boneline.Item1, boneline.Item2), new Tuple<double, double, double, Vector3>(alpha, beta, gamma, new Vector3((float)X_length, (float)Y_length, (float)Z_length)));

        //            //sprawdzam czy katy sa te same  
        //            //Debug.WriteLine(boneline.Item1+"---"+boneline.Item2+" Kąty_kosci \t\t"+alpha+" ,  "+beta+" ,  "+gamma);
        //            //Debug.WriteLine(boneline.Item1+"---"+boneline.Item2+" Kąty_odejmowanego kawałka "+checking_alpha+" ,  "+checking_beta+" ,  "+checking_gamma);
        //            //Debug.WriteLine(" length "+X_length+"  ,  "+Y_length+"  ,  "+Z_length+"  długosc  "+lenght);
        //        }
        //    }
        //}

    }
}
