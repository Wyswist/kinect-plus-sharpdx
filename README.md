# Visual assessment of the correctness of human training movements using the Kinect sensor.

The project was made as an Bachelor final project. A detailed description of the project can be found at this [link](https://drive.google.com/file/d/1E65C7bpwzw23cgbyhaoGEMxOYH4toR6F/view?usp=sharing). The Microsoft Kinect SDK technology was used. The project was written in C# language. For the application to work properly, you need a Kinect One sensor or Kinect Studio program.

## 
### Short description of the project:
The project was to teach new movement techniques on the basis of previously recorded correct movements. First, the movements of the "*teacher*" who knows best how to execute the movement are recorded. Then a "*student*" should appear in front of the kinect sensor who wants to recreate the movements.The software shows in the subsequent motion sequences, which body parts are stacked in the wrong alignment of the body compared to the "*teacher*".
